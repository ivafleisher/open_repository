#include <stdio.h>/*printf*/
#include <stdlib.h>/*malloc*/
#include "fsa.h"

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

static void TestSuggestSize     ();
static void TestData            ();

static void VerifySuggestSize   (size_t result , size_t expected);
static void VerifyFreeSize      (size_t result , size_t expected);
/******************************************************************************
                                    * MAIN *       
******************************************************************************/
int main()
{
    TestSuggestSize();
    TestData();
 
    return 0;
}
/******************************************************************************/

static void TestSuggestSize()
{
    size_t element_size[] = {4,8,10,24};
    printf(KCYN"TestSuggestSize\n"KWHT);

    VerifySuggestSize(FSASuggestSize(2, element_size[0]), 40 );
    VerifySuggestSize(FSASuggestSize(2, element_size[1]), 40 );
    VerifySuggestSize(FSASuggestSize(2, element_size[2]), 48 );
    VerifySuggestSize(FSASuggestSize(2, element_size[3]), 72 );
 
}
/******************************************************************************/

static void Print(size_t *buff, size_t size)
{
    size_t i = 0;
    printf(KCYN"next free %lu  | "KWHT, buff[0]);
    for (i = 1; i < size; i+=2)
    {
        printf(KMAG"%lu "KWHT, buff[i]);
        printf("%lu ", buff[i+1]);
    }
}
/******************************************************************************/
static void TestData()
{
    fsa_t *allocator = NULL;
    size_t * buffer = NULL;
    
    void * to_free3 = NULL;
    void * to_free [4] = {NULL};
    
    size_t data[] ={1,2,3,4};
    size_t size = 3;
    size_t i = 0;
    printf(KCYN"TestData\n"KWHT);

    VerifySuggestSize(FSASuggestSize(3, 4), 56);
    buffer = malloc(56);
    allocator = FSAInit(buffer, 56, 4);
    VerifyFreeSize(FSANumOfAvailableBlocks(allocator),3);
    Print(buffer, 7);

    printf(KYEL"\nDATA\n"KWHT);
   

    for (i = 0; i < size; i++)
    {
         printf(KMAG"\nALLOC \n"KWHT);
        to_free[i] = FSAAlloc(allocator);
        *(size_t *)to_free[i]  = data[i];
        Print(buffer, 7);
        VerifyFreeSize(FSANumOfAvailableBlocks(allocator),size- i-1);
    }

    for (i = 0; i < size; i++)
    {
        printf(KMAG"\nFREE\n"KWHT);
        FSAFree(to_free[i]);
        Print(buffer, 7);
        VerifyFreeSize(FSANumOfAvailableBlocks(allocator), i+1); 
    }

    
    printf(KMAG"\nALLOC 4\n"KWHT);
    to_free[3] = FSAAlloc(allocator);
    *(size_t *)to_free[3] = data[3];
    Print(buffer, 7); 

    free(buffer);
}
/************************************Verify************************************/
static void VerifySuggestSize(size_t result , size_t expected)
{
    if (result != expected)
    {
        printf(KRED"Problem.VerifySuggestSize %lu, expected %lu\n"KWHT,
              result, expected);
    }
    else
    {
        printf(KGRN"GOOD.VerifySuggestSize\n"KWHT);
    }
}

static void VerifyFreeSize(size_t result , size_t expected)
{
    if (result != expected)
    {
        printf(KRED"Problem.VerifyFreeSize %lu, expected %lu\n"KWHT,
              result, expected);
    }
    else
    {
        printf(KGRN"GOOD.VerifyFreeSize\n"KWHT);
    }
}