/*******************************************************************************
                            *FSA FUNCTIONS*
 ******************************************************************************/
#include<assert.h> /* assert */
#include "fsa.h"   /* fsa_t */

#define WORD_SIZE (sizeof(size_t))
#define HEADER (sizeof(size_t))
#define ASSERT_NOT_ZERO(x) (assert(0 != x));
#define ASSERT_NOT_NULL(x) (assert(NULL != x));

/* Fixed Size Allocator imitates the behavior of malloc and free */
struct allocator 
{
    size_t offset_next_free;
};

/*******************************SUPPORT FUNC **********************************/

static size_t AlignedSize(size_t size_block);

/******************************************************************************/
fsa_t *FSAInit(void *buffer, size_t buffer_size, size_t block_size)
{
    fsa_t *fsa = NULL;
    size_t *runner = NULL;
    size_t block = 0;
    size_t amount_of_blocks = 0;

    ASSERT_NOT_NULL(buffer)
    ASSERT_NOT_ZERO(buffer_size) 
    ASSERT_NOT_ZERO(block_size) 

    fsa =  buffer;
    runner = (size_t *)(fsa + 1);
    block = AlignedSize(block_size) + HEADER;
    amount_of_blocks = (buffer_size - sizeof(fsa_t) ) / block;
    
    fsa->offset_next_free = ((size_t)runner - (size_t)fsa) + HEADER;
    
    while(amount_of_blocks - 1)
    {
        *runner = ((size_t)runner - (size_t)fsa) + (block + HEADER);
        runner = (size_t *)((char*)runner + block);
        --amount_of_blocks; 
    }

    *runner = 0;

    return fsa;
}

/******************************************************************************/

void *FSAAlloc(fsa_t *allocator)
{
    char * to_return = NULL;
    size_t offset = 0;
    size_t ofset_to_allocated = 0;
    
    ASSERT_NOT_NULL(allocator)

    to_return = (char *)allocator;
    if(0 == allocator->offset_next_free)
    {
        return NULL;
    }
    /* find next free */
    ofset_to_allocated = allocator->offset_next_free; 
    to_return  = (void*)(to_return + allocator->offset_next_free);
    
    /* assigned to allocator->next free - new free block */
    allocator->offset_next_free = *((size_t *)(to_return - HEADER));
    
    /* assigned offset of allocated block */
    *((size_t *)(to_return - HEADER)) = ofset_to_allocated;

    return to_return;
}

/******************************************************************************/

void FSAFree(void *block)
{
    char *runner = NULL;
    fsa_t *allocator = NULL;
    size_t offset_to_fsa = 0;
    size_t next_free = 0;
    
    ASSERT_NOT_NULL(block)
    
    runner = block;
    /* find offset to allocator */
    offset_to_fsa = *((size_t*)(runner - HEADER));
    allocator = (fsa_t *)(runner - offset_to_fsa);
    
    /* save next free  */
    next_free = allocator->offset_next_free;
    
    /*   asssigned to the new next free */
    allocator->offset_next_free = offset_to_fsa;
    
    /* assigned in heder of new free - next free */
    *((size_t *)(runner - HEADER)) = next_free;
}

/******************************************************************************/

size_t FSANumOfAvailableBlocks(fsa_t *allocator)
{
    size_t *runner = NULL;
    size_t counter = 0;
    
    ASSERT_NOT_NULL(allocator)

    if (allocator->offset_next_free == 0)
    {
        return 0;
    }
    
    runner = (size_t *)((char *)allocator + allocator->offset_next_free - HEADER); 
    while(*runner)
    {
        ++counter;
        runner = (size_t *)((char*)allocator + *runner - HEADER);
    } 

    return counter + 1;
}
/******************************************************************************/

size_t FSASuggestSize(size_t num_of_blocks, size_t block_size)
{
    return ((AlignedSize(block_size) + HEADER) * num_of_blocks) + sizeof(fsa_t);
}
/******************************************************************************/

static size_t AlignedSize(size_t size_block)
{
    if (size_block % WORD_SIZE == 0)
    {
        return size_block;
    }

    return size_block + (WORD_SIZE - (size_block % WORD_SIZE));
}