#include <stdio.h>      /* printf           */
#include <unistd.h>     /* sleep            */
#include "watchdog.h"  /* START &END watch */

int main( int argc, char **argv )
{
    int i = 0;
    StartWatch((char const **)argv);

    for (i = 0;i <30; i++)
    {
        printf("USER PART :%d\n",i);
        sleep(1); 
    }
    EndWatch();

    return 0;
    (void)argc;
}