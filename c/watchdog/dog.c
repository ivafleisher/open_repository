/*
 * =============================================================================
 *
 *       Filename:  dog.c
 *
 *       Description:  Watchdog
 *
 *       Version:  1.0

 *
 * =============================================================================
 */
#define _POSIX_C_SOURCE 199309L
#define _XOPEN_SOURCE
#include <errno.h>           /*  errno          */
#include <signal.h>          /*  kill           */
#include <stdio.h>           /*  printf         */
#include <unistd.h>          /*  execv ,sleep   */
#include <stdlib.h>          /*  exit , abort   */
#include <fcntl.h>           /*  sem open       */
#include <semaphore.h>       /*  semaphore      */

#include "scheduler.h"
#include "watchdog.h"
typedef int(*task_t)(void*param);

typedef struct data
{
    sched_t *scheduler;
    task_t reveive;
    pid_t* other_pid;
    void *name;

    volatile sig_atomic_t *run;
    volatile sig_atomic_t *is_received;

}data_t;

static void LOG(const char * msg)
{
    fputs(msg,stderr);
}
#define RETURN_BAD(is_good, status, msg)\
if((is_good)){LOG(msg);return(status);}

void DogHandler(int signum);
int InitScheduler(data_t *value);
int TaskIsRecieved(void*param);
int TaskSend(void*param);
int RevieveUser(void *info);


/******************************************************************************/
static volatile sig_atomic_t is_running_dog = 1;
static volatile sig_atomic_t is_alive_u = 0;
pid_t other_pid;
sem_t *sem;

#define NAME_DOG "./dog"
#define SEM_NAME "/Moria"
#define SEM_PERMS (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP)
#define INITIAL_VALUE 0
/******************************************************************************/

void DogHandler(int signum)
{
#ifndef NDEBUG
    printf("DOG RECEIVE SIGNAL %d\n",getpid());
#endif
    if(signum == SIGUSR2)
    {
        is_running_dog = 0;
    }
    is_alive_u = 1;
    (void) signum;
}
/******************************************************************************/
/*start waching the user programm*/
int main( int argc, char **argv )
{
    struct sigaction UserActionRUN = {0};
    struct sigaction UserActionSTOP = {0};
    data_t param_d = {0};

    param_d.reveive = RevieveUser;
    param_d.run =&is_running_dog;
    param_d.is_received =&is_alive_u;
    param_d.name = argv+1;

    other_pid = getppid();/* PID FATHER */
    param_d.other_pid =&other_pid;

    param_d.scheduler = SchedCreate();
    RETURN_BAD(NULL == param_d.scheduler, 1,"FAIL create Scheduler ")

    UserActionRUN.sa_handler = DogHandler;
    UserActionSTOP.sa_handler = DogHandler;

    RETURN_BAD(0 > sigaction(SIGUSR1, &UserActionRUN, NULL), 1,"FAIL sigaction ")
    RETURN_BAD(0 > sigaction(SIGUSR2, &UserActionSTOP, NULL), 1,"FAIL sigaction ")

    #ifndef NDEBUG
    printf("***IN DOG\n");
    printf("***PID FATHER %d\n",other_pid);
    printf("***PID DOG %d\n",getpid());
    #endif

    if ((sem = sem_open(SEM_NAME, O_EXCL, SEM_PERMS, INITIAL_VALUE)) == SEM_FAILED)
    {
        perror ("sem_open");
    }
    sem_post (sem);

#ifndef NDEBUG
    printf("SEM POST\n");
#endif

    InitScheduler(&param_d);
    SchedRun(param_d.scheduler);
    SchedDestroy(param_d.scheduler);

    sem_close(sem);

    return 0;
    (void)argc;
}

/******************************************************************************/
int RevieveUser(void *info)
{
    data_t *reveive = (data_t *)info;
#ifndef NDEBUG
    printf("REVIVE USER %s\n", ((char **)reveive->name)[0]);
#endif
    execv(((char **)(reveive->name))[0],reveive->name);
    RETURN_BAD(1, 1, "execv fail")

    return 0;
}
