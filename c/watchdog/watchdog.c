/*
 * =============================================================================
 *
 *       Filename:  Watchdog.c
 *
 *       Description:  Watchdog
 *
 *       Version:  1.0
 *      
 * =============================================================================
 */
#define _POSIX_C_SOURCE 199309L
#define _XOPEN_SOURCE
#include <errno.h>           /*  errno              */ 
#include <signal.h>          /*  kill               */
#include <stdio.h>           /*  printf             */
#include <unistd.h>          /*  execv ,sleep       */
#include <stdlib.h>          /*  exit , abort       */
#include <string.h>          /*  strcmp,memcpy      */
#include <fcntl.h>           /*  sem open           */
#include <pthread.h>         /*  create thread      */
#include <semaphore.h>       /*  semaphore          */

#include "scheduler.h"
#include "watchdog.h"

typedef int(*task_t)(void*param);

typedef struct data
{
    sched_t *scheduler;
    task_t reveive;
    pid_t* other_pid;
    void *name;

    volatile sig_atomic_t *run;
    volatile sig_atomic_t *is_received;
}data_t;
/*************************Declaration*******************************************/
int InitScheduler(data_t *value); 
int TaskIsRecieved(void*param);
int TaskSend(void*param);
int CreateDog(void *info,pid_t *other_pid);
int ReviveDog(void *value);

void* ResponseThread(void *param);
static void CreateThread(int argc ,char **argv) ;
static void Destroy();

static void LOG(const char * msg)
{
    fputs(msg,stderr);
}

static void PER_MSG(const char *str)
{
    perror (str);
}

#define RETURN_PERROR(is_good, msg)\
if((is_good)){PER_MSG(msg);}

#define RETURN_BAD(is_good, status, msg)\
if((is_good)){LOG(msg);return(status);}

/******************************************************************************/
volatile sig_atomic_t is_running = 1;
volatile sig_atomic_t is_alive = 0;

pid_t other_pid;
void * name = NULL;
pthread_t response;
sem_t *sem;

#define NAME_DOG "./dog"
#define SEM_NAME "/Moria"
#define FAIL    (1)
#define INITIAL_VALUE 0
#define SEM_PERMS (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP)
/******************************************************************************/
static void __attribute__((constructor)) CreateThread(); 
static void __attribute__((destructor)) Destroy();  

void UserHandler(int signum)
{
    if(signum == SIGUSR2)
    {
        is_running = 0;
    }

    is_alive = 1;
    (void) signum;
}
/******************************************************************************/

/*start waching the user programm*/
int StartWatch(char const *argv[])
{
    struct sigaction UserActionRUN = {0};
    struct sigaction UserActionSTOP = {0}; 
   
    name = argv;
    UserActionRUN.sa_handler = UserHandler;
    UserActionSTOP.sa_handler = UserHandler; 
  
    RETURN_BAD(0 > sigaction(SIGUSR1, &UserActionRUN, NULL), 1,"FAIL sigaction ")
    RETURN_BAD(0 > sigaction(SIGUSR2, &UserActionSTOP, NULL), 1,"FAIL sigaction ") 
    /* Create DOG */
    RETURN_BAD(CreateDog(argv,&other_pid), 1,"FAIL Create dog");
    
    return 0;
}

void EndWatch()
{
    is_running = 0;
}

/******************************************************************************/
void* ResponseThread(void *param)
{
    data_t param_u = {0};
    printf("***IN THREAD %d\n",getpid());
    
    param_u.reveive = ReviveDog;
    param_u.run = &is_running;
    param_u.is_received = &is_alive;
    param_u.other_pid = &other_pid;
    param_u.name = name;
    param_u.scheduler = SchedCreate();
    RETURN_BAD( NULL == param_u.scheduler, NULL, "CREATE SCHEDULER fail")
   
    /* create semaphore */
    if ((sem = sem_open (SEM_NAME, O_CREAT, 0660, 0)) == SEM_FAILED) 
    {
        SchedDestroy(param_u.scheduler);
        kill(*(param_u.other_pid), SIGUSR2);
        perror ("sem_open");

        return NULL;
    }
    
    /* WAIT For semaphore from dog*/
    sem_wait(sem);
   
    /* RUN scheduler */
    if  (InitScheduler(&param_u) == FAIL)
    {
        SchedDestroy(param_u.scheduler);
        RETURN_PERROR(kill(*(param_u.other_pid), SIGUSR2)!= 0, "kill fail")
        sem_close(sem);
        sem_unlink(SEM_NAME);

        return NULL;
    }

    SchedRun(param_u.scheduler); 
    SchedDestroy(param_u.scheduler);

    sem_close(sem);
    sem_unlink(SEM_NAME);

    return NULL;
    (void)param;
}
/******************************************************************************/
int InitScheduler(data_t *value)
{
    ilrd_uid_t uid = {0};
    uid = SchedAdd(value->scheduler ,TaskIsRecieved,value,5, time(NULL)+2);
    RETURN_BAD(UIDIsSame(uid, bad_uid), 1,"FAIL SCHEDULER ADD ")

    uid = SchedAdd(value->scheduler ,TaskSend, value, 1, time(NULL));
    RETURN_BAD(UIDIsSame(uid, bad_uid), 1,"FAIL SCHEDULER ADD ")

    return 0;
} 
/********************************TASKS*****************************************/
int TaskIsRecieved(void*param)
{
    data_t *data = (data_t *)param;
    
    if(!*(data->run)){ return 0; }

    if(!(*(data->is_received)))
    {
#ifndef NDEBUG
        printf("\n\n%d FLAG %d TO REVIVE*****\n\n",getpid(),*(data->is_received));
#endif 
        RETURN_PERROR(kill(*(data->other_pid), SIGKILL)!= 0, "kill fail")
        data->reveive(data); 
    }
    else
    {

#ifndef NDEBUG
        printf("\nFLAG %d,%d IS RECIEVED\n\n",*(data->is_received),getpid());
#endif
        *(data->is_received) = 0;
    }

    return 1;
}

int TaskSend(void*param)
{
    data_t *reveive = (data_t *)param;
    
    if(!*(reveive->run))
    {
        RETURN_PERROR(kill(*(reveive->other_pid), SIGUSR2)!= 0, "kill fail")

        return 0;
    }

#ifndef NDEBUG
    printf("%d SEND SIGNAL to %d\n",getpid(),*(reveive->other_pid));
#endif
    RETURN_PERROR(kill(*(reveive->other_pid), SIGUSR1)!= 0, "kill fail")

    return 1;
}
/*******************************************************************************/
int CreateDog(void *info,pid_t *pid)
{
    char **args = NULL;
    char **information = info;
    int arg = 0;
    
    while (*information != NULL)
    {
        information++;
        arg++;
    }

    args = malloc(sizeof(void*)*(arg + 2));
    memcpy ((args + 1), info, sizeof(void*) * arg);
    args[0] = NAME_DOG;
    args[arg+1] = NULL;

    *pid = fork();
    RETURN_BAD(0 > *pid, 1,"FAIL fork ") 
        
    if (*pid == 0)
    {
#ifndef NDEBUG
        printf("\n\n**********CREATE DOG\n\n");
#endif
        execv(NAME_DOG, args);
        RETURN_BAD(1, 1, "FAIL execv ") 
    }

    free(args);

    return 0;
}

int ReviveDog(void *value)
{
    data_t *reveive = (data_t *)value;
    RETURN_BAD(CreateDog(reveive->name, reveive->other_pid), 1,"FAIL CREATE DOG ") 
    sem_wait(sem);

    return 0;
}
/******************************************************************************/
 static void CreateThread(int argc ,char **argv) 
{ 
    if(0 == strcmp(*argv, NAME_DOG)){ return; }
    /*Create Thread that will wait for dog to send the signal*/
    pthread_create(&response, NULL, ResponseThread,NULL); 
    (void)argc;
}


static void Destroy()
{
    pthread_join(response,NULL);
#ifndef NDEBUG
    printf("\n\nI am called Last %d\n",getpid()); 
#endif
}



