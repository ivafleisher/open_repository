/*
 * =============================================================================
 *
 *       Filename:  Watchdog.h
 *
 *       Description:  Watchdog Header File
 *
 *       Version:  1.0
 *   
 *
 * =============================================================================
 */

#ifndef __WATCHDOG_H__
#define __WATCHDOG_H__


/*start waching the user programm*/
int StartWatch(char const *argv[]);

/*end watching the user programm*/
void EndWatch();


#endif /* __WATCHDOG_H__*/
