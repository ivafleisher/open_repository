
 /******************************************************************************
 *          * CALCULATOR *
 *          Description:  Perform mathematical expression 
 *          with using Reverse Polish notation (RPN).
 *
 *          Version:  1.0
 *
 ******************************************************************************/
#include <ctype.h>  /*  isdigit  */
#include <assert.h> /*  assert   */
#include <stdlib.h> /*  malloc   */
#include <string.h> /*  strlen   */
#include <stdio.h>  /*  strlen   */
#include <math.h>   /*  power    */
#include "calculator.h"
#include "stack.h"

#define MAX_CHAR (256)
/******************************************************************************/
typedef enum en_state
{
	WAITING_FOR_NUMBER,
	WAITING_FOR_OPERATION,
	NUM_OF_OPER
} state_t;

typedef struct calculator
{
	stack_t *operators;
	stack_t *numbers;
    state_t state;
    char *input;
}calculator_t;
/*****************************************************************************/
typedef calc_status_t (*action)(calculator_t * calculator);
typedef double (*operation)(double first, double second);

static calculator_t * Init                 (size_t size, char **expression);
static calc_status_t ParserNum             (calculator_t * calculator);
static calc_status_t ParserOperation       (calculator_t * calculator);
static calc_status_t ParserOpenBracket     (calculator_t * calculator);
static calc_status_t ParserCloseBracket    (calculator_t * calculator);
static calc_status_t Error                 (calculator_t * calculator);
static void ProccessOperation              (calculator_t * calculator);
static void FinishOper                     (calculator_t * calculator);

static double Add                          (double num1, double num2);
static double Divide                       (double num1, double num2);
static double Multiply                     (double num1, double num2);
static double Substract                    (double num1, double num2);
static double Power                        (double num1, double num2);
static double Dummy                        (double num1, double num2);

 
static const action LUT[NUM_OF_OPER][MAX_CHAR] = {{
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
ParserOpenBracket,Error,Error,Error,Error,Error,Error,Error,
ParserNum,ParserNum,ParserNum,ParserNum,ParserNum,
ParserNum,ParserNum,ParserNum,
ParserNum,ParserNum,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error}
,
{Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,ParserCloseBracket,ParserOperation,ParserOperation,Error,
ParserOperation,Error,ParserOperation,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,ParserOperation,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error,
Error,Error,Error,Error,Error,Error,Error,Error
}};


static const operation table_operation[MAX_CHAR] = {
Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,
Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,
Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,
Dummy,Dummy,Dummy,Multiply,Add,Dummy,Substract,Dummy,Divide,Dummy,Dummy,Dummy,
Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,
Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,
Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,
Dummy,Dummy,Dummy,Dummy,Power,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,
Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,
Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,
Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,
Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,
Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,
Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,
Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,
Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,
Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,
Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,
Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,
Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy,Dummy};

const int precedence [MAX_CHAR] = 
{
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,-1,-1,2,1,0,1,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
};

/*
* Input: pointer to the string expression, pointer to double answer
* Process: perform mathematical expression and store result in answer
* Returns: calc_status_t enum status SUCCESS(0) if successful / other statuses
*                  if failed
*/
calc_status_t Calculator(const char *expression, double *_answer)
{
    size_t size = strlen(expression);
    calc_status_t status = SUCCESS;
    calculator_t* calculator = Init (size, (char **)&expression);

    if (NULL == calculator)
    {
        return status = SYSTEM_ERROR;
    }

    while('\0' != *(calculator->input) && SUCCESS == status)
    {
        status = LUT[calculator->state][(int)(*(calculator->input))](calculator);
    }
   
    while(!StackIsEmpty(calculator->operators))
    {
        FinishOper(calculator);
    }

    *_answer = *(double *)StackPop(calculator->numbers);

    return status;
}

/********************************SUPPORT FUNC**********************************/

static calculator_t * Init (size_t size, char **expression)
{
    calculator_t * calculator = malloc(sizeof(calculator_t));
    if (NULL == calculator)
    {
        return NULL;
    }
    
    calculator->numbers = StackCreate(size, sizeof(double));
    calculator->operators = StackCreate(size, sizeof(char));
    if(NULL == calculator->numbers)
    {
        free(calculator);
        return NULL;
    }

    if (NULL == calculator->operators)
    {
        free(calculator);
        calculator = NULL;
        
        free(calculator->numbers);
        calculator->numbers = NULL;
        
        return NULL;
    }
    
    calculator->input = *expression;
    calculator->state = WAITING_FOR_NUMBER;
    
    return calculator;
}
/******************************************************************************/
static calc_status_t ParserNum(calculator_t * calculator)
{
    double number = strtod(calculator->input, &calculator->input);
    StackPush(calculator->numbers, &number);
    calculator->state = WAITING_FOR_OPERATION;

    return SUCCESS;
}

static calc_status_t ParserOperation(calculator_t * calculator)
{
    char operator =*(calculator->input);
    calculator->input++;
    
    if (!StackIsEmpty(calculator->operators) &&
    precedence[(int)operator] < precedence[(int)(*(char*)StackPeek(calculator->operators))])
    {
        ProccessOperation(calculator);
    }

    StackPush(calculator->operators,&operator);
    calculator->state = WAITING_FOR_NUMBER;
    
    return SUCCESS;
}
/******************************************************************************/
static calc_status_t ParserOpenBracket(calculator_t * calculator)
{
    char operator =*(calculator->input);
    calculator->input++;
    StackPush(calculator->operators,&operator);

    calculator->state = WAITING_FOR_NUMBER;

    return SUCCESS;
}
static calc_status_t ParserCloseBracket(calculator_t * calculator)
{
    calculator->state = WAITING_FOR_NUMBER;
    calculator->input++;

    while('(' != *(char *)StackPeek(calculator->operators))
    {
        ProccessOperation(calculator);
    }

    StackPop(calculator->operators);

    return SUCCESS;
}

/******************************************************************************/
static void FinishOper(calculator_t * calculator) 
{
   	while (!StackIsEmpty(calculator->operators))
	{
        ProccessOperation(calculator);
	}
}

static void ProccessOperation(calculator_t *calculator)
{
    double num1 = 0;
    double num2 = 0;
    double result = 0;

    int index = (int)(*(char *)(StackPop(calculator->operators)));
    num2 = *(double *)StackPop(calculator->numbers);
    num1 = *(double *)StackPop(calculator->numbers);
    result = table_operation[index](num1, num2);
    
    StackPush(calculator->numbers,&result);
}
/**************************OPERATION*******************************************/
static calc_status_t Error(calculator_t * calculator)
{
    return SYNTAX_ERROR;
    (void)calculator;
}
static double Add(double num1, double num2)
{
    return num1 + num2;
}
static double Divide(double num1, double num2)
{
    return num1 / num2;
}
static double Multiply(double num1, double num2)
{
    return num1 * num2;
}
static double Substract(double num1, double num2)
{
    return num1 - num2;
}
static double Power(double num1, double num2)
{
    return pow(num1, num2);
}
static double Dummy(double num1, double num2)
{
    return SYNTAX_ERROR;
    (void)num1;
    (void)num2;
}



