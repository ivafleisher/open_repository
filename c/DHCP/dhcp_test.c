/*******************************************************************************
                                 * TESTS DHCP * 
*******************************************************************************/
#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

#include <stdio.h>
#include "dhcp.h"

static void TestCreate(void);
static void TestAlloc(void);
static void PrintIP(ip_t ip);

void VerifySize(dhcp_t *server, size_t expected);
static void IsSame(unsigned char* source, unsigned char* dest);


/*******************************************************************************
                                 * MAIN * 
*******************************************************************************/
int main(void)
{

/*     TestCreate(); */
     TestAlloc(); 
    return 0; 
}

/*******************************************************************************
                                 * TESTS * 
*******************************************************************************/
static void TestCreate(void)
{
    ip_t network = {196, 196, 196, 0};
    dhcp_t *server = DHCPCreate(network, 30);

    if (NULL == server)
    {
        printf("FAIL.TestCreate\n");
    }
    VerifySize(server, (1 << (2)) - 3);
    
    DHCPDestroy(server); 
}


static void TestAlloc(void)
{
    ip_t network = {255,255,255,255};
    ip_t return_addr = {0,0,0,0};
    
    dhcp_t *dhcp = DHCPCreate(network, 29);
    
    ip_t data[]= {{54, 48, 22, 1},
                 {54, 48, 23, 16},
                 {54, 48, 23, 17},
                 {54, 48, 23, 2}};
    size_t size = (1 << 3) - 3;

    size_t size_arr= sizeof(data)/sizeof(data[0]);
    size_t i = 0;

    VerifySize(dhcp, (1 << 3) - 3);
    for (i = 0; i < size_arr; i++)
    {
        printf(KMAG"\nALLOC\n"KWHT);
        PrintIP(data[i]);

        DHCPAllocate(dhcp,data[i], return_addr);
        printf(KMAG"\nRETURN ADDRESS\n"KWHT);

        PrintIP(return_addr);

        IsSame((unsigned char*)data[i], (unsigned char*)return_addr);
        VerifySize(dhcp, size-i-1);
    }

    printf(KMAG"\nREMOVE\n"KWHT);

    PrintIP(return_addr);

    DHCPFree(dhcp, return_addr);
    VerifySize(dhcp, size-i+1);


    DHCPDestroy(dhcp); 
}
/*******************************************************************************
                                 * VERIFY * 
*******************************************************************************/

void VerifySize(dhcp_t *server, size_t expected)
{
    if (CountFree(server) != expected)
    {
        printf(KRED"Problem.VerifySize %lu, expected %lu\n"KWHT,
                CountFree(server), expected);
    }
    else
    {
        printf(KGRN"GOOD.VerifySize %lu\n"KWHT,
                CountFree(server));
    }
}

static void PrintIP(ip_t ip)
{
	printf("IP: %d.%d.%d.%d\n", ip[0], ip[1], ip[2], ip[3]);
}

static void IsSame(unsigned char* source, unsigned char* dest)
{
  size_t i = 0;

  for (i = 0; i < 4; i++)
  {
    if (dest[i] != source[i])
    {
        printf("PROBLEM. dest = [%d], sourse =[%d]\n",dest[i] ,source[i]);
    }
  }
}

static void PrintBitArr(int bitarr)
{
    int i = 0; 
    int mask = 1;
    for (i = 0; i < 32; i++)
    {
        if(i % 8 == 0)
        {
            printf("\n");
        }
        
        printf("%d,", bitarr & mask);
        bitarr>>=1;
    }
} 


/

static void PrintBitArr(int bitarr)
{
    int i = 0; 
    int mask = 1;
    for (i = 0; i < 32; i++)
    {
        if(i % 8 == 0)
        {
            printf("\n");
        }
        
        printf("%d,", bitarr & mask);
        bitarr>>=1;
    }
} 
