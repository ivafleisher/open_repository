#define _POSIX_C_SOURCE 200112L  /* timespec */

#include <pthread.h>    /*timespec      */
#include <stdio.h>      /*printf        */
#include "stack.h"      /*stack         */
#include "bits_array.h" /*bits_array    */

#define START_POSITIONS (64)
#define BOARD_WIDTH (8)
#define POSITIONS (64)
#define LIMIT_TIME (100)

#define OFFSET_D_LR (9)
#define OFFSET_D_RL (7)
#define ONE ((size_t)(1))

typedef struct coordinates
{
    int x;
    int y;
} coordinates_t;

static int EightQueen();

static coordinates_t ConvertToCoordinates(size_t position);
static int QueenTour                     (size_t chess_board,
                                         coordinates_t Position,
                                         stack_t *stack_positios,size_t counter);

static int IsLegalAndFree           (size_t chess_board, coordinates_t Position);
static size_t FillBoard             (size_t chess_board, coordinates_t position);
static size_t ConvertToPosition     (coordinates_t coordinates);

static size_t LUpRDown                      (size_t Position);
static size_t UpDown                        (size_t Position);
static size_t RL                            (size_t Position);
static size_t RUpLDown                      (size_t Position);
 
static int AreEightQueensOnBoard            (size_t counter);
static size_t MaskLUpRDown                  ();
static size_t MaskRL                        ();
static size_t MaskUpDown                    ();
static size_t MaskRUpLDown                  ();

typedef size_t (*new_position)(size_t);

int main()
{
    EightQueen();

    return 0;
}

static int EightQueen()
{
    int j = 0;
    int i = 0;
    struct timespec start = {0};
    struct timespec end = {0};
    double time_taken = 0;

    size_t chess_board = 0;
    stack_t *arr_stacks[START_POSITIONS] =  {0};

    for (j = 0; j < START_POSITIONS; j++)
    {
        arr_stacks[j]= StackCreate(8, sizeof(coordinates_t));
    }
    
	clock_gettime(CLOCK_MONOTONIC, &start); 

    for(j = 0; j < START_POSITIONS; j++)
    {
        coordinates_t first = ConvertToCoordinates(j);
       
        printf("[%d] %d\n", j, QueenTour(chess_board, ConvertToCoordinates(j),
                 arr_stacks[j], 1)); 

        StackPush(arr_stacks[j], &first);
    }
     
    clock_gettime(CLOCK_MONOTONIC, &end); 
    time_taken = (end.tv_sec - start.tv_sec) * 1e9; 
    time_taken = (time_taken + (end.tv_nsec - start.tv_nsec)) * 1e-9; 

    for(j = 0; j < START_POSITIONS; j++)
    {
        while (!StackIsEmpty(arr_stacks[j]))
        {
             coordinates_t data =  *(coordinates_t *)StackPop(arr_stacks[j]);
            printf("stack [%d]position [%d] :x %d, y %d  ;\n",j ,i, data.x, 
                                                            data.y);
            i++;
        }
        
        i = 0;
        printf("\n\n");
        
        StackDestroy(arr_stacks[j]);
        arr_stacks[j] = NULL;
    }
    printf("Time : %f\n", time_taken);

    return 0;
}



static int QueenTour(size_t chess_board,coordinates_t position,
                                         stack_t *stack_positios,
                                          size_t amount_of_queens)
{
    size_t i = 0;
    coordinates_t potential_position = {0};
   
    if (AreEightQueensOnBoard(amount_of_queens))
    {
        return 1;
    }

    chess_board = FillBoard(chess_board, position);

    for (i = 0; i < POSITIONS; i++)
    {
        potential_position = ConvertToCoordinates(i);
        
        if (IsLegalAndFree(chess_board, potential_position))
        {
            position = potential_position;
            
            if (QueenTour(chess_board, position, stack_positios, amount_of_queens+1))
            {
                StackPush(stack_positios, &position);
                
                return 1;
            }
        }
    }
   
    return 0;
}

/******************************************************************************/

static int IsLegalAndFree (size_t chess_board, coordinates_t position)
{
    int x = position.x;
    int y = position.y;
    size_t one_oposite = 1 ;
    int width = BOARD_WIDTH;
  
    if ((x > width-1) || (x < 0))
    {
        return 0;
    }
    else if ((y > width-1) || (y < 0))
    {
        return 0;
    }
    else if (((one_oposite << (POSITIONS-1)) >> 
                ConvertToPosition(position)) & chess_board)
    {
        return 0;
    }
   
    return 1;
}

 static int AreEightQueensOnBoard(size_t counter)
{
    return 8 == counter;
}
/*******************************Convert****************************************/
size_t ConvertToPosition(coordinates_t coordinates)
{  
    return coordinates.x + coordinates.y * BOARD_WIDTH;
}

coordinates_t ConvertToCoordinates(size_t position)
{
    coordinates_t data = {0};
    data.x = position % BOARD_WIDTH;
    data.y = position / BOARD_WIDTH;

    return data;
}
/*********************************FillBoard************************************/

size_t FillBoard(size_t chess_board, coordinates_t position)
{
    size_t position_board = ConvertToPosition(position);
    
    chess_board |= LUpRDown(position_board);
    chess_board |= UpDown(position_board);
    chess_board |= RL(position_board);
    chess_board |= RUpLDown(position_board);

    return chess_board;
}

size_t LUpRDown(size_t Position)
{
    size_t i = 0;
    size_t result = 0;
    size_t mask = MaskLUpRDown();
    coordinates_t coord_position = ConvertToCoordinates(Position);
    
    if (coord_position.x > coord_position.y )
    {
       result = Position - OFFSET_D_LR *(Position / BOARD_WIDTH);
       
       for(i = 0; i < result; i++)
       {
          mask ^= ONE << (OFFSET_D_LR * i);
       }
       
       mask >>= result;
        
       return mask;
    }

    result = Position - OFFSET_D_LR *(Position % BOARD_WIDTH);
    mask >>= result;

    return mask;
}


size_t RUpLDown(size_t Position)
{
    size_t i = 0;
    size_t result = 0;
    size_t mask = MaskRUpLDown();

    result = Position - OFFSET_D_RL *(Position / BOARD_WIDTH);
   
    if (result >= BOARD_WIDTH)
    {
        while (Position % BOARD_WIDTH != OFFSET_D_RL)
        {
            Position--;
        }

        mask >>= (Position / BOARD_WIDTH)*OFFSET_D_RL;

        return mask;
    }
          
    for (i = 1; i < BOARD_WIDTH - (result); i++)
    {
        mask ^= ONE << (OFFSET_D_RL * i);
    }
    
    mask <<= OFFSET_D_RL - result ;

    return mask;
}

size_t UpDown(size_t Position)
{
    size_t result = 0;
    size_t mask = MaskUpDown();
   
    result = Position - ((Position / BOARD_WIDTH) * BOARD_WIDTH);
    mask >>= result;

    return mask;
}

size_t RL(size_t Position)
{
    size_t mask = MaskRL();
    mask >>= (Position -(Position % BOARD_WIDTH));
 
    return mask;
}

/*******************************Masks******************************************/
static size_t MaskLUpRDown()
{
    size_t i = 0;
    size_t mask = 0;

    for (i = 0; i < BOARD_WIDTH; i++)
    {
        mask |= ONE << (OFFSET_D_LR * i);
    }
    
    return mask;
}

static size_t MaskRL()
{
    size_t i = 0;
    size_t mask = 0;

    for (i = 0; i < BOARD_WIDTH; i++)
    {
        mask |= (ONE << (63 - i));
    }

    return mask;
}

static size_t MaskUpDown()
{
    size_t i = 0;
    size_t mask = 0;

    for (i = 1; i < BOARD_WIDTH + 1; i++)
    {
        mask |= (ONE << (i * BOARD_WIDTH - 1));
    }

    return mask; 
}

static size_t MaskRUpLDown()
{
    size_t i = 0;
    size_t mask = 0;

    for (i = 1; i < BOARD_WIDTH + 1; i++)
    {
        mask |= ONE << (OFFSET_D_RL * i);
    }
    
    return mask;
}

/* gd eight_queen.c -o queen -Wl,-rpath=../ds -L../ds -lds -I../ds/include */

/*
void PrintBinary(size_t num)
{
    if (0 == num)
    {
        return;
    }

    PrintBinary(num >> 1);
    printf("%lu", num & 1);
}

void PrintBinary1(size_t num)
{
    if (0 == num)
    {
        printf("0\n");
        return;
    }
    
    PrintBinary(num);
    printf("\n");
}

*/