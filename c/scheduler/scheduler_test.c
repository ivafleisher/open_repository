#include <stdio.h>/*printf*/
#include "scheduler.h"

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"


static void TestCreateDestroy       ();
static void TestAddRemove           ();
static void TestRUN                 ();

static void VerifySize              (size_t result, size_t expected);
static void VerifyIsEmpty           (int result, int expected);

/*******************************************************************************
 *                                  MAIN                                       *
 ******************************************************************************/
int main()
{
    TestCreateDestroy();
    TestAddRemove();
    TestRUN();
    return 0;
}

 /**********************************CREAYE DESTROY*****************************/
int Func(void*param)
{
    printf("In FUNC\n");
    return 1;
    (void)param;
}
int Func2(void*param)
{
    printf("In FUNC2\n");
    return 1;
    (void)param;
}
int Func3(void*param)
{
    printf("In FUNC3\n");
    return 1;
    (void)param;
}
int Func4(void*param)
{
    printf("In Func4\n");
    return 1;
    (void)param;
}
int Func5(void*param)
{
    printf("In FUNC5 Add func\n");
    SchedAdd((sched_t*)param, Func4, NULL,1, time(NULL)+2);
    return 1;
    (void)param;
}
 /*****************************************************************************/

static void TestCreateDestroy()
{
    sched_t *scheduler = SchedCreate();

    printf(KCYN"TestCreate\n"KWHT);

    if (NULL != scheduler)
    {
        printf(KGRN"GOOD.CreatDestroy.\n"KWHT);
    }
    else
    {
        printf(KRED"Problem.CreatDestroy.\n"KWHT);
    }

    VerifySize(SchedSize(scheduler), 0);
    VerifyIsEmpty(SchedIsEmpty(scheduler), 1);
    SchedDestroy(scheduler);
}


static void TestAddRemove()
{
    sched_t *scheduler = SchedCreate();
    ilrd_uid_t uid = {0};

    printf(KCYN"TestAddRemove\n"KWHT);

    VerifySize(SchedSize(scheduler), 0);
    VerifyIsEmpty(SchedIsEmpty(scheduler), 1);

    SchedAdd(scheduler, Func, NULL,3, time(NULL)+2);
    uid = SchedAdd(scheduler, Func, NULL, 3, time(NULL)+2);

    VerifySize(SchedSize(scheduler), 2);
    VerifyIsEmpty(SchedIsEmpty(scheduler), 0);

    SchedRemove(scheduler,uid);
    VerifySize(SchedSize(scheduler), 1);
    VerifyIsEmpty(SchedIsEmpty(scheduler), 0);

    SchedDestroy(scheduler);
}

static void TestRUN()
{
    sched_t *scheduler = SchedCreate();
    ilrd_uid_t uid = {0};

    printf(KCYN"TestRUN\n"KWHT);

    VerifySize(SchedSize(scheduler), 0);
    VerifyIsEmpty(SchedIsEmpty(scheduler), 1);

    SchedAdd(scheduler, Func, NULL,1, time(NULL)+2);
    SchedAdd(scheduler, Func2, NULL,1, time(NULL)+3);
    SchedAdd(scheduler, Func3, NULL,1, time(NULL)+4);
    SchedAdd(scheduler, Func5, scheduler,4, time(NULL)+5);
    SchedAdd(scheduler, SchedStop, scheduler,8, time(NULL)+10);

    VerifySize(SchedSize(scheduler), 5);
    VerifyIsEmpty(SchedIsEmpty(scheduler), 0);
    SchedRun(scheduler);


    SchedDestroy(scheduler);
}


 /**********************************VERIFY*************************************/


 static void VerifySize(size_t result, size_t expected)
 {
    if ( result == expected)
    {
        printf(KGRN"GOOD.VerifySize.\n"KWHT);
    }
    else
    {
        printf(KRED"Problem.VerifySize.\n"KWHT);
    }
 }

  static void VerifyIsEmpty(int result, int expected)
 {
    if ( result == expected)
    {
        printf(KGRN"GOOD.VerifyIsEmpty.\n"KWHT);
    }
    else
    {
        printf(KRED"Problem.VerifyIsEmpty.\n"KWHT);
    }
 }
