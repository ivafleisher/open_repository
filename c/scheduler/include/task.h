
#ifndef __TASK_H__
#define __TASK_H__

/*******************************************************************************
                                 * API
                              
 ******************************************************************************/

#include <stddef.h> /* size_t */
#include <time.h> /* time_t */
#include <unistd.h> /* pid_t */
#include <sys/types.h> /* pid_t */
#include "uid.h" /* ilrd_uid_t */

typedef int (*actionf_t)(void* param);

typedef struct task task_t;

/*
 * Input: pointer to func, void pointer of param , frequency , offset of strart .
 * Process: Create a new task.
 * Return:pointer to new task;
 */
task_t *TaskCreate(actionf_t action, void * param, size_t frequency, time_t startpoint);
/*
 * Input: pointer to task.
 * Process: destroy task(free memory).
 * Return:none;
 */
void  TaskDestroy(task_t *task);
/*
 * Input: pointer to task.
 * Process: Get UID of the task.
 * Return:return UID;
 */
ilrd_uid_t TaskGetUID(task_t *task);
/*
 * Input: pointer to task.
 * Process: Get execution time .
 * Return:return execution time;
 */
time_t TaskGetExecTime(task_t *task);

/*
 * Input: pointer to task.
 * Process: Update scheduler time.
 * Return:none;
 */
void TaskRescheduler(task_t *task);
/*
 * Input: pointer to task.
 * Process: execute the function with the parameter.
 * Return:return status of the function;
 */
int TaskExecute(task_t *task);



#endif /* __TASK_H__ */
