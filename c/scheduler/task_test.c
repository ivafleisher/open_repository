#include <stdio.h>/*printf*/
#include "task.h"

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"


static void TestCreate          (void);
static void TestFunc(void);



/*******************************************************************************
 *                                  MAIN                                       *
 ******************************************************************************/
int main()
{
 
   TestCreate();
    TestFunc();
    return 0;
}

int Func(void*param)
{
    printf("In FUNC\n");
    return 0;
    (void)param;
}

static void TestCreate(void)
{
   task_t *task =TaskCreate(Func, NULL, 3, 1);

    printf(KCYN"TestCreate\n"KWHT);
    if (NULL != task)
    {
        printf(KGRN"GOOD.CreatDestroy.\n"KWHT);
    }
    else
    {
        printf(KRED"Problem.CreatDestroy.\n"KWHT);
    }
    
    TaskDestroy(task);
}

static void TestFunc(void)
{
    task_t *task =TaskCreate(Func, NULL, 3, 1);
    ilrd_uid_t get_uid = {0};
    printf(KCYN"TestFunc\n"KWHT);
   
    printf(KMAG"TaskGetUID\n"KWHT);
    get_uid = TaskGetUID(task);
    printf("get_uid %ld %d %ld\n", get_uid.timestamp, get_uid.pid,get_uid.counter);
    
    printf(KMAG"TaskExecute\n"KWHT);
    TaskExecute(task);
   
    printf(KMAG"TaskRescheduler\n"KWHT);
    printf("before time %ld\n",TaskGetExecTime(task));
    TaskRescheduler(task);
    
    printf("after time %ld\n",TaskGetExecTime(task));
    TaskDestroy(task);
}


