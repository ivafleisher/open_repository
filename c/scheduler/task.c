#include <stdlib.h> /* malloc */
#include"task.h"

struct task 
{
    ilrd_uid_t uid;
    actionf_t action;
    size_t frequency;
    time_t exec_time;
    void *param;
};
/*******************************************************************************/

task_t *TaskCreate(actionf_t action, void * param, size_t frequency,
                                                    time_t startpoint)
{
    task_t *task = malloc(sizeof(task_t));

    if (NULL == task)
    {
        return NULL;
    }

    task->param = param;
    task->action = action;
    task->frequency = frequency;
    task->exec_time = startpoint;
    task->uid = UIDCreate();

    return task;
}
/*******************************************************************************/
void  TaskDestroy(task_t *task)
{
    free(task);
    task = NULL;
}
/*******************************************************************************/

ilrd_uid_t TaskGetUID(task_t *task)
{
    return task->uid;
}
/*******************************************************************************/

int TaskExecute(task_t *task)
{
    return task->action(task->param);
}
/*******************************************************************************/

void TaskRescheduler(task_t *task)
{
     task->exec_time += task->frequency;
}
/*******************************************************************************/
time_t TaskGetExecTime(task_t *task)
{
    return task->exec_time;
}
/*******************************************************************************/


