/*******************************************************************************
                            *FUNCTIONS  SCHEDULER*
 ******************************************************************************/
#include <assert.h>     /*  assert      */
#include <stdlib.h>     /* malloc, free */
#include "task.h"       /*  task_t      */
#include "pqueue.h"     /*  pqueue_t    */
#include "scheduler.h"  /*  sched_t     */

#define ASSERT_NOT_NULL(x) (assert(NULL != x));
/******************************************************************************/
struct sched
{
    pqueue_t *pqueue;
    int is_run;
};
/*************************SUPPORT FUNC*****************************************/

static int CompareExecTime ( void* task,  void *com_task )
{
    return (TaskGetExecTime((task_t *)(com_task)) - TaskGetExecTime((task_t *)(task)));
}
static int IsSameUID(void *first, void *second_uid)
{
    return UIDIsSame(TaskGetUID((task_t *)first),*((ilrd_uid_t*)second_uid));
}
/******************************************************************************/

sched_t *SchedCreate(void)
{
    sched_t *scheduler = malloc(sizeof(sched_t));
    if ( NULL == scheduler)
    {
        return NULL;
    }

    scheduler->pqueue = PQCreate(CompareExecTime);
    if ( NULL == scheduler->pqueue)
    {
        free(scheduler);
        scheduler = NULL;

        return NULL;
    }

    scheduler->is_run = 0;

    return scheduler;
}
/******************************************************************************/

void  SchedDestroy(sched_t *schedule)
{
    ASSERT_NOT_NULL(schedule)

    SchedClear(schedule);

    PQDestroy(schedule->pqueue);

    free(schedule);
    schedule = NULL;
}
/******************************************************************************/

int SchedIsEmpty(const sched_t *schedule)
{
    ASSERT_NOT_NULL(schedule)

    return PQIsEmpty(schedule->pqueue);
}
/******************************************************************************/

size_t SchedSize(const sched_t *schedule)
{
    ASSERT_NOT_NULL(schedule)

    return PQSize(schedule->pqueue) + schedule->is_run;
}
/******************************************************************************/

void SchedClear(sched_t *scheduler)
{
    ASSERT_NOT_NULL(scheduler)

    while (!PQIsEmpty(scheduler->pqueue))
    {
        TaskDestroy(PQPeek(scheduler->pqueue));
        PQDequeue(scheduler->pqueue);
    }
}
/******************************************************************************/

void SchedRemove(sched_t *scheduler,ilrd_uid_t uid)
{
    ASSERT_NOT_NULL(scheduler)

   TaskDestroy(PQErase(scheduler->pqueue, IsSameUID, &uid));
}
/******************************************************************************/

ilrd_uid_t SchedAdd(sched_t *scheduler, int (*action)(void*param), void *param,
                    size_t freq, time_t exec_time)
{
    task_t *task = NULL;

    ASSERT_NOT_NULL(scheduler)

    task = TaskCreate( action, param, freq, exec_time);
    if ((NULL == task) || (PQEnqueue(scheduler->pqueue, task)))
    {
        return bad_uid;
    }

    return TaskGetUID(task);
}
/******************************************************************************/

int SchedRun(sched_t *scheduler)
{
    time_t start_time = time(NULL);

    ASSERT_NOT_NULL(scheduler)

    scheduler->is_run = 1;

    while(scheduler->is_run && !SchedIsEmpty(scheduler))
    {
        task_t *curr_task = PQPeek(scheduler->pqueue);
        time_t time_to_sleep =  TaskGetExecTime(curr_task)- time(NULL);

        while(time_to_sleep < 0)
        {
            TaskRescheduler(curr_task);
            time_to_sleep =  TaskGetExecTime(curr_task) - time(NULL);
        }

        PQDequeue(scheduler->pqueue);

        while(time_to_sleep = sleep(time_to_sleep))
        {
            sleep(time_to_sleep);
        }

        if (1 != TaskExecute(curr_task))
        {
            TaskDestroy(curr_task);
        }
        else
        {
            TaskRescheduler(curr_task);
            if(PQEnqueue(scheduler->pqueue,curr_task))
            {
                return 1;
            }
        }
    }

    return 0;
}
/******************************************************************************/

int SchedStop(void *scheduler)
{
   ((sched_t *) scheduler)->is_run = 0;

    return 0;
}
/******************************************************************************/
