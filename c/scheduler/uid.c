
/*******************************************************************************
                            *FUNCTIONS PRIORITY QUEUE
 ******************************************************************************/
#include <assert.h>     /* assert       */
#include <stdlib.h>     /* malloc, free */
#include <stdio.h>
#include "uid.h"

ilrd_uid_t bad_uid = {0,0,0};

ilrd_uid_t UIDCreate(void)
{
    ilrd_uid_t uid = {0};
    static size_t counter = 0;

    uid.timestamp = time(NULL);
    if (((time_t) -1) == uid.timestamp)
    {
        return bad_uid;
    }
    
    uid.pid = getpid();
    uid.counter = __sync_fetch_and_add (&counter, 1);


    return uid;
}

int UIDIsSame(ilrd_uid_t uid1, ilrd_uid_t uid2)
{
    return  ((uid1.timestamp == uid2.timestamp)&&
            (uid1.pid == uid2.pid)&&
            (uid1.counter == uid2.counter)&&
            (uid1.tid == uid2.tid));
}
