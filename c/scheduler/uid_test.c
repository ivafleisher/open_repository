#include <stdio.h>/*printf*/
#include "uid.h"

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"


static void TestCreate          (void);
static void VerifyIsSame        (int result, int expect);
void *CreateID                  (void *data);


/*******************************************************************************
 *                                  MAIN                                       *
 ******************************************************************************/
int main()
{
 
   TestCreate();

    return 0;
}

static void TestCreate(void)
{
    int i = 0;
    pthread_t th;
/*     ilrd_uid_t new_uid1 = UIDCreate();
    ilrd_uid_t new_uid2 = UIDCreate(); */
    for ( i = 0; i < 2; ++i)
    {
       pthread_create(&th, NULL, CreateID,NULL);
    }
   
    printf(KCYN"TestCreate\n"KWHT);
    
    printf(KMAG"Not Same\n"KWHT);
    sleep(2);
    
  /*   VerifyIsSame(UIDIsSame(new_uid1, new_uid2), 0);
    VerifyIsSame(UIDIsSame(new_uid1, bad_uid), 0);
    
    printf(KMAG"Same\n"KWHT);
    VerifyIsSame(UIDIsSame(new_uid1, new_uid1), 1); */
}


/********************************VERIFY****************************************/
static void VerifyIsSame(int result, int expect)
{
    if(result == expect)
    {
        printf(KGRN"GOOD.VerifyIsSame\n"KWHT);
    }
    else
    {
        printf(KRED"PROBLEM.VerifyIsSame\n"KWHT);
    }
}

void *CreateID(void *data)
{
    ilrd_uid_t new_uid1 = UIDCreate();
    return NULL;
}