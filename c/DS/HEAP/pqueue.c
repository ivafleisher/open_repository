#include "pqueue.h"
#include "heap.h"

struct p_queue
{
  void *smth;
};

/*
* Input:  pointer to comparator function that will set default ordering of 
  all elements that will be added.
* Process: allocate memory for empty list
* Return: pointer to queue if successful, NULL if failed
*/
pqueue_t *  PQCreate(pqcompare_t comparator)
{
    return (pqueue_t *)HeapCreate((heap_cmp_t )comparator);
}

/*
* Input:  pointer to queue
* Process: destroy the whole queue (free memory)
* Return: none
*/
void PQDestroy(pqueue_t *pqueue)
{
    HeapDestroy((heap_t *)pqueue);
}

/*
* Input:  pointers to the queue and data to be inserted
* Process: adds data to the queue preserving internal order.
* Return: "0 on success, all other values are a failure
*/
int PQEnqueue(pqueue_t *pqueue, void* data)
{
    return HeapPush((heap_t*)pqueue, data);
}

/*
* Input:  pointer to the queue
* Process: Checks to see if queue is empty
* Return: "1" if empty "0" if not
*/
int PQIsEmpty(const pqueue_t *pqueue)
{
    return HeapIsEmpty((heap_t* )pqueue);
}

/*
 * Input: pointer to a queue
 * Process: The function counts the number of elements in the given queue
 * Return: number of elements
 */
size_t PQSize(const pqueue_t *pqueue)
{
    return HeapSize((heap_t*)pqueue);
}

/*
 * Input: Queue
 * Process: Observe data at front of queue.
 * Return: Pointer to data.
 */
void *PQPeek(pqueue_t *pqueue)
{
    return HeapPeek((heap_t*) pqueue);
}

/*
*Input:  pointer to queue ;
*Process: Pop element at the begin;
*/
void PQDequeue(pqueue_t *pqueue)
{
    HeapPop((heap_t*)pqueue);
}

/*
*Input:  Queue, match function, key that is to be looked for;
*Process: Erase the first element that contains the key from the queue
*Return: A pointer to the data that was stored in that element, else returns NULL
*/
void *PQErase(pqueue_t *pqueue, ismatch_t ismatch, void *key)
{
    return HeapRemove((heap_t* )pqueue,(int(*)(const void* data, const void* param)) ismatch, key);
}

/*
* Input: Pointer to the queue to clear
* Process: Removes all elements int the queue, but leaves the queue pointer intact
*/
void PQClear(pqueue_t *pqueue)
{
    while (!HeapIsEmpty((heap_t* )pqueue))
    {
        HeapPop((heap_t*) pqueue);
    }
}
