
#include <stdio.h>/*printf*/
#include "pqueue.h"

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

static void CreatDestroy        ();
static void TestEnqErase        ();
static void TestClean           ();
static void TestEnqDeq          ();
static int CompareInt           (void *value, void *value2);
static void VerifyEmpty         (size_t result, size_t expect);
static void VerifyCount         (size_t result_size, size_t expect_size);

/*******************************************************************************
 *                                  MAIN                                       *
 ******************************************************************************/
int main()
{
    CreatDestroy();
    TestEnqErase();
    TestEnqDeq();
    TestClean();

    return 0;
}

static int CompareInt(void *value, void *value2)
{
    return (*(int *)value - *( int *)value2);
}
static int CompareInt2(void *value, void *value2)
{
    return (*(int *)value == *( int *)value2);
}


static void CreatDestroy()
{
    pqueue_t *pq = PQCreate(CompareInt);
    
    printf(KCYN"CreatDestroy\n"KWHT);

    if (NULL != pq)
    {
        printf(KGRN"GOOD.CreatDestroy.\n"KWHT);
    }
    else
    {
        printf(KRED"Problem.CreatDestroy.\n"KWHT);
    }
    
    VerifyEmpty(PQIsEmpty(pq), 1);
    VerifyCount(PQSize(pq), 0);

    PQDestroy(pq);
}

/******************************** EnQ Erase ***********************************/

static void TestEnqErase()
{
    pqueue_t *pq = PQCreate(CompareInt);
    int data[] = {5,2,4,3,1};

    size_t size = sizeof(data)/sizeof(data[0]);
    size_t i = 0;
    
    printf(KCYN"TestEnqErase\n"KWHT);
    printf(KMAG"ENQUEUE\n"KWHT);

    for (i = 0; i < size; i++)
    {
        PQEnqueue(pq, &data[i]);
        
        VerifyCount(PQSize(pq),i +1);
    }
    
    VerifyEmpty(PQIsEmpty(pq), 0);
    
    printf(KMAG"ERASE\n"KWHT);
    
    for (i = 0; i < size; i++)
    {   
        printf("DATA %d", *((int *)PQErase(pq, CompareInt2, &data[i])));
       
        VerifyCount(PQSize(pq),size - i - 1);
    } 

    PQDestroy(pq);
} 


/********************************EnQ DeQ*********************************/

static void TestEnqDeq()
{
    pqueue_t *pq = PQCreate(CompareInt);
    int data[] = {5,2,4,3,1};

    size_t size = sizeof(data)/sizeof(data[0]);
    size_t i = 0;
    
    printf(KCYN"TestEnqDeq\n"KWHT);
    printf(KMAG"ENQUEUE\n"KWHT);

    for (i = 0; i < size; i++)
    {
        PQEnqueue(pq, &data[i]);
        
        VerifyCount(PQSize(pq),i +1);
    }
    
    VerifyEmpty(PQIsEmpty(pq), 0);
    
    printf(KMAG"DEQUEUE\n"KWHT);
    
    for (i = 0; i < size; i++)
    {   
        printf("DATA %d", *((int *)PQPeek(pq)));

        PQDequeue(pq);
        VerifyCount(PQSize(pq),size - i - 1);
    } 

    PQDestroy(pq);
} 

/********************************Clean *********************************/

static void TestClean()
{
    pqueue_t *pq = PQCreate(CompareInt);
    int data[] = {5,2,4,3,1};

    size_t size = sizeof(data)/sizeof(data[0]);
    size_t i = 0;
    
    printf(KCYN"TestClean\n"KWHT);
    printf(KMAG"INSERT\n"KWHT);

    for (i = 0; i < size; i++)
    {
        PQEnqueue(pq, &data[i]);
        
        VerifyCount(PQSize(pq),i +1);
    }
    
    VerifyEmpty(PQIsEmpty(pq), 0);
    
    printf(KMAG"CLEAN\n"KWHT);
    
    PQClear(pq);
    VerifyEmpty(PQIsEmpty(pq), 1);
    VerifyCount(PQSize(pq), 0);

    PQDestroy(pq);
} 
/********************************VERIFY****************************************/
static void VerifyCount(size_t result_size, size_t expect_size)
{
    if(result_size == expect_size)
    {
        printf(KGRN"GOOD.CapacitySize.size %lu\n"KWHT,
        result_size);
    }
    else
    {
        printf(KRED"PROBLEM.CapacitySize.size %lu\n"KWHT,
        result_size);
    }
}

static void VerifyEmpty(size_t result, size_t expect)
{
    if (result == expect)
    {
        printf(KGRN"GOOD.VerifyEmpty\n"KWHT);
    }
    else
    {
        printf(KRED"PROBLEM.VerifyEmpty.size %lu\n"KWHT,
        result);
    }
}
