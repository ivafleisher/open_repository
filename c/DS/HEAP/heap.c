/******************************************************************************
                            * HEAP *
******************************************************************************/

#include <stddef.h>	/* size_t */
#include <stdlib.h> /* malloc */
#include "vector.h" /* dynamic vector functions*/
#include "heap.h"
/******************************************************************************/
struct heap 
{
	vector_t* vector;
    heap_cmp_t cmp;
};
/******************************************************************************/

static void HeapifyUp           (heap_t *heap ,int index);
static void HeapfyDown          (heap_t *heap, int  index);
static void Swap                (heap_t *heap,size_t index, size_t index2);

static size_t ParentIndex       (int index);
static size_t LeftChildIndex    (int index);
static size_t RightChildIndex   (int index);
static size_t MaxChild          (const heap_t *heap, int index);

static void* LeftCh             (const heap_t *heap,int index);
static void* Parent             (const heap_t *heap,int index);
static void* RightCh            (const heap_t *heap,int index);
static void* GetData            (const heap_t *heap,int index);
/******************************************************************************/

heap_t* HeapCreate(heap_cmp_t cmp)
{
    heap_t *heap = malloc(sizeof(heap_t));
    if (NULL == heap)
    {
        return NULL;
    }

    heap->vector = VectorCreate(3);
    if (NULL == heap->vector)
    {
        free(heap);
        heap = NULL;

        return NULL;
    }

    heap->cmp = cmp;

    return heap;
}

/******************************************************************************/

void HeapDestroy(heap_t* heap)
{
    VectorDestroy(heap->vector);
    
    free(heap);
    heap = NULL;
}
/******************************************************************************/

int HeapPush(heap_t* heap, void* data)
{
    int index = HeapSize(heap);
    if(VectorPushBack(heap->vector, data))
    {
        return 1;
    }

    HeapifyUp(heap ,index);
    
    return 0;
}
/******************************************************************************/

void HeapPop(heap_t* heap)
{
    size_t size = HeapSize(heap);
    size_t index = 0;

    Swap(heap,0, size -1);
    VectorPopBack(heap->vector);

    HeapfyDown(heap, index);
}
/******************************************************************************/

void* HeapPeek(const heap_t* heap)
{
    return VectorGetElement(heap->vector, 0);
}
/******************************************************************************/

size_t HeapSize(const heap_t* heap)
{
    return VectorSize(heap->vector);
}
/******************************************************************************/

int HeapIsEmpty(const heap_t* heap)
{
    return VectorIsEmpty(heap->vector);
}
/******************************************************************************/

void *HeapRemove(heap_t* heap, int(*is_match)(const void* data, const void* param), void* param)
{
    size_t index = 0;
    void *data_to_return = NULL;
    size_t size = HeapSize(heap);

    while (index != size)
    {
        if (is_match(GetData(heap, index), param))
        {
            data_to_return = GetData(heap,index);
            Swap(heap,index, size -1);

            VectorPopBack(heap->vector);

            HeapfyDown(heap, index);
            HeapifyUp(heap, index);
            return data_to_return;
        }
      
        ++index;   
    }

    return NULL;
}

/****************************SUPPORT FUNC**************************************/

static void* Parent(const heap_t *heap,int index)
{
    index =  (index - 1) / 2;
    return VectorGetElement(heap->vector, index);
}

static void* GetData(const heap_t *heap,int index)
{
    return VectorGetElement(heap->vector, index);
}

static void* LeftCh(const heap_t *heap, int index)
{
    index =  2 * index + 1;
    return VectorGetElement(heap->vector, index);
}

static void* RightCh(const heap_t *heap,int index)
{
    index =  2 * index + 2;
    return VectorGetElement(heap->vector, index);
}

static void Swap(heap_t *heap,size_t index, size_t index2)
{
    void *first_data =GetData(heap,index);
    void *second_data = GetData(heap,index2);
  
    VectorSetElement(heap->vector,index, second_data);
    VectorSetElement(heap->vector,index2, first_data);
}

static void HeapifyUp(heap_t *heap ,int index)
{
    int parent =0;
    while(index > 0 && 0 > heap->cmp(Parent(heap,index),GetData(heap,index)))
    {
        parent = (index - 1) / 2;
        Swap(heap,ParentIndex(index) ,index);

        index = parent;
    }
}

static size_t RightChildIndex(int index)
{
    return (2 * index + 2);
}

static size_t LeftChildIndex(int index)
{
    return (2 * index + 1);
}

static size_t ParentIndex(int index)
{
    return (index - 1) / 2;
}

static size_t MaxChild (const heap_t *heap,int index)
{
    if (RightChildIndex(index) >=  HeapSize(heap) )
    {
        return LeftChildIndex(index);
    }
    else if (heap->cmp(RightCh(heap,index), LeftCh(heap, index)) < 0)
    {
        return LeftChildIndex(index);
    }

    return RightChildIndex(index);
}


static void HeapfyDown( heap_t *heap, int  index)
{
    size_t index_child = MaxChild (heap, index);
    size_t size = HeapSize(heap);

    while ((index_child < size) &&
    (heap->cmp(GetData(heap,index), GetData(heap, index_child)) < 0))
    {
        Swap(heap,index, MaxChild(heap, index));

        index = index_child;
        index_child = MaxChild (heap, index);
    }
}
