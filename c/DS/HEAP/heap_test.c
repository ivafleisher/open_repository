#include <stdio.h>/*printf*/
#include "heap.h"

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

static void CreatDestroy(void);
static void TestPush(void);
static void TestRemove(void);

static void VerifyEmpty(int result , int expected);
static void VerifySize(size_t result, size_t expected);
int CompareValues(const void *value, const void *value2);
int IsMatch(const void *value, const void *value2);

int main()
{
  /*  
    */
   CreatDestroy();
    TestPush(); 
    TestRemove();


    /* 
    */
    return 0;
}
/********************************CREATE DESTROY********************************/
static void CreatDestroy(void)
{
    heap_t *heap =  HeapCreate(CompareValues);
    
    printf(KCYN"CreatDestroy\n"KWHT);

    if (NULL != heap)
    {
        printf(KGRN"GOOD.CreatDestroy.\n"KWHT);
    }
    else
    {
        printf(KRED"Problem.CreatDestroy.\n"KWHT);
    }
    
    VerifyEmpty(HeapIsEmpty(heap), 1);
    VerifySize(HeapSize(heap), 0);

    HeapDestroy(heap);
}


static void TestPush(void)
{
    heap_t *heap =  HeapCreate(CompareValues);
    int data[] = {9,7,8,6,5,4,3,2,1};
    size_t size = sizeof(data)/sizeof(data[0]);
    size_t i = 0;
    printf(KCYN"TestPush\n"KWHT);
    
    for (i = 0; i <size; i++)
    {
        HeapPush(heap, &data[i]);
        VerifySize(HeapSize(heap), i + 1);
        printf("ROOT %d\n",*(int *)HeapPeek(heap));
    }

    printf("*************REMOVE********** \n");

    for (i = 0; i <size; i++)
    {
        printf("REMOVE %d\n",*(int *)HeapRemove(heap,IsMatch ,&data[i]));
        VerifySize(HeapSize(heap), size - i -1);
        printf("ROOT %d\n",*(int *)HeapPeek(heap));
    }

    VerifyEmpty(HeapIsEmpty(heap), 1);

    HeapDestroy(heap);
}

static void TestRemove(void)
{
    heap_t *heap =  HeapCreate(CompareValues);
    int data[] = {9,7,8,6,5,4,3,2,1};
    size_t size = sizeof(data)/sizeof(data[0]);
    size_t i = 0;
    printf(KCYN"TestPush\n"KWHT);
    
    for (i = 0; i <size; i++)
    {
        HeapPush(heap, &data[i]);
        VerifySize(HeapSize(heap), i + 1);
        printf("ROOT %d\n",*(int *)HeapPeek(heap));
    }

    printf("*************REMOVE********** \n");

    for (i = 0; i <size; i++)
    {
        int value = 0;
        printf("BEFORE: ROOT %d\n",value= *(int *)HeapPeek(heap));
        printf("REMOVE %d\n",*(int *)HeapRemove(heap,IsMatch ,&value));
        printf("AFTER :ROOT %d\n",value= *(int *)HeapPeek(heap));
        printf("*********************** \n");

        VerifySize(HeapSize(heap), size - i -1);
    }

    VerifyEmpty(HeapIsEmpty(heap), 1);

    HeapDestroy(heap);
}
/******************************************************************************/

static void VerifyEmpty(int result , int expected)
{
    if (result != expected)
    {
        printf(KRED"FAIL.VerifyEmpty"KWHT);
    }
}

static void VerifySize(size_t result, size_t expected)
{
    if (result != expected)
    {
        printf(KRED"FAIL.VerifySize"KWHT);
    }
}

int CompareValues(const void *value, const void *value2)
{
    return *(int *)value - *(int *)value2;
}

int IsMatch(const void *value, const void *value2)
{
    return 0 == CompareValues(value, value2);
}