#include <stdio.h>/*printf*/
#include <assert.h>/*assert*/
#include <stdlib.h>/*malloc*/
#include <string.h>/*memcpy*/

#define ASSERT_STACK_NOT_NULL assert(NULL != stack);
typedef struct stack_struct
{
    void *head;
    void *end;
    void *top;
    size_t element_size;
}stack_t;

stack_t *StackCreate(size_t max_number_of_elements, size_t element_size)
{
    stack_t *new_stack = malloc(sizeof(stack_t) + max_number_of_elements * element_size);

    assert(0 < max_number_of_elements);
    assert(0 < element_size);

    
    new_stack->head = new_stack + 1;
    new_stack->top = new_stack->head;
    new_stack->end = (char*)new_stack->head + max_number_of_elements * element_size;
    new_stack->element_size = element_size;

    return new_stack;
}

void StackDestroy(stack_t *stack)
{
    ASSERT_STACK_NOT_NULL
    free(stack);
    stack = NULL;
}

/* -1 full*/
int StackPush(stack_t *stack, const void *element)
{
    ASSERT_STACK_NOT_NULL
    assert(NULL != element);

    if (stack->top == stack->end)
    {
        return -1;
    }
    memcpy(stack->top, element, stack->element_size);
    stack->top = (char*)stack->top + stack->element_size;

    return 0;
}

void *StackPop(stack_t *stack)
{
    ASSERT_STACK_NOT_NULL
    if (stack->top == stack->head)
    {
        return NULL;
    }
    return stack->top = (char *)stack->top - stack->element_size;
}

const void *StackPeek(const stack_t *stack)
{
    ASSERT_STACK_NOT_NULL
    if (stack->top == stack->head)
    {
        return NULL;
    }

    return (char *)stack->top - stack->element_size;
}

size_t StackSize(const stack_t *stack)
{
    ASSERT_STACK_NOT_NULL
    return ((char*)stack->top - (char*)stack->head)/stack->element_size;
}

/*1-is empty ,0- is not empty*/
int StackIsEmpty(const stack_t *stack)
{
    ASSERT_STACK_NOT_NULL
    return (stack->top == stack->head);
}
