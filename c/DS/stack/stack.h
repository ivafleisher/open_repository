#ifndef __STACK_H__
#define __STACK_H__

#include <stddef.h>     /* size_t */

/* Stack is a LIFO data structure */

/* type of a stack */
typedef struct stack_struct stack_t;


/* returns a new instance of a stack of elements of specified size */
stack_t *StackCreate(size_t max_number_of_elements, size_t element_size);
/* deletes the instance of stack and frees all memory (if any allocated) */
void StackDestroy(stack_t *stack);

/* adds the element VLAD to the top of the stack. Returns error code if stack is full*/
int StackPush(stack_t *stack, const void *element);
/* returns the last added element and removes it from the top of the stack */
void *StackPop(stack_t *stack);
/* returns a pointer to the last added element without removing it */
const void *StackPeek(const stack_t *stack);

/* returns the number of elements on the stack */
size_t StackSize(const stack_t *stack);
/* returns 1 if stack is empty and 0 if it`s not */
int StackIsEmpty(const stack_t *stack);

#endif
