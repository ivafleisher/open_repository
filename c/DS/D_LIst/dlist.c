
/*******************************************************************************
                                 *  OL85 API
                                 * version 1.0
 ******************************************************************************/
#include <stdlib.h> /* malloc, free */
#include<assert.h>  /* assert */
#include<stdio.h>  /* assert */
#include "dlist.h"

#define ASSERT_NOT_NULL(x) (assert(NULL != x));
enum directions{PREV, NEXT, AMOUNT_OF_DIR};

typedef struct node
{
    struct node *dir[AMOUNT_OF_DIR];
    void * data;
}node_t;

struct dlist 
{
    node_t head;
    node_t tail;
};
/***************************SUPPORT FUNC***************************************/
static node_t * NodeCreate(void *data, node_t *prev, node_t *next)
{
    node_t *node = malloc(sizeof(node_t));
    if (NULL == node)
    {
        return NULL;
    }

    node->data = data;
    node->dir[NEXT] = next;
    node->dir[PREV] = prev;

    return node;
}
/******************************************************************************/
dlist_t*  DListCreate(void)
{
    dlist_t* dlist = malloc(sizeof(dlist_t));
    
    if (NULL == dlist)
    {
        return NULL;
    }

    dlist->head.dir[PREV] = NULL;
    dlist->head.dir[NEXT] = &(dlist->tail);
    dlist->tail.dir[PREV] = &(dlist->head);
    dlist->tail.dir[NEXT] = NULL;

    return dlist;
}
/******************************************************************************/

void DListDestroy(dlist_t *list)
{
    iter_t begin = NULL;
    iter_t end = NULL;
    ASSERT_NOT_NULL(list)

    begin = DListBegin(list);
    end = DListEnd(list);

    while (!DListIsSame(begin, end))
    {
        iter_t next = DListNext(begin);
        free(begin);
        begin = next;
    }
    
    free(list);
    list = NULL;
}

/******************************************************************************/

int DListIsEmpty(const dlist_t *list)
{
    ASSERT_NOT_NULL(list)

    return (list->head.dir[NEXT] == &(list->tail));
}
/******************************************************************************/

static int Count(void *value, void *param)
{
    ++(*(size_t *)param);
    return 0;
}

size_t DListSize(const dlist_t *list)
{     
    iter_t begin = NULL;
    iter_t end = NULL;
    size_t counter = 0;

    ASSERT_NOT_NULL(list)

    begin = DListBegin(list);
    end = DListEnd(list);

    DListForEach(begin, end, Count, &counter);

    return counter;
}
/******************************************************************************/

void *DListGetData(iter_t iter)
{
    ASSERT_NOT_NULL(iter)
    
    return ((node_t *)iter)->data;
}
/******************************************************************************/
iter_t DListInsert(dlist_t *dlist, iter_t iter, void *data)
{
    node_t *curr = NULL;
    node_t *prev = NULL;
    node_t *new_node = NULL;

    ASSERT_NOT_NULL(dlist)
    ASSERT_NOT_NULL(iter)

    curr = (node_t *)iter;
    prev = ((node_t *)iter)->dir[PREV];
    new_node = NodeCreate(data, prev, curr);

    if (NULL == new_node)
    {
        return &dlist->tail;
    }

    prev->dir[NEXT] = new_node;
    curr->dir[PREV] = new_node;
    
    return new_node;
}
/******************************************************************************/

iter_t DListPushFront (dlist_t *dlist, void *data)
{
    ASSERT_NOT_NULL(dlist)

    return DListInsert(dlist, DListBegin(dlist), data);
}
/******************************************************************************/


iter_t DListPushBack (dlist_t *dlist, void *data)
{
    ASSERT_NOT_NULL(dlist)

    return DListInsert(dlist, DListEnd(dlist), data);
}
/******************************************************************************/

void DListPopFront(dlist_t *dlist)
{
    ASSERT_NOT_NULL(dlist)

    DListRemove(DListBegin(dlist));
}
/******************************************************************************/

void DListPopBack(dlist_t *dlist)
{
    ASSERT_NOT_NULL(dlist)

    DListRemove(DListPrev(&(dlist->tail)));
}
/******************************************************************************/

iter_t DListRemove(iter_t iterator)
{
    node_t *next = NULL;
    node_t *prev = NULL;
    
    ASSERT_NOT_NULL(iterator)
   
    next = ((node_t *)iterator)->dir[NEXT];
    prev = ((node_t *)iterator)->dir[PREV];

    free(iterator);
    iterator = NULL;

    prev->dir[NEXT] = next;
    next->dir[PREV] = prev;

    return next;
}
/******************************************************************************/

iter_t DListFind(iter_t from, iter_t to, compare_t compare , void* key)
{
    ASSERT_NOT_NULL(from)
    ASSERT_NOT_NULL(to)
    ASSERT_NOT_NULL(key)

    while (!DListIsSame(from, to) && compare(((node_t *)from)->data, key)!= 0)
    { 
        from = DListNext(from);
    }
    
    return from;
}
/******************************************************************************/

int DListForEach(iter_t from, iter_t to, action_t action, void *param)
{
    int status = 0;
    
    ASSERT_NOT_NULL(from)
    ASSERT_NOT_NULL(to)
    
    while (!DListIsSame(from, to) && !status)
    {
        status = action(DListGetData(from), param);
        from = DListNext(from);
    }

    return status;
}
/******************************************************************************/

iter_t DListBegin(const dlist_t *dlist)
{
    ASSERT_NOT_NULL(dlist)

    return dlist->head.dir[NEXT];
}

/******************************************************************************/

iter_t DListEnd(const dlist_t *dlist)
{
    ASSERT_NOT_NULL(dlist)

    return (iter_t)&(dlist->tail);
}
/******************************************************************************/

iter_t DListNext(iter_t iter)
{
    ASSERT_NOT_NULL(iter)

    return ((node_t *)iter)->dir[NEXT];
}
/******************************************************************************/

iter_t DListPrev(iter_t iter)
{
    ASSERT_NOT_NULL(iter)

    return ((node_t *)iter)->dir[PREV];
}
/******************************************************************************/

int DListIsSame(iter_t iter1, iter_t iter2)
{
    return (iter1 == iter2);
}
/******************************************************************************/

iter_t DListSplice(iter_t from, iter_t to, iter_t where)
{
    node_t * next_where = NULL;
    node_t* prev_from = NULL;
    node_t* prev_to = NULL;

    ASSERT_NOT_NULL(from)
    ASSERT_NOT_NULL(to)
    ASSERT_NOT_NULL(where)

    next_where = (node_t *)DListNext(where);
    prev_from = (node_t *)DListPrev(from);
    prev_to = (node_t *)DListPrev(to);

    ((node_t *)to)->dir[PREV] = prev_from;
    prev_from->dir[NEXT] = to;

    ((node_t *)where)->dir[NEXT] = from;
    ((node_t *)from)->dir[PREV] = where;
    prev_to->dir[NEXT] = next_where;
    next_where->dir[PREV] = prev_to;

    return prev_to;
}

/******************************************************************************/

dlist_t *DListMultifind(iter_t from, iter_t to, compare_t compare,
                                         void *key, dlist_t *dest)
{
    ASSERT_NOT_NULL(from)
    ASSERT_NOT_NULL(to)
    ASSERT_NOT_NULL(key)
    ASSERT_NOT_NULL(dest)

    while (!DListIsSame(from, to) && (from = DListFind(from, to,compare , key)))
    { 
        DListPushBack (dest, DListGetData(from));
        from = DListNext(from);
    }

    return dest;
}



