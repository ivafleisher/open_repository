
#include <stdio.h>/*printf*/
#include "dlist.h"

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

static void TestFind            (); 
static void CreatDestroy        ();
static void TestInsertBefore    ();
static void TestForEach         ();
static void TestSplice          ();
static void TestMultiFind();
static void TestPop();

static int IsBigger             (void *value, void *value2);
static void VerifyElement       (int result_element, int expected_element);
static int AddOnePrint          (void *value, void *param);
static int CompareInt           (void *value, void *value2);
static void VerifyEmpty         (size_t result, size_t expect);
static void VerifyCount         (size_t result_size, size_t expect_size);

/*******************************************************************************
 *                                  MAIN                                       *
 ******************************************************************************/
int main()
{
    /* 
   */
    CreatDestroy();
    TestInsertBefore();
    TestForEach();
    TestFind();
    TestSplice();
    TestMultiFind();
    TestPop();

    return 0;
}
/********************************CREATE DESTROY********************************/
static void CreatDestroy()
{
    dlist_t * list = DListCreate();
    
    printf(KCYN"CreatDestroy\n"KWHT);

    if (NULL != list)
    {
        printf(KGRN"GOOD.CreatDestroy.\n"KWHT);
    }
    else
    {
        printf(KRED"Problem.CreatDestroy.\n"KWHT);
    }
    
    VerifyEmpty(DListIsEmpty(list), 1);
    VerifyCount(DListSize(list), 0);

    DListDestroy(list);
}
/********************************INSERT REMOVE*********************************/

static void TestInsertBefore()
{
    dlist_t * list = DListCreate();
    int data[] = {1,2,3,4,5};
    iter_t iterator = NULL;
    size_t size = sizeof(data)/sizeof(data[0]);
    size_t i = 0;
    
    printf(KCYN"TestInsert\n"KWHT);
    printf(KMAG"INSERT\n"KWHT);

    for (i = 0; i < size; i++)
    {
        iter_t iterator_before = DListBegin(list);
        DListInsert(list,iterator_before, &data[i]);
        
        VerifyCount(DListSize(list),i +1);
    }
    
    VerifyEmpty(DListIsEmpty(list), 0);
    
    printf(KMAG"REMOVE\n"KWHT);
    
    for (i = 0; i < size; i++)
    {
        iterator = DListBegin(list);
        printf("DATA %d", *((int *)DListGetData(iterator)));

        DListRemove(iterator);
        VerifyCount(DListSize(list),size - i-1);
    }

    DListDestroy(list);
} 


/********************************FIND******************************************/
static int CompareInt(void *value, void *value2)
{
    return (*(int *)value - *( int *)value2);
}

static void TestFind()
{
    dlist_t * list = DListCreate();
    iter_t iter_begin = NULL;
    iter_t iter_end = NULL;
    iter_t find = NULL;
    
    int data[] = {1,2,3,4,5}; 
    int find_arr[] = {2,3, 6};
    size_t size = sizeof(data)/sizeof(data[0]);
    size_t i = 0;
    
    printf(KCYN"TestFind\n"KWHT);
    printf(KMAG"INSERT\n"KWHT);

    for (i = 0; i < size; i++)
    {
        DListPushBack (list,  &data[i]);
        
        VerifyCount(DListSize(list),i +1);
    }
    
    iter_begin = DListBegin(list);
    iter_end = DListEnd(list);

    printf(KMAG"Find exsist element\n"KWHT);

    find = DListFind(iter_begin, iter_end, CompareInt , &(find_arr[0]));
    VerifyElement(*((int *)DListGetData(find)), 2);

    printf(KMAG"Find exsist element\n"KWHT);

    find = DListFind(iter_begin, iter_end, CompareInt , &(find_arr[1]));
    VerifyElement(*((int *)DListGetData(find)), 3);
    
    printf(KMAG"Find not exsist element\n"KWHT);
    
    iter_begin = DListBegin(list);
    iter_end = DListEnd(list);

    find = DListFind(iter_begin, iter_end, CompareInt , &(find_arr[2]));
    printf("Is Equal :%d\n",DListIsSame(find, iter_end));

    DListDestroy(list);
}
/********************************FOR EACH**************************************/

static int AddOnePrint(void *value, void *param)
{
    int param_in = *((int *)param);

    printf("Add One to %d  = %d \n",
        *((int *)value), param_in += *((int *)value));
    
    return 0;
}

static void TestForEach()
{
    dlist_t * list = DListCreate();
    iter_t iter_begin = NULL;
    iter_t iter_end = NULL;
    
    int data[] = {1,2,3,4,5}; 
    int one = 1;
    size_t size = sizeof(data)/sizeof(data[0]);
    size_t i = 0;
    
    printf(KCYN"TestForEach\n"KWHT);
    printf(KMAG"INSERT\n"KWHT);

    for (i = 0; i < size; i++)
    {
        DListPushFront(list, &data[i]);
        
        VerifyCount(DListSize(list),i +1);
    }
    
    iter_begin = DListBegin(list);
    iter_end = DListEnd(list);

    printf(KMAG"FOR EACH\n"KWHT);

    DListForEach(iter_begin, iter_end, AddOnePrint,&one);

    DListDestroy(list);
} 
/********************************POP BACK FRONT *******************************/
static void TestPop()
{
    dlist_t * list = DListCreate();
    iter_t iter_begin = NULL;
    iter_t iter_end = NULL;
    
    int data[] = {1,2,3,4,5}; 
    int one = 1;
    size_t size = sizeof(data)/sizeof(data[0]);
    size_t i = 0;
    
    printf(KCYN"TestPop\n"KWHT);
    printf(KMAG"INSERT\n"KWHT);

    for (i = 0; i < size; i++)
    {
        DListPushFront(list, &data[i]);
        
        VerifyCount(DListSize(list),i +1);
    }

    printf(KMAG"POP\n"KWHT);

    for (i = 0; i < size; i++)
    {
        printf("Data %d",*(int *) DListGetData(DListPrev(DListEnd(list))));
        DListPopBack(list);
        
        VerifyCount(DListSize(list), size - i - 1);
    }

    DListDestroy(list);
}
/********************************SPLICE****************************************/
static void TestSplice()
{
    dlist_t * dest = DListCreate();
    dlist_t * source = DListCreate();
    iter_t iter_begin = NULL;
    iter_t iter_end = NULL;
    iter_t from = NULL;
    iter_t to = NULL;
    iter_t where = NULL;
    
    int data1[] = {1,2,3,4,5}; 
    int data2[] = {6,7,8,9,10}; 
    int one = 0;
    size_t size = sizeof(data1)/sizeof(data1[0]);
    size_t i = 0;
    
    printf(KCYN"TestSplice\n"KWHT);
    printf(KMAG"INSERT\n"KWHT);

    for (i = 0; i < size; i++)
    {
        DListPushBack (dest,&data1[i]);
        DListPushBack (source,&data2[i]);
        
        VerifyCount(DListSize(dest),i +1);
    }
   
    iter_begin = DListBegin(dest);
    iter_end = DListEnd(dest);
    DListForEach(iter_begin, iter_end, AddOnePrint,&one);
    
    printf(KMAG"INSERT\n"KWHT);
    
    iter_begin = DListBegin(source);
    iter_end = DListEnd(source);
    DListForEach(iter_begin, iter_end, AddOnePrint,&one);

    from = DListNext(DListBegin(source));
    to = DListPrev(DListEnd(source));
    where = DListBegin(dest);
    
    printf(KMAG"SPLICE\n"KWHT);
    DListSplice(from,to , where);
    
    printf(KMAG"1\n"KWHT);
    iter_begin = DListBegin(source);
    iter_end = DListEnd(source);
    DListForEach(iter_begin, iter_end, AddOnePrint,&one);
    
    printf(KMAG"2\n"KWHT);
    iter_begin = DListBegin(dest);
    iter_end = DListEnd(dest);
    DListForEach(iter_begin, iter_end, AddOnePrint,&one);

    DListDestroy(dest);
    DListDestroy(source);
}

static int IsBigger(void *value, void *value2)
{
    return !(*(int *)value > *( int *)value2);
}

static void TestMultiFind()
{
    dlist_t * dest = DListCreate();
    dlist_t * source = DListCreate();
    iter_t iter_begin = NULL;
    iter_t iter_end = NULL;
    
    int data1[] = {1,2,3,4,5}; 
    int key = 2;
    int param =0;
    size_t size = sizeof(data1)/sizeof(data1[0]);
    size_t i = 0;
    
    printf(KCYN"TestMultiFind\n"KWHT);
    printf(KMAG"INSERT\n"KWHT);

    for (i = 0; i < size; i++)
    {
        DListPushBack (source,&data1[i]);
        VerifyCount(DListSize(source),i +1);
    }
   
    iter_begin = DListBegin(source);
    iter_end = DListEnd(source);
    printf(KMAG"IsBigger then 2\n"KWHT);
    DListMultifind(iter_begin, iter_end, IsBigger,&key, dest);

    iter_begin = DListBegin(dest);
    iter_end = DListEnd(dest);

    DListForEach(iter_begin, iter_end, AddOnePrint,&param);

    DListDestroy(dest);
    DListDestroy(source);
}

/********************************VERIFY****************************************/
static void VerifyCount(size_t result_size, size_t expect_size)
{
    if(result_size == expect_size)
    {
        printf(KGRN"GOOD.CapacitySize.size %lu\n"KWHT,
        result_size);
    }
    else
    {
        printf(KRED"PROBLEM.CapacitySize.size %lu\n"KWHT,
        result_size);
    }
}

static void VerifyEmpty(size_t result, size_t expect)
{
    if (result == expect)
    {
        printf(KGRN"GOOD.VerifyEmpty\n"KWHT);
    }
    else
    {
        printf(KRED"PROBLEM.VerifyEmpty.size %lu\n"KWHT,
        result);
    }
}

static void VerifyElement(int result_element, int expected_element)
{
    if (result_element == expected_element )
    {
        printf(KGRN"GOOD.Element\n"KWHT);
    }
    else
    {
        printf(KRED"PROBLEM.Element. element %d, expected %d\n"KWHT,
        result_element, expected_element);
    }
}