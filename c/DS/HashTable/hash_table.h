
#ifndef __HTABLE_H__
#define __HTABLE_H__

/*******************************************************************************
                                * API FOR HASHTABLE
                            
******************************************************************************/

#include <stddef.h>	/* size_t */
#include "dlist.h" /* double linked list */

/*
*Hash function
*Input : data:
 first - data from node of the list, second - compare data
 Return: index of array:.
*/
typedef size_t (* hash_func_t)(void* entry);

/*
* action function
* Input : two void pointers:
* first - value from node of the list, second - param
* Return :"0" - if successful , "1" - if fails.
*/
typedef int (* hash_action_t)(void *value, void* param);


typedef struct htable htable_t;

/*
* Is match function
* Input : two void pointers:
* first - data from node of the list, second - compare data
* Return :"0" - if successful , difference if not equal.
*/
typedef int (*is_match_t)(void* value, void* key);

/*
* Input: number of elements of array, compare function
* Process: Create an HashTable
* Returns: pointer to the HashTable
*/
htable_t *HTableCreate(hash_func_t hash_func, size_t table_size, is_match_t is_match);

/*
* Input: pointer to the HashTable
* Process: free the HashTable and all elements in it.
* Returns: none.
*/
void HTableDestroy(htable_t *htable);

/*
* Input: pointer to the HashTable
* Process: traverse HashTable and calculate number of nodes
* Returns: size of the HashTable
*/
size_t HTableSize(const htable_t *htable);

/*
* Input: pointer to the HashTable
* Process: check if node exist
* Returns: 1 if empty / 0 if not
*/
int HTableIsEmpty(const htable_t *htable);

/*
* Input: Pointer to HashTable and data
* Process: insert the new node
* Returns: 0/1
*/
int HTableInsert(htable_t *htable, void *entry);

/*
* Input: Pointer to HashTable and data
* Process: find node with data and remove the node
* Returns: none
*/
void HTableRemove(htable_t *htable, void *key);

/*
* Input:  Pointer to the HTable, pointer to key.
* Process: Traverse HashTable looking for desired data
* Returns: data if successful / NULL if failed
*/
void *HTableFind(const htable_t *htable, void *to_find);

/*
* Input:  Pointer to the HTable, pointer to parameters, ation func.
* Process: Traverse HashTable and do an action for each node
* Returns: '0' if successful / '1' if failed
*/
int HTableForEach(htable_t *htable, hash_action_t action, void *param);

/*
* Input:  Pointer to the HTable
* Process: calculate the Load Factor of the Hash Table
* Returns: Load Factor
*/
double HTableLoadFactor(const htable_t *htable);

/*
* Input:  Pointer to the HTable
* Process: calculate the Standar Deviation of the Hash Table
* Returns: Standar Deviation
*/
double HTableSD(const htable_t *htable);

#endif /* __HTABLE_H__ */
