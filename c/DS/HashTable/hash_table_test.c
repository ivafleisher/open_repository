#include <stdio.h>/* printf */
#include <stdlib.h>
#include <string.h>/*strlen, stcmp*/

#include "hash_table.h"

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"
/********************************DECLARITION***********************************/
size_t Heshkey              (void *string);
int StrCmp                  (void* value, void* key);
void TestCreate             (void);
void TestInsertRemove       (void);
int PrintHash               (void *value, void* param);
void VerifySize             (size_t result, size_t expected);
void VerifyIsEmpty          (int result, int expected);
void Vocabulary             (void);

/******************************************************************************
                                    * MAIN *
******************************************************************************/                                    
int main()
{
    Vocabulary();
   /*  TestCreate();
    TestInsertRemove(); */

    return 0;
}
/******************************************************************************/
#define SIZE (10) 

void TestCreate(void)
{
    htable_t* hash_table = HTableCreate(Heshkey, SIZE, StrCmp);
    printf(KMAG"TestCreate\n"KWHT);
    if(NULL == hash_table)
    {
        printf(KRED"FAIL.TestCreate\n"KWHT);
    }
    VerifySize(HTableSize(hash_table), 0);
    VerifyIsEmpty(HTableIsEmpty(hash_table),1);

    HTableDestroy(hash_table);
}

void TestInsertRemove(void)
{
    htable_t* hash_table = HTableCreate(Heshkey, SIZE, StrCmp);
    size_t i = 0;
    char *data[] = {"world", "hello", "hi", "yes", "no","monday"};
    size_t size = sizeof(data)/sizeof(data[0]);

    printf(KMAG"TestInsert\n"KWHT);
  
    for (i = 0; i < size; i++)
    {
        HTableInsert(hash_table, &data[i]);
        VerifySize(HTableSize(hash_table), i+1);
    }
    HTableForEach(hash_table, PrintHash, NULL);

    for (i = 0; i <size; i++)
    {
        printf("DATA :%s\n",*(char**)HTableFind(hash_table, &data[i]));
        HTableRemove(hash_table, &data[i]);
        VerifySize(HTableSize(hash_table), size -i-1);
    }

    HTableDestroy(hash_table);  
}
/******************************************************************************/
void VerifySize(size_t result, size_t expected)
{
    if(result != expected)
    {
        printf(KRED"FAIL.Size result:%ld, expected:%ld\n"KWHT,result,expected);
    }
}
void VerifyIsEmpty(int result, int expected)
{
    if(result != expected)
    {
        printf(KRED"FAIL.IsEmpty result:%d, expected:%d\n"KWHT,result,expected);
    }
}
/******************************************************************************/
size_t  Heshkey(void *string)
{
    size_t hash = 5381;
    int c;
    const char * st = *(void **)string;

    while (*st != '\0')
    {
        c = *st;
        hash = ((hash << 5) + hash) + c;
        st++;
    }
    
    return hash;
} 

int StrCmp(void* value, void* key)
{
    return strcmp(*(void**)value, *(void**)key);
}

int PrintHash(void *value, void* param)
{
    printf("%s\n",*(char**)value);
    (void)param;
    return 0;
}
/******************************************************************************/
#define SIZE_W (25)
void Vocabulary()
{
    FILE * fp;
    htable_t *table = NULL;
    char *to_write = NULL;
    char *words = NULL;
    char buffer[SIZE_W] = {0}; 
    size_t counter = 0;

    fp = fopen("/usr/share/dict/words","r");
    if (NULL == fp)
    {
        printf("FAIL OPEN\n");
    }

    while (NULL != fgets(buffer,SIZE_W,fp))
    {
        ++counter;
    }
    
    printf("COUNTER %ld\n", counter);

    fseek(fp, 0, SEEK_SET);
     
    table = HTableCreate(Heshkey,counter,StrCmp);
    words = calloc(sizeof(char) *SIZE_W ,counter);
     
    to_write = words;
     
    for(; fgets(to_write,SIZE_W,fp) != NULL; to_write += SIZE_W)
    {
        HTableInsert(table, &to_write); 
    }

    printf("LOAD FACTOR :%f\n",HTableLoadFactor(table));
      
    printf(KCYN"PRINT WORD:"KWHT);

    while (0 != strcmp(buffer, "exit\n"))
    {
        to_write = fgets(buffer, SIZE_W, stdin);
        if (NULL != HTableFind(table,&to_write))
        {
            printf(KGRN"PRESENT\n"KWHT);
        }
        else 
        {
            printf(KRED"NOT EXIST\n"KWHT);
        }
    }
/* 
    HTableForEach(table, PrintHash,NULL);
 */ 
    free(words);
    words = NULL;

    HTableDestroy(table);
    fclose(fp);
}


