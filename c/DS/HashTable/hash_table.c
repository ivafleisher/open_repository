/*
 * =============================================================================
 *          Filename:  avl.c
 *
 *          Description:   library
 *
 *          Version:  1.0
 *          Created:  03/21/2020
 *
 *         Author:
 * =============================================================================
 */
#include <stdlib.h>
#include "hash_table.h"
#include "dlist.h"

struct htable
{
	hash_func_t hash_func;
    is_match_t is_match;
    size_t table_size;
	dlist_t *table[1];
};
void DestroyTables(htable_t* hash_table, size_t index_to);

htable_t *HTableCreate(hash_func_t hash_func, size_t table_size,
                                            is_match_t is_match)
{
    size_t i = 0;

    htable_t* hash_table = malloc(sizeof(htable_t)+sizeof(dlist_t *) * table_size);
    if(NULL == hash_table)
    {
        return NULL;
    }

    for(i = 0; i < table_size; i++)
    {
        hash_table->table[i] = DListCreate();
        if(NULL == hash_table->table[i])
        {
            DestroyTables(hash_table, i);

            free(hash_table);
            hash_table = NULL;
            return NULL;
        }
    }

    hash_table->hash_func = hash_func;
    hash_table->is_match = is_match;
    hash_table->table_size = table_size;

    return hash_table;
}

void DestroyTables(htable_t* hash_table, size_t index_to)
{
    size_t i = 0;
    for (i = 0; i <index_to; i++)
    {
        DListDestroy(hash_table->table[i]);
    }
}

/*
* Input: pointer to the HashTable
* Process: free the HashTable and all elements in it.
* Returns: none.
*/
void HTableDestroy(htable_t *htable)
{
    size_t i = 0;
    for(i = 0; i < htable->table_size; i++)
    {
        DListDestroy(htable->table[i]);
    }

    free(htable);
    htable = NULL;
}

/*
* Input: pointer to the HashTable
* Process: traverse HashTable and calculate number of nodes
* Returns: size of the HashTable
*/
size_t HTableSize(const htable_t *htable)
{
    size_t i = 0;
    size_t size_table = 0;

    for(i = 0; i < htable->table_size; i++)
    {
        size_table += DListSize(htable->table[i]);
    }

    return size_table;
}

/*
* Input: pointer to the HashTable
* Process: check if node exist
* Returns: 1 if empty / 0 if not
*/
int HTableIsEmpty(const htable_t *htable)
{
    size_t i = 0;
    for (i = 0; i < htable->table_size; i++)
    {
       if (!DListIsEmpty(htable->table[i]))
       {
           return 0;
       }
    }
    return 1;
}

/*
* Input: Pointer to HashTable and data
* Process: insert the new node
* Returns: 0/1
*/
int HTableInsert(htable_t *htable, void *entry)
{
    iter_t to_return = {0};
    int key = htable->hash_func(entry) % htable->table_size;

    to_return = DListPushBack(htable->table[key], entry);
    if (DListIsSame(DListEnd(htable->table[key]), to_return))
    {
        return 1;
    }

    return 0;
}

/*
* Input: Pointer to HashTable and data
* Process: find node with data and remove the node
* Returns: none
*/
void HTableRemove(htable_t *htable, void *key)
{
    int index = htable->hash_func(key) % htable->table_size;
    iter_t begin =  DListBegin(htable->table[index]);
    iter_t end =  DListEnd(htable->table[index]);
    iter_t find = {0};

    if  (DListIsSame(end, find = DListFind(begin, end,
                    (compare_t)(htable->is_match), key)))
    {
        return;
    }

    DListRemove(find);
}

/*
* Input:  Pointer to the HTable, pointer to key.
* Process: Traverse HashTable looking for desired data
* Returns: data if successful / NULL if failed
*/
 void *HTableFind(const htable_t *htable, void *to_find)
{
    int key = htable->hash_func(to_find) % htable->table_size;
    iter_t begin =  DListBegin(htable->table[key]);
    iter_t end =  DListEnd(htable->table[key]);

    return DListGetData(DListFind(begin, end, (compare_t)(htable->is_match) ,to_find));
}

/*
* Input:  Pointer to the HTable, pointer to parameters, ation func.
* Process: Traverse HashTable and do an action for each node
* Returns: '0' if successful / '1' if failed
*/
int HTableForEach(htable_t *htable, hash_action_t action, void *param)
{
    int status = 0;
    size_t i = 0;
    for (i = 0; i < htable->table_size; i++)
    {
        if(0 != (status = DListForEach(DListBegin(htable->table[i]),
                        DListEnd(htable->table[i]),(action_t)action, param)))
        {
            return status;
        }
    }
    return status;
}

/*
* Input:  Pointer to the HTable
* Process: calculate the Load Factor of the Hash Table
* Returns: Load Factor
*/
double HTableLoadFactor(const htable_t *htable)
{
    double occupied =  HTableSize(htable);

    return occupied/htable->table_size;
}

/*
* Input:  Pointer to the HTable
* Process: calculate the Standar Deviation of the Hash Table
* Returns: Standar Deviation
*/
double HTableSD(const htable_t *htable)
{
    return 0;
}
