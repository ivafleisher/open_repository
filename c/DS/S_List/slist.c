#include <stdlib.h> /* malloc, free */
#include<assert.h> /* assert */
#include "slist.h"
/*******************************************************************************
 *                        SINGLE LINKED LIST FUNCTIONS 
 * 
 ******************************************************************************/
#define ASSERT_NOT_NULL(x) (assert(NULL != x));

typedef struct node
{
    void *data;
    struct node* next_node;
}node_t;

struct list 
{
    node_t *head;
    node_t *tail;
};
/******************************SUPPORT FUNC************************************/
static node_t *CreateNode(void* data, node_t* next)
{
    node_t *new_node = malloc(sizeof(node_t));
    
    if (NULL == new_node)
    {
        return NULL;
    }

    new_node->data = data;
    new_node->next_node = next;

    return new_node;
}

static void UpDateTail(iter_t iterator_tail, node_t* new_tail)
{
    ((list_t *)((node_t *)iterator_tail)->data)->tail = new_tail;
}


static iter_t FindEnd(iter_t iterator)
{
    iter_t prev = NULL;

    while (iterator != NULL)
    {
        prev = iterator;
        iterator = SListNext(iterator);
    }

    return prev;
}
/***************************CREATE LIST****************************************/
list_t* SListCreate(void)
{
    list_t* new_list = malloc(sizeof(list_t));
    
    if (NULL == new_list)
    {
        return NULL;
    }

    new_list->head = CreateNode(new_list, NULL);
    
    if (NULL == new_list->head)
    {
        free(new_list);
        new_list = NULL;

        return NULL;
    }

    new_list->tail = new_list->head; 

    return new_list;
}
/******************************************************************************/
void SListDestroy(list_t *list)
{
    iter_t begin = NULL;

    ASSERT_NOT_NULL(list)

    begin = SListBegin(list);

    while (!SListIsEqual(begin, NULL))
    {
        iter_t next = SListNext(begin);
        free(begin);
        begin = next;
    }
    
    free(list);
    list = NULL;
}

/******************************************************************************/
int SListIsEmpty(const list_t *list)
{
    ASSERT_NOT_NULL(list)

    return (list->head == list->tail);
}
/******************************************************************************/
size_t SListCount(const list_t *list)
{
    iter_t begin = NULL;
    iter_t end = NULL;
    size_t counter = 0;

    ASSERT_NOT_NULL(list)

    begin = SListBegin(list);
    end = SListEnd(list);

    while (!SListIsEqual(begin, end))
    {
        begin = SListNext(begin);
        counter++;
    }

    return counter;
}
/******************************************************************************/
void *SListGetData(iter_t iterator)
{
    ASSERT_NOT_NULL(iterator)

    return  ((node_t *)iterator)->data;
}

/******************************************************************************/
void SListSetData(iter_t iterator, void *value)
{
    ASSERT_NOT_NULL(iterator)
    ASSERT_NOT_NULL(((node_t *)iterator)-> next_node)

    ((node_t *)iterator)->data = value;
}
/******************************************************************************/

iter_t SListInsertBefore(list_t *list, iter_t iter, void *data)
{
    node_t* new_node = NULL;

    ASSERT_NOT_NULL(iter)

    new_node = CreateNode(((node_t *)iter)->data, 
                        ((node_t *)iter)->next_node);
    if (NULL == new_node)
    {
        return list->tail;
    }

    if (NULL == SListNext(iter))
    {
        UpDateTail(iter, new_node);
    }

    ((node_t *)iter)->data = data;
    ((node_t *)iter)->next_node = new_node;
    
    return iter;
}
/******************************************************************************/
iter_t SListRemove(iter_t iterator)
{
    void * save_data = NULL;
    node_t * save_next = NULL;
    iter_t next_iter = NULL;

    ASSERT_NOT_NULL(iterator)
    ASSERT_NOT_NULL(((node_t *)iterator)->next_node)

    save_data = ((node_t *)iterator)->next_node->data;
    save_next = ((node_t *)iterator)->next_node->next_node;
    next_iter = SListNext(iterator);
    
    ((node_t *)iterator)->data = save_data;
    ((node_t *)iterator)->next_node = save_next;
    
    if (NULL == SListNext(iterator))
    {
        UpDateTail(iterator, (node_t *)iterator);
    }

    free(next_iter);
    next_iter = NULL;
    
    return iterator;
}
/******************************************************************************/
iter_t SListFind(iter_t from, iter_t to, compare_t compare , void* data)
{
    ASSERT_NOT_NULL(data)
    ASSERT_NOT_NULL(from)
    ASSERT_NOT_NULL(to)

    while(!SListIsEqual(from, to) && 
        (compare(SListGetData(from), data) != 0))
    {
        from = SListNext(from);
    }

    if (SListIsEqual(from, to))
    {
        return FindEnd(from);
    }

    return from;
}
/******************************************************************************/
int SListForEach(iter_t from, iter_t to, action_t action , void *param)
{
    int status = 0;
    
    ASSERT_NOT_NULL(from)
    ASSERT_NOT_NULL(to)
    
    while (!SListIsEqual(from, to) && !status)
    {
        status = action(SListGetData(from), param);
        from = SListNext(from);
    }

    return status;
}
/******************************************************************************/
iter_t SListBegin(const list_t *list)
{
    ASSERT_NOT_NULL(list)

    return list->head;
}
/******************************************************************************/
iter_t SListEnd(const list_t *list)
{
    ASSERT_NOT_NULL(list)

    return list->tail;
}

/******************************************************************************/
iter_t SListNext(iter_t iterator)
{
    ASSERT_NOT_NULL(iterator)

    return ((node_t *)iterator)->next_node;
}
/******************************************************************************/
int SListIsEqual(iter_t iter1, iter_t iter2)
{
    return (iter1 == iter2);
}
/******************************************************************************/
list_t *SListAppend(list_t *dest, list_t *src)
{
    node_t *save_head_src = src->head;

    dest->tail->data = src->head->data;
    dest->tail->next_node = src->head->next_node;

    dest->tail = src->tail;
    dest->tail->data = dest;

    free(save_head_src);
    free(src);

    return dest;
}

