
#include <stdio.h>/*printf*/
#include "slist.h"

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

static void TestFind            ();
static void CreatDestroy        ();
static void TestForEach         ();
static void TestInsertBefore    ();
static void TestGetSet          ();
static void TestAppend          ();

static int AddOnePrint          (void *value, void *param);
static int CompareInt           (void *value, void *value2);
static void VerifyEmpty         (size_t result, size_t expect);
static void VerifyCount         (size_t result_size, size_t expect_size);
static void VerifyElement       (int result_element, int expected_element);
/*******************************************************************************
 *                                  MAIN                                       *
 ******************************************************************************/
int main()
{
    /* 
   */
   /*
   CreatDestroy();
   TestInsertBefore();
   TestFind();
   TestAppend();
    */
   TestForEach();
   TestGetSet();

    return 0;
}
/********************************CREATE DESTROY********************************/
static void CreatDestroy()
{
    list_t * list = SListCreate();
    
    printf(KCYN"CreatDestroy\n"KWHT);

    if (NULL != list)
    {
        printf(KGRN"GOOD.CreatDestroy.\n"KWHT);
    }
    else
    {
        printf(KRED"Problem.CreatDestroy.\n"KWHT);
    }
    
    VerifyEmpty(SListIsEmpty(list), 1);
    VerifyCount(SListCount(list), 0);

    SListDestroy(list);
}
/********************************INSERT REMOVE*********************************/

static void TestInsertBefore()
{
    list_t * list = SListCreate();
    int data[] = {1,2,3,4,5};
    iter_t iterator = NULL;
    size_t size = sizeof(data)/sizeof(data[0]);
    size_t i = 0;
    
    printf(KCYN"TestInsertBefore\n"KWHT);
    printf(KMAG"INSERT\n"KWHT);

    for (i = 0; i < size; i++)
    {
        iter_t iterator_before = SListBegin(list);
        SListInsertBefore(list,iterator_before, &data[i]);
        
        VerifyCount(SListCount(list),i +1);
    }
    
    VerifyEmpty(SListIsEmpty(list), 0);
    
    printf(KMAG"REMOVE\n"KWHT);
    
    for (i = 0; i < size; i++)
    {
        iterator = SListBegin(list);
        printf("DATA %d", *((int *)SListGetData(iterator)));

        SListRemove(iterator);
        VerifyCount(SListCount(list),size - i-1);
    }

    SListDestroy(list);
}
/********************************GetSet****************************************/

static void TestGetSet()
{
    list_t * list = SListCreate();
    iter_t iter_begin = NULL;
    iter_t iter_end = NULL;
    iter_t iterator = NULL;
    int zero = 0;
    
    int data[] = {1,2,3,4,5}; 
    size_t size = sizeof(data)/sizeof(data[0]);
    size_t i = 0;
    
    printf(KCYN"TestGetSet\n"KWHT);
    printf(KMAG"INSERT\n"KWHT);

    for (i = 0; i < size; i++)
    {
        iter_t iterator_before = SListBegin(list);
        SListInsertBefore(list, iterator_before, &data[i]);
        
        VerifyCount(SListCount(list),i +1);
    }
    
    iter_begin = SListBegin(list);
    iter_end = SListEnd(list);

    printf(KMAG"FOR EACH\n"KWHT);

    SListForEach(iter_begin, iter_end, AddOnePrint,&zero);
    printf(KMAG"FOR EACH after Set\n"KWHT);
    iterator = SListBegin(list);
  
    while (!SListIsEqual(iterator, iter_end))
    {
        SListSetData(iterator, &data[0]); 
        iterator = SListNext(iterator);
    }

    iterator = SListBegin(list);
    SListForEach(iter_begin, iter_end, AddOnePrint,&zero); 
    SListDestroy(list);
}

/********************************FIND******************************************/
static int CompareInt(void *value, void *value2)
{
    return *(int *)value - *( int *)value2;
}

static void TestFind()
{
    list_t * list = SListCreate();
    iter_t iter_begin = NULL;
    iter_t iter_end = NULL;
    iter_t find = NULL;
    
    int data[] = {1,2,3,4,5}; 
    int find_arr[] = {2,3, 6};
    size_t size = sizeof(data)/sizeof(data[0]);
    size_t i = 0;
    
    printf(KCYN"TestFind\n"KWHT);
    printf(KMAG"INSERT\n"KWHT);

    for (i = 0; i < size; i++)
    {
        iter_t iterator_before = SListBegin(list);
        SListInsertBefore(list, iterator_before, &data[i]);
        
        VerifyCount(SListCount(list),i +1);
    }
    
    iter_begin = SListBegin(list);
    iter_end = SListEnd(list);

    printf(KMAG"Find exsist element\n"KWHT);

    find = SListFind(iter_begin, iter_end, CompareInt , &(find_arr[0]));

    VerifyElement(*((int *)SListGetData(find)), 2);

    printf(KMAG"Find exsist element\n"KWHT);

    find = SListFind(iter_begin, iter_end, CompareInt , &(find_arr[1]));

    VerifyElement(*((int *)SListGetData(find)), 3);
    printf(KMAG"Find not exsist element\n"KWHT);
    
    iter_begin = SListBegin(list);
    iter_end = SListEnd(list);

    find = SListFind(iter_begin, iter_end, CompareInt , &(find_arr[2]));
    printf("Is Equal :%d\n",SListIsEqual(find, iter_end));

    SListDestroy(list);
}
/********************************FOR EACH**************************************/

static int AddOnePrint(void *value, void *param)
{
    int param_in = *((int *)param);

    printf("Add One to %d  = %d \n",
        *((int *)value), param_in += *((int *)value));
    
    return 0;
}

static void TestForEach()
{
    list_t * list = SListCreate();
    iter_t iter_begin = NULL;
    iter_t iter_end = NULL;
    
    int data[] = {1,2,3,4,5}; 
    int one = 1;
    size_t size = sizeof(data)/sizeof(data[0]);
    size_t i = 0;
    
    printf(KCYN"TestForEach\n"KWHT);
    printf(KMAG"INSERT\n"KWHT);

    for (i = 0; i < size; i++)
    {
        iter_t iterator_before = SListBegin(list);
        SListInsertBefore(list, iterator_before, &data[i]);
        
        VerifyCount(SListCount(list),i +1);
    }
    
    iter_begin = SListBegin(list);
    iter_end = SListEnd(list);

    printf(KMAG"FOR EACH\n"KWHT);

    SListForEach(iter_begin, iter_end, AddOnePrint,&one);

    SListDestroy(list);
}
/********************************APPEND****************************************/

static void TestAppend()
{
    list_t * dest = SListCreate();
    list_t * source = SListCreate();
    list_t* new = NULL;
    iter_t iterator_before = NULL;
    iter_t iter_begin = NULL;
    iter_t iter_end = NULL;
    
    int data1[] = {1,2,3,4,5}; 
    int data2[] = {6,7,8,9,10}; 
    int one = 0;
    size_t size = sizeof(data1)/sizeof(data1[0]);
    size_t i = 0;
    
    printf(KCYN"TestAppend\n"KWHT);
    printf(KMAG"INSERT\n"KWHT);

    for (i = 0; i < size; i++)
    {
        iterator_before = SListBegin(dest);
        SListInsertBefore(dest, iterator_before, &data1[i]);
        
        VerifyCount(SListCount(dest),i +1);
    }
    iter_begin = SListBegin(dest);
    iter_end = SListEnd(dest);
    SListForEach(iter_begin, iter_end, AddOnePrint,&one);
    
    printf(KMAG"INSERT\n"KWHT);
    
    for (i = 0; i < size; i++)
    {
        iterator_before = SListBegin(source);
        SListInsertBefore(source, iterator_before, &data2[i]);
        
        VerifyCount(SListCount(source),i +1);
    }
    
    iter_begin = SListBegin(source);
    iter_end = SListEnd(source);
    SListForEach(iter_begin, iter_end, AddOnePrint,&one);
   
    printf(KMAG"INSERT\n"KWHT);
    new = SListAppend(dest, source);
    iter_begin = SListBegin(new);
    iter_end = SListEnd(new);
    SListForEach(iter_begin, iter_end, AddOnePrint,&one);

    SListDestroy(new);
}

/********************************VERIFY****************************************/
static void VerifyCount(size_t result_size, size_t expect_size)
{
    if(result_size == expect_size)
    {
        printf(KGRN"GOOD.CapacitySize.size %lu\n"KWHT,
        result_size);
    }
    else
    {
        printf(KRED"PROBLEM.CapacitySize.size %lu\n"KWHT,
        result_size);
    }
}

static void VerifyEmpty(size_t result, size_t expect)
{
    if (result == expect)
    {
        printf(KGRN"GOOD.VerifyEmpty\n"KWHT);
    }
    else
    {
        printf(KRED"PROBLEM.VerifyEmpty.size %lu\n"KWHT,
        result);
    }
}

static void VerifyElement(int result_element, int expected_element)
{
    if (result_element == expected_element )
    {
        printf(KGRN"GOOD.Element\n"KWHT);
    }
    else
    {
        printf(KRED"PROBLEM.Element. element %d, expected %d\n"KWHT,
        result_element, expected_element);
    }
}
