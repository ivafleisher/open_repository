
/*******************************************************************************
                            *FUNCTIONS PRIORITY QUEUE
 ******************************************************************************/
#include <assert.h>  /* assert */
#include <stdlib.h> /* malloc, free */
#include "sorted_list.h"
#include "pqueue.h"

#define ASSERT_NOT_NULL(x) (assert(NULL != x));

pqueue_t *PQCreate(pqcompare_t comparator)
{
    return (pqueue_t *)SortedListCreate(comparator);
}
/******************************************************************************/
void PQDestroy(pqueue_t *pqueue)
{
    ASSERT_NOT_NULL(pqueue)

    SortedListDestroy((sorted_list_t *)pqueue);
}
/******************************************************************************/

int PQEnqueue(pqueue_t *pqueue, void* data)
{
    ASSERT_NOT_NULL(pqueue)

    return (SortedListIsSame(SortedListEnd((const sorted_list_t *)pqueue), 
                            SortedListInsert((sorted_list_t *)pqueue, data)));
}
/******************************************************************************/
int PQIsEmpty(const pqueue_t *pqueue)
{
    ASSERT_NOT_NULL(pqueue)

    return SortedListIsEmpty((sorted_list_t *)pqueue);
}
/******************************************************************************/
size_t PQSize(const pqueue_t *pqueue)
{
    ASSERT_NOT_NULL(pqueue)

    return SortedListSize((const sorted_list_t *)pqueue);
}
/******************************************************************************/

void *PQPeek(pqueue_t *pqueue)
{
    ASSERT_NOT_NULL(pqueue)

    return SortedListGetData(SortedListBegin((sorted_list_t *)pqueue));
}
/******************************************************************************/

void PQDequeue(pqueue_t *pqueue)
{
    ASSERT_NOT_NULL(pqueue)

    SortedListPopFront((sorted_list_t *)pqueue);
}
/******************************************************************************/

static int Match ( int(*match)(void* param1,void *param),void *data, void *param)
{
    return !match(data, param);
}

 void *PQErase(pqueue_t *pqueue, ismatch_t ismatch, void *key)
{
    sorted_list_iter_t from = {0};
    sorted_list_iter_t to = {0};
    void * return_data = NULL;
    
    ASSERT_NOT_NULL(pqueue)
    from = SortedListBegin((sorted_list_t *)pqueue);
    to = SortedListEnd((sorted_list_t *)pqueue);

    while (!SortedListIsSame(from, to) 
    && Match(ismatch,SortedListGetData(from), key))
    {
        from = SortedListNext(from);
    }

    if (SortedListIsSame(SortedListEnd((sorted_list_t *)pqueue), from))
    {
        return NULL;
    }

    return_data = SortedListGetData(from);
    SortedListRemove(from);

    return return_data;
}



/******************************************************************************/

void PQClear(pqueue_t *pqueue)
{
    sorted_list_iter_t runner = {0};
    
    ASSERT_NOT_NULL(pqueue)

    runner = SortedListBegin((sorted_list_t *)pqueue);

    while((!SortedListIsSame(SortedListEnd((sorted_list_t *)pqueue), runner)))
    {
        runner = SortedListRemove(runner);
    }
}

