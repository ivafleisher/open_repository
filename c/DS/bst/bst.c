
#include <stdlib.h> /* malloc */
#include "bst.h"

typedef int(*cmp_funct_t)(void* data1, void* data2);
enum child{LEFT,RIGHT, NUM_OF_CHILDREN};

struct node 
{
    void *data;
    bst_node_t *parent;  
    bst_node_t *children[NUM_OF_CHILDREN];  
};

struct btree 
{
    bst_node_t stub;
    cmp_funct_t comparator;
};
/*********************************SUPPORT FUNC**********************************/
bst_node_t *GetRightCh(bst_node_t *node)
{
    return node->children[RIGHT];
}

bst_node_t *GetLeftCh(bst_node_t *node)
{
    return node->children[LEFT];
}
  

bst_node_t *GetParent(bst_node_t *node)
{
    return node->parent;
}

bst_node_t *CreateNode(bst_node_t *parent,int side, void *data)
{
    bst_node_t *node = calloc(sizeof(bst_node_t),1);
    if(NULL == node)
    {
        return NULL;
    }
    node->parent = parent;
    node->data = data;
    parent->children[side] = node;

    return node;
}
int IsLeaf(bst_iter_t iter)
{
    return (NULL== GetLeftCh(iter.bst_node)) && 
            (NULL == GetRightCh(iter.bst_node));
}
int HasTwoChildren(bst_iter_t iter)
{
    return (NULL != GetLeftCh(iter.bst_node)) && 
            (NULL!= GetRightCh(iter.bst_node));
}
int NoChildren(bst_node_t * node)
{
    return (NULL == GetLeftCh(node)) && 
            (NULL== GetRightCh(node));
}

int HasOneChild(bst_iter_t iter)
{
    return !HasTwoChildren(iter) && !IsLeaf(iter);
}

int FindSide(bst_node_t *node)
{
    int side = 0;
    if (GetLeftCh(GetParent(node)) == node)
    {
        side = LEFT;
    }
    else
    {
        side = RIGHT;
    }

    return side;
}

void SwapData(void **data1, void**data2 )
{
    void* temp = *data1;
    *data1 = *data2;
    *data2 = temp;

}
/******************************************************************************/
/*
* Input: pointer to the compare function used for sorting the binary tree
          *Compare Function
          *Input: two pieces of information to be comapared
          *Process: compares the two pieces of data
          *Return: The difference betweeen the data.
          *
* Process: Create a binary tree
* Returns: pointer to the binary tree
*/
bst_t *BSTCreate(int(*cmp_funct)(void* data1, void* data2))
{
    bst_t *btree = malloc(sizeof(bst_t));
    if(NULL == btree)
    {
        return NULL;
    }
    
    btree->stub.children[LEFT] = NULL;
    btree->stub.children[RIGHT] = NULL;
    btree->stub.parent= NULL;
    btree->comparator = cmp_funct;

    return btree;
}

/*
* Input: pointer to the binary tree
* Process: free the binary tree and all elements in it.
* Returns: none.
*/
void BSTDestroy(bst_t *btree)
{
    bst_node_t *node = &(btree->stub);
    bst_node_t *parent = NULL;
    
    while (!BSTIsEmpty(btree))
    {
        if (NULL != GetLeftCh(node))
        {
            node = GetLeftCh(node);
        }
        else if (NULL != GetRightCh(node))
        {
            node = GetRightCh(node);
        }
        else if (NoChildren(node))
        {
            if (GetLeftCh(GetParent(node)) == node)
            {
                node->parent->children[LEFT] = NULL;
            }
            else if (GetRightCh(GetParent(node)) == node)
            {
                node->parent->children[RIGHT] = NULL;
            }

            parent = node->parent;

            free(node);
            node = NULL;
            node = parent;
       }
    }

    free(btree);
    btree = NULL;
}

/*
* Input: pointer to the binary tree
* Process: traverse binary tree and calculate number of nodes
* Returns: size of the tree
*/
size_t BSTSize(const bst_t *btree)
{
    bst_iter_t runner = BSTBegin(btree);
    bst_iter_t end = BSTEnd(btree);
    size_t counter = 0;

    while (!BSTIterIsEqual(runner, end))
    {
        runner = BSTNext(runner);
        counter++;
    }
    
    return counter;
}

/*
* Input: pointer to the binary tree
* Process: check if node exist
* Returns: 1 if empty/ 0 if not
*/
int BSTIsEmpty(const bst_t *btree)
{
    return (btree->stub.children[LEFT] == NULL);
}

/*
* Input: pointer to binary tree, pointer to data
* Process: traverse tree to find position for new node
* based upon the data
* Returns: valid iter if successful / iter to the end  if failed
*/
bst_iter_t BSTInsert(bst_t *btree, void *data)
{
    int side = LEFT;
    bst_node_t *node = &(btree->stub);
    bst_iter_t to_return = {0};

    while(NULL != node->children[side])
    {
        node = node->children[side];
        
        if (0 > btree->comparator(node->data,data))
        {
            side = RIGHT;
        }
        else
        {
           side = LEFT;
        }
    }
    
    to_return.bst_node = CreateNode(node, side, data);

    return to_return;

}



/*
* Input: Iterator (bst_iter_t)
* Process: free data in the given iterator
* Returns: none
*/
void BSTRemove(bst_iter_t iter)
{
    bst_node_t *parent = NULL;
    bst_node_t *child = NULL;
    bst_iter_t prev = {0};
    bst_node_t *node = iter.bst_node;
    int side  = 0;

    side = FindSide(iter.bst_node);
    parent = GetParent(iter.bst_node);
    if(IsLeaf(iter))
    {
       parent->children[side] = NULL;
    }

    if( HasOneChild(iter))
    {
        if (NULL != GetLeftCh(iter.bst_node))
        {
           child = GetLeftCh(iter.bst_node);
        }
        else
        {
            child = GetRightCh(iter.bst_node);
        }

        parent->children[side] = child;
        child->parent = parent;
    }
    if (HasTwoChildren(iter))
    {
        prev = BSTPrev(iter);
        side = FindSide(prev.bst_node);
        
        parent = GetParent(prev.bst_node);

        if (NULL != GetLeftCh(prev.bst_node))
        {
            child = GetLeftCh(prev.bst_node);
            parent->children[side] = child;
            child->parent = parent;
        }
        else if(!HasTwoChildren(prev))
        {
            parent->children[side] = NULL;
        }

        SwapData(&(iter.bst_node->data),&(prev.bst_node->data));

        node = prev.bst_node;
    }

    free(node);
    node = NULL;
}

/*
* Input: pointer to binary tree
* Process: find node with the smallest data
* Returns: iterator to the first element in the binary
* tree
*/
bst_iter_t BSTBegin(const bst_t *btree)
{
    bst_node_t *node = (bst_node_t *)&(btree->stub);
    bst_iter_t to_return = {0};

    while(NULL != node->children[LEFT])
    {
        node = node->children[LEFT];
    }

    to_return.bst_node = node;

    return to_return;
}

/*
* Input: pointer to binary tree
* Process: find node with the biggest data
* Returns: iterator to the last element in the binary
* tree
*/
bst_iter_t BSTEnd(const bst_t *btree)
{
    bst_iter_t to_return = {0};

    to_return.bst_node = (bst_node_t *)&(btree->stub);
    return to_return;
}

/*
* Input:  Iterator (bst_iter_t)
* Process: find node with the closest smallest data to
* given iterator.
* Return: pointer to previous node
*/
bst_iter_t BSTPrev(bst_iter_t iter)
{
    bst_node_t *node = iter.bst_node;
    
    if(NULL != GetLeftCh(node))
    {
        node = GetLeftCh(node);
        while(NULL !=  GetRightCh(node))
        {
            node = GetRightCh(node);
        }
    }
    else 
    {
        while(node == (GetLeftCh(GetParent(node))))
        {
            node = GetParent(node);
        }

        node = GetParent(node);
    }
     iter.bst_node= node;
     return iter;
}

/*
* Input:  Iterator (bst_iter_t)
* Process: find node with the closest biggest data to
* given iterator.
* Return: pointer to next node
*/
bst_iter_t BSTNext(bst_iter_t iter)
{
    bst_node_t *node = iter.bst_node;
    
    if(NULL != GetRightCh(node))
    {
        node = GetRightCh(node);
        while(NULL !=  GetLeftCh(node))
        {
            node = GetLeftCh(node);
        }
    }
    else 
    {
        while(node == (GetRightCh(GetParent(node))))
        {
            node = GetParent(node);
        }

        node = GetParent(node);
    }
   
    iter.bst_node = node;
    return iter;

}

/*
* Input:  Iterator1 (bst_iter_t), iterator2 (bst_iter_t)
* Process: Compare 2 iterators
* Return: 1 if nodes are equal/ 0 if not
*/
int BSTIterIsEqual(bst_iter_t iter1, bst_iter_t iter2)
{
    return(iter1.bst_node == iter2.bst_node);
}

/*
* Input:  iterator (bst_iter_t)
* Process: Obtain data stored in iterator
* Return: pointer to the data
*/
void *BSTGetData(bst_iter_t iter)
{
    return iter.bst_node->data;
}

/*
* Input:  Pointer to the binary tree, pointer to search function that
* compare key with the elements in the binary tree, pointer to key.
        * Search Function
        * Input: two pieces of information to be comapared
        * Process: compares the two pieces of data
        * Return: The difference betweeen the data.
        *
* Process: Traverse tree looking for desired data
* Returns: valid iter if successful / iter to the end  if failed
*/
bst_iter_t BSTFind(bst_t *btree, int(*search)(void* data, void* to_find),
                    void *to_find)
{
    bst_iter_t runner = BSTBegin(btree);
    bst_iter_t end = BSTEnd(btree);

    while (!BSTIterIsEqual(runner, end) && 0 != search(BSTGetData(runner), to_find))
    {
        runner = BSTNext(runner);
    }

    return runner;
}


bst_iter_t BSTGet(bst_t *btree, void *to_find)
{
    int side = LEFT;
    bst_node_t *node = &(btree->stub);
    bst_iter_t to_return = {0};
    node = node->children[side];

    while(NULL != node && 
    0 != btree->comparator(node->data,to_find))
    {
        if (0 > btree->comparator(node->data,to_find))
        {
            side = RIGHT;
        }
        else
        {
           side = LEFT;
        }

        node = node->children[side];
    }
    
    if (NULL == node)
    {
        return BSTEnd(btree);
    }

    to_return.bst_node = node;
    
    return to_return;

}
/*
* Input: Iter from, iter to, pointer to action function that
* performs actions with the elements of binary tree, pointer to parameter
* for function.
        *Action Function
        *Input: the data to be acted upon, the data that is acting
        *Process: Perform the desired action using the provided data
        *Return: 0 if success, all other values indicate failure
        *
* Process: Perform actions with the elements of binary tree in from(included)
* to to(exluded)
* Return: 0 if action was successful, other values from user function if failed
*/
int BSTForEach(bst_iter_t from, bst_iter_t to, int(*action)(void* data, void* param), void* param)
{
    int status  = 0;

    while (!BSTIterIsEqual(from, to) && 
        0 == (status = action(BSTGetData(from), param)))
    {
        from = BSTNext(from);
    }

    return status;
}
