
#include <stdio.h>/*printf*/
#include "bst.h"

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"


static void CreatDestroy        ();
static void TestInsert          ();
static void TestRemove          ();

static void VerifyEmpty         (int result, int expected);
static void VerifySize          (size_t result, size_t expected);
static void VerifyData          (void* result, int expected);
/*******************************************************************************
 *                                  MAIN                                       *
 ******************************************************************************/
int main()
{
   CreatDestroy();
    TestInsert(); 
    TestRemove();
   /*  
    */
    return 0;
}
/********************************CREATE DESTROY********************************/
static int CompareInt(void *value, void *value2)
{
    return (*(int *)value - *( int *)value2);
}


static void CreatDestroy()
{
    bst_t * bst = BSTCreate(CompareInt);
    
    printf(KCYN"CreatDestroy\n"KWHT);

    if (NULL != bst)
    {
        printf(KGRN"GOOD.CreatDestroy.\n"KWHT);
    }
    else
    {
        printf(KRED"Problem.CreatDestroy.\n"KWHT);
    }
    VerifyEmpty(BSTIsEmpty(bst), 1);
    VerifySize(BSTSize(bst), 0);
    BSTDestroy(bst);
}

static void TestInsert()
{
    bst_t * bst = BSTCreate(CompareInt);
    int data[]= {50,39,41,56,53,60};
    size_t size = sizeof(data)/sizeof(data[0]);
    bst_iter_t iter = {0};
    size_t i = 0;
    
    printf(KCYN"TestInsert\n"KWHT);

    VerifyEmpty(BSTIsEmpty(bst), 1);
    VerifySize(BSTSize(bst), 0); 
    
    for (i = 0; i < size; i++)
    {
        printf(KMAG"INSERT\n"KWHT);
        iter = BSTInsert(bst, &data[i]);
        VerifyEmpty(BSTIsEmpty(bst), 0);
        VerifySize(BSTSize(bst), i+1); 
        VerifyData(BSTGetData(iter), data[i]); 
    }

    BSTDestroy(bst);
}
static int AddOnePrint(void *value, void *param)
{
    int param_in = *((int *)param);

    printf("Add One to %d  = %d \n",
        *((int *)value), param_in += *((int *)value));
    
    return 0;
}


static void TestRemove()
{
    bst_t * bst = BSTCreate(CompareInt);
    int data[]= {40, 20, 10, 30, 32, 31, 50};
    size_t size = sizeof(data)/sizeof(data[0]);
    bst_iter_t iter[7];
    bst_iter_t prev = {0};
    bst_iter_t prev_prev = {0};
    size_t i = 0;
    int param= 0;
    
    printf(KCYN"TestInsert\n"KWHT);

    VerifyEmpty(BSTIsEmpty(bst), 1);
    VerifySize(BSTSize(bst), 0); 
    
    for (i = 0; i < size; i++)
    {
        printf(KMAG"INSERT\n"KWHT);
        iter[i]= BSTInsert(bst, &data[i]);
        VerifyEmpty(BSTIsEmpty(bst), 0);
        VerifySize(BSTSize(bst), i+1); 
        VerifyData(BSTGetData(iter[i]), data[i]); 
    }

    for (i = 0; i < size; i++)
    {
        printf(KMAG"FOR EACH\n"KWHT);
        BSTForEach(BSTBegin(bst),BSTEnd(bst),AddOnePrint,&param);
       
        printf(KMAG"REMOVE\n"KWHT);
        printf("DATA %d\n",data[i]);
        iter[i] = BSTGet(bst, &data[i]);
        printf("DATA %d\n",*(int *)BSTGetData(iter[i]));

        BSTRemove(iter[i]);
      
        VerifySize(BSTSize(bst), size-i-1); 
    }
    BSTDestroy(bst);
}


/******************************************************************************/

static void VerifyData(void* result, int expected)
{
    if(*(int *)result != expected)
    {
        printf(KRED"FAIL.VerifyData result: %d,expected:%d\n"KWHT,*(int *)result,expected);
    }
    else
    {
        printf(KGRN"GOOD.VerifyData \n"KWHT);
       
    }
}
static void VerifyEmpty(int result, int expected)
{
    if(result != expected)
    {
        printf(KRED"FAIL.VerifyEmpty result: %d,expected:%d\n"KWHT,result,expected);
    }
    else
    {
        printf(KGRN"GOOD.VerifyEmpty \n"KWHT);
       
    }
}

static void VerifySize(size_t result, size_t expected)
{
    if(result != expected)
    {
        printf(KRED"FAIL.VerifySize result: %ld,expected:%ld\n"KWHT,result,expected);
    }
    else
    {
        printf(KGRN"GOOD.VerifySize \n"KWHT);
       
    }
}