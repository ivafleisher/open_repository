#include <stdio.h>/*printf*/
#include "sorted_list.h"

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"


static void CreatDestroy        ();
static void TestInsertBefore    ();
static void TestMerge           (); 
static void TestPop();

static int AddOnePrint          (void *value, void *param);
static int CompareInt           (void *value, void *value2);
static void VerifyEmpty         (size_t result, size_t expect);
static void VerifyCount         (size_t result_size, size_t expect_size);

/*******************************************************************************
 *                                  MAIN                                       *
 ******************************************************************************/
int main()
{
 
   CreatDestroy();
   TestInsertBefore();
   TestMerge();
   TestPop();

    return 0;
}

/********************************CREATE DESTROY********************************/
static int CompareInt(void *value, void *value2)
{
    return (*(int *)value - *( int *)value2);
}

static void CreatDestroy()
{
    sorted_list_t *list = SortedListCreate(CompareInt);
    
    printf(KCYN"CreatDestroy\n"KWHT);

    if (NULL != list)
    {
        printf(KGRN"GOOD.CreatDestroy.\n"KWHT);
    }
    else
    {
        printf(KRED"Problem.CreatDestroy.\n"KWHT);
    }
    
    VerifyEmpty(SortedListIsEmpty(list), 1);
    VerifyCount(SortedListSize(list), 0);

    SortedListDestroy(list);
}

/********************************INSERT REMOVE*********************************/
static int AddOnePrint(void *value, void *param)
{
    int param_in = *((int *)param);

    printf("Add One to %d  = %d \n",
        *((int *)value), param_in += *((int *)value));
    
    return 0;
}
static void TestInsertBefore()
{
    sorted_list_t * list = SortedListCreate(CompareInt);
    int data[] = {5,2,4,3,1};

    size_t size = sizeof(data)/sizeof(data[0]);
    sorted_list_iter_t iterator = {0};
    size_t i = 0;
    int param = 0;
    
    printf(KCYN"TestInsert\n"KWHT);
    printf(KMAG"INSERT\n"KWHT);

    for (i = 0; i < size; i++)
    {
        SortedListInsert(list, &data[i]);
        
        
        VerifyCount(SortedListSize(list),i +1);
    }
    
    VerifyEmpty(SortedListIsEmpty(list), 0);
    
    printf(KMAG"FOR EACH\n"KWHT);
    SortedListForEach(SortedListBegin(list), SortedListEnd(list),
                     AddOnePrint, &param);
    
    printf(KMAG"REMOVE\n"KWHT);
    
    for (i = 0; i < size; i++)
    {   
        iterator = SortedListBegin(list);
        printf("DATA %d", *((int *)SortedListGetData(iterator)));

        SortedListRemove(iterator);
        VerifyCount(SortedListSize(list),size - i-1);
    } 

    SortedListDestroy(list);
} 
/********************************POP*****************************************/

static void TestPop()
{
    sorted_list_t * list = SortedListCreate(CompareInt);
    sorted_list_t * list2 = SortedListCreate(CompareInt);
   
    int data[] = {5,2,4,3,1};

    size_t size = sizeof(data)/sizeof(data[0]);
    size_t i = 0;
    int param = 0;
    
    printf(KCYN"TestPop\n"KWHT);
    printf(KMAG"INSERT\n"KWHT);

    for (i = 0; i < size; i++)
    {
        SortedListInsert(list, &data[i]);
        SortedListInsert(list2, &data[i]);
        
        VerifyCount(SortedListSize(list),i +1);
        VerifyCount(SortedListSize(list2),i +1);
    }
    
    VerifyEmpty(SortedListIsEmpty(list), 0);
    
    printf(KMAG"FOR EACH\n"KWHT);
    SortedListForEach(SortedListBegin(list), SortedListEnd(list),
                    AddOnePrint, &param);
    
    printf(KMAG"POP\n"KWHT);
    
    for (i = 0; i < size ; i++)
    {   
        printf("DATA %d", *((int *)SortedListGetData(SortedListBegin(list))));

        SortedListPopFront(list);
        VerifyCount(SortedListSize(list),size - i-1);
    }   

    printf(KMAG"POP\n"KWHT);
    for (i = 0; i < size ; i++)
    {   
        printf("DATA %d", *((int *)SortedListGetData(SortedListPrev(SortedListEnd(list2)))));

        SortedListPopBack(list2);
        VerifyCount(SortedListSize(list2),size - i-1);
    } 


    SortedListDestroy(list);
} 
/********************************MERGE*****************************************/

static void TestMerge()
{
    sorted_list_t * dest = SortedListCreate(CompareInt);
    sorted_list_t * source = SortedListCreate(CompareInt);
    sorted_list_t * new = NULL;
    sorted_list_iter_t iter_begin = {0};
    sorted_list_iter_t iter_end = {0};

    /*
    int data1[] = {5,1,4,9,10}; 
    int data2[] = {6,7,8,3,2};  
    
    int data1[] = {5,2,4,3,1};
    int data2[] = {6,7,8,9,10}; 
    
    int data1[] = {1,4,7,8}; 
    int data2[] = {2,3,5,10}; 
    
    int data1[] = {2,5,7}; 
    int data2[] = {1,2,6,8,9}; 
  
 
    int data1[] = {6,7,8,9,10}; 
    int data2[] = {1,2,3,4,5};
  
    */
    int data1[] = {6,7,8,3,2}; 
    int data2[] = {5,1,4,9,10}; 
   
    int one = 0;

    size_t size = sizeof(data1)/sizeof(data1[0]);
    size_t size2 = sizeof(data2)/sizeof(data2[0]);
    size_t i = 0;
    
    printf(KCYN"TestMerge\n"KWHT);
    printf(KMAG"INSERT\n"KWHT);

    for (i = 0; i < size; i++)
    {
        SortedListInsert(dest, &data1[i]);
        
        VerifyCount(SortedListSize(dest),i +1);
    }

    iter_begin = SortedListBegin(dest);
    iter_end = SortedListEnd(dest);
    SortedListForEach(iter_begin, iter_end, AddOnePrint,&one);

        
    printf(KMAG"INSERT\n"KWHT);
    for (i = 0; i < size2; i++)
    {
        SortedListInsert(source, &data2[i]);
        VerifyCount(SortedListSize(source),i +1);
    }
    
    iter_begin = SortedListBegin(source);
    iter_end = SortedListEnd(source);
    SortedListForEach(iter_begin, iter_end, AddOnePrint,&one);
    
    printf(KMAG"MERGE\n"KWHT);
    new = SortedListMerge(dest, source);

    iter_begin = SortedListBegin(new);
    iter_end = SortedListEnd(new);
   
    SortedListForEach(iter_begin, iter_end, AddOnePrint,&one);
    iter_begin = SortedListBegin(new);
    iter_end = SortedListEnd(new);
    
    while (!SortedListIsSame(iter_begin,SortedListPrev( iter_end)))
    {
        if (*((int *)SortedListGetData(iter_begin)) >
            *((int *)SortedListGetData(SortedListNext(iter_begin))))
        {
            printf("not sorted\n");
        }
        iter_begin =SortedListNext(iter_begin);
    }

    SortedListDestroy(new);

} 
/********************************VERIFY****************************************/
static void VerifyCount(size_t result_size, size_t expect_size)
{
    if(result_size == expect_size)
    {
        printf(KGRN"GOOD.CapacitySize.size %lu\n"KWHT,
        result_size);
    }
    else
    {
        printf(KRED"PROBLEM.CapacitySize.size %lu\n"KWHT,
        result_size);
    }
}

static void VerifyEmpty(size_t result, size_t expect)
{
    if (result == expect)
    {
        printf(KGRN"GOOD.VerifyEmpty\n"KWHT);
    }
    else
    {
        printf(KRED"PROBLEM.VerifyEmpty.size %lu\n"KWHT,
        result);
    }
}
