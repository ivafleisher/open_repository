
/*******************************************************************************
                            *SORTED LIST FUNCTIONS*
 ******************************************************************************/
#include<assert.h>  /* assert */
#include <stdlib.h> /* malloc, free */
#include "sorted_list.h"

#define ASSERT_NOT_NULL(x) (assert(NULL != x));

struct sorted_list
{
    dlist_t * dlist;
    sorted_compare_t compare;
};

/**********************************Support Func********************************/
static sorted_list_iter_t Find(sorted_list_iter_t from, sorted_list_iter_t to,
                                    sorted_compare_t compare, void *key)
{
    while (!SortedListIsSame(from, to) 
    && 0 >= compare(SortedListGetData(from), key))
    {
        from = SortedListNext(from);
    }

    return from;
}

/******************************************************************************/
sorted_list_t *  SortedListCreate(sorted_compare_t compare)
{
    sorted_list_t * list = malloc (sizeof(sorted_list_t));
    
    if (NULL == list)
    {
        return NULL;
    }

    list->dlist = DListCreate();

    if(NULL == list->dlist)
    {
        free(list);
        list = NULL;
        
        return NULL;
    }

    list->compare = compare;

    return list;
}

/******************************************************************************/

void SortedListDestroy(sorted_list_t *sortedlist)
{
    ASSERT_NOT_NULL(sortedlist)
    DListDestroy(sortedlist->dlist);

    free(sortedlist);
    sortedlist = NULL;
}
/******************************************************************************/
sorted_list_iter_t SortedListInsert(sorted_list_t *sorted_list, void* data)
{
    sorted_list_iter_t curr = {0};
    
    ASSERT_NOT_NULL(sorted_list)

    curr = SortedListBegin(sorted_list);

    curr = Find(curr, SortedListEnd(sorted_list),
                            sorted_list->compare,data);

    curr.dlist_iter_t = DListInsert(sorted_list->dlist, curr.dlist_iter_t, data);

    return curr;
}
/******************************************************************************/

int SortedListIsEmpty(const sorted_list_t *sortedlist)
{
    ASSERT_NOT_NULL(sortedlist)

    return  DListIsEmpty(sortedlist->dlist);
}
/******************************************************************************/

size_t  SortedListSize(const sorted_list_t *sortedlist)
{
    ASSERT_NOT_NULL(sortedlist)

    return DListSize(sortedlist->dlist);
}
/******************************************************************************/

void *SortedListGetData(sorted_list_iter_t iter)
{
    return DListGetData(iter.dlist_iter_t);
}
/******************************************************************************/

void SortedListPopFront(sorted_list_t *sortedlist)
{
    ASSERT_NOT_NULL(sortedlist)

    DListPopFront(sortedlist->dlist);
}
/******************************************************************************/

void SortedListPopBack(sorted_list_t *sortedlist)
{
    ASSERT_NOT_NULL(sortedlist)

    DListPopBack(sortedlist->dlist);
}
/******************************************************************************/

sorted_list_iter_t SortedListRemove(sorted_list_iter_t iterator)
{
    iterator.dlist_iter_t = DListRemove(iterator.dlist_iter_t);

    return iterator;
}
/******************************************************************************/

sorted_list_iter_t SortedListFind(sorted_list_t *sortedlist, 
                                sorted_list_iter_t from, 
					            sorted_list_iter_t to ,
                                void * key)
{
    ASSERT_NOT_NULL(sortedlist)

    from.dlist_iter_t = DListFind(from.dlist_iter_t, to.dlist_iter_t,
                                            sortedlist->compare , key);
    return from;
}

/******************************************************************************/

int SortedListForEach(sorted_list_iter_t from, sorted_list_iter_t to, 
                                            sorted_action_t action, void *param)
{
    return DListForEach(from.dlist_iter_t, to.dlist_iter_t, action, param);
}
/******************************************************************************/

sorted_list_iter_t SortedListBegin(const sorted_list_t *sortedlist)
{
    sorted_list_iter_t iter = {0}; 

    ASSERT_NOT_NULL(sortedlist)

    iter.dlist_iter_t = DListBegin(sortedlist->dlist);
    
    return iter;
}

/******************************************************************************/
sorted_list_iter_t SortedListEnd(const sorted_list_t *sortedlist)
{
    sorted_list_iter_t iter = {0}; 

    ASSERT_NOT_NULL(sortedlist)

    iter.dlist_iter_t = DListEnd(sortedlist->dlist);

    return iter;
}
/******************************************************************************/

sorted_list_iter_t SortedListNext(sorted_list_iter_t iter)
{
    iter.dlist_iter_t = DListNext(iter.dlist_iter_t);

    return iter;
}
/******************************************************************************/

sorted_list_iter_t SortedListPrev(sorted_list_iter_t iter)
{
    iter.dlist_iter_t = DListPrev(iter.dlist_iter_t);

    return iter;
}
/******************************************************************************/

int SortedListIsSame(sorted_list_iter_t iter1, sorted_list_iter_t iter2)
{
    return DListIsSame(iter1.dlist_iter_t, iter2.dlist_iter_t);
}

/******************************************************************************/
sorted_list_t * SortedListMerge(sorted_list_t *sortedlist1,
                                sorted_list_t *sortedlist2)
{
    sorted_list_iter_t curr_first = {0};
    sorted_list_iter_t where = {0};
    sorted_list_iter_t from = {0};
    sorted_list_iter_t to = {0};

    ASSERT_NOT_NULL(sortedlist1)
    ASSERT_NOT_NULL(sortedlist2)

    curr_first = SortedListBegin(sortedlist1);
    from = SortedListBegin(sortedlist2);

    while (!SortedListIsSame(from, SortedListEnd(sortedlist2)))
    {    
        to = from;

        curr_first = Find(curr_first, SortedListEnd(sortedlist1),
                            sortedlist1->compare, SortedListGetData(from));
        where = SortedListPrev(curr_first);
   
        if (!SortedListIsSame(curr_first, SortedListEnd(sortedlist1)))
        {
            to = Find(to, SortedListEnd(sortedlist2),
                    sortedlist1->compare, SortedListGetData(curr_first));
        }
        else
        {
            to = SortedListEnd(sortedlist2);
        }
       
        DListSplice(from.dlist_iter_t, to.dlist_iter_t, where.dlist_iter_t);
        from = SortedListBegin(sortedlist2);
    }  
    
   SortedListDestroy(sortedlist2);
    
    return sortedlist1;

}
/******************************************************************************/

sorted_list_iter_t SortedListFindIf(sorted_list_iter_t from, sorted_list_iter_t to,
                                            sorted_compare_t compare, void *key)
{
     from.dlist_iter_t = DListFind(from.dlist_iter_t, to.dlist_iter_t,
                                                        compare , key);

    return from;
}






