
#include <stdio.h> /* printf */
#include "queue.h"

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"
static void CreatDestroy        ();
static void TestEnqDeq          ();
static void TestAppend          ();

static void VerifyEmpty         (size_t result, size_t expect);
static void VerifyCount         (size_t result_size, size_t expect_size);
static void VerifyElement       (int result_element, int expected_element);
/*******************************************************************************
 *                                  MAIN                                       *
 ******************************************************************************/
int main()
{
    CreatDestroy();
    TestEnqDeq();
    TestAppend();

    return 0;
}
/********************************CREATE DESTROY********************************/
static void CreatDestroy()
{
    queue_t * queue = QCreate();
    
    printf(KCYN"CreatDestroy\n"KWHT);

    if (NULL != queue)
    {
        printf(KGRN"GOOD.CreatDestroy.\n"KWHT);
    }
    else
    {
        printf(KRED"Problem.CreatDestroy.\n"KWHT);
    }
    
    VerifyEmpty(QIsEmpty(queue), 1);
    VerifyCount(QCount(queue), 0);

    QDestroy(queue);
}
/********************************ENQUEUE DEQUEUE*******************************/

static void TestEnqDeq()
{
    queue_t * queue = QCreate();
    int data[] = {1,2,3,4,5};
    size_t size = sizeof(data)/sizeof(data[0]);
    size_t i = 0;
    
    printf(KCYN"TestEnqDeq\n"KWHT);
    printf(KMAG"ENQUEUE\n"KWHT);

    for (i = 0; i < size; i++)
    {
        QEnqueue(queue, &data[i]);
        
        VerifyCount(QCount(queue),i +1);
    }
    
    VerifyEmpty(QIsEmpty(queue), 0);
    
    printf(KMAG"DEQUEUE\n"KWHT);
    
    for (i = 0; i < size; i++)
    {
        printf("DATA %d", *((int *)QPeek(queue)));

        QDequeue(queue);
        VerifyCount(QCount(queue),size - i-1);
    }

    QDestroy(queue);
}
/********************************APPEND****************************************/

static void TestAppend()
{
    queue_t *dest = QCreate();
    queue_t *source = QCreate();
    queue_t *new = NULL;
    
    int data1[] = {1,2,3,4,5}; 
    int data2[] = {6,7,8,9,10}; 
    size_t size = sizeof(data1)/sizeof(data1[0]);
    size_t i = 0;
      
    printf(KCYN"TestAppend\n"KWHT);
    printf(KMAG"INSERT\n"KWHT);

    for (i = 0; i < size; i++)
    {
        QEnqueue(dest, &data1[i]);
        QEnqueue(source, &data2[i]);
        
        VerifyCount(QCount(dest),i +1);
    }
   
    printf(KMAG"QAppend\n"KWHT);
    
    new = QAppend(dest, source);
    VerifyCount(QCount(dest),10);

    printf(KMAG"DEQUEUE\n"KWHT);
    
    for (i = 0; i < size; i++)
    {
        printf("DATA %d", *((int *)QPeek(new)));

        QDequeue(new);
        VerifyCount(QCount(new),10 - i-1);
    }

    QDestroy(new);
}
/********************************VERIFY****************************************/
static void VerifyCount(size_t result_size, size_t expect_size)
{
    if(result_size == expect_size)
    {
        printf(KGRN"GOOD.CapacitySize.size %lu\n"KWHT,
        result_size);
    }
    else
    {
        printf(KRED"PROBLEM.CapacitySize.size %lu\n"KWHT,
        result_size);
    }
}

static void VerifyEmpty(size_t result, size_t expect)
{
    if (result == expect)
    {
        printf(KGRN"GOOD.VerifyEmpty\n"KWHT);
    }
    else
    {
        printf(KRED"PROBLEM.VerifyEmpty.size %lu\n"KWHT,
        result);
    }
}

static void VerifyElement(int result_element, int expected_element)
{
    if (result_element == expected_element )
    {
        printf(KGRN"GOOD.Element\n"KWHT);
    }
    else
    {
        printf(KRED"PROBLEM.Element. element %d, expected %d\n"KWHT,
        result_element, expected_element);
    }
}
