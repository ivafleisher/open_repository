#include "slist.h"  /* slist */
#include "queue.h"  /* queue */
#include<assert.h> /* assert */

#define ASSERT_NOT_NULL(x) (assert(NULL != x));

struct queue 
{
    void * head;
    void * tail;
};
/******************************************************************************/
queue_t *QCreate(void)
{
    return (queue_t *)SListCreate();
}
/******************************************************************************/
void QDestroy(queue_t *queue)
{
    ASSERT_NOT_NULL(queue)

    SListDestroy((list_t *)queue);
}
/******************************************************************************/
int QIsEmpty(const queue_t *queue)
{
    ASSERT_NOT_NULL(queue)

    return SListIsEmpty((const list_t *)queue);
}
/******************************************************************************/
size_t QCount(const queue_t *queue)
{
    ASSERT_NOT_NULL(queue)

    return SListCount((const list_t *)queue);
}
/******************************************************************************/
int QEnqueue(queue_t *queue, void *data)
{
    iter_t insert_it = NULL;
    iter_t tail = NULL;
    
    ASSERT_NOT_NULL(queue)

    tail = SListEnd((const list_t *)queue);

    insert_it = SListInsertBefore((list_t *)queue, tail, data);
    tail = SListEnd((const list_t *)queue);

    return SListIsEqual(insert_it, tail);
}
/******************************************************************************/
void QDequeue(queue_t *queue)
{
    iter_t begin = NULL;

    ASSERT_NOT_NULL(queue)
    assert(!SListIsEqual(queue->head, queue->tail));

    begin = SListBegin((const list_t *)queue);
    SListRemove(begin);
}
/******************************************************************************/
void *QPeek(queue_t *queue)
{
    ASSERT_NOT_NULL(queue)
    assert(!SListIsEqual(queue->head, queue->tail));

    return SListGetData(queue->head);
}
/******************************************************************************/
queue_t *QAppend(queue_t *dest, queue_t *src)
{
    ASSERT_NOT_NULL(dest)
    ASSERT_NOT_NULL(src)

    return (queue_t *)SListAppend((list_t *)dest, (list_t *)src);
}