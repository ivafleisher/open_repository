#include <stdio.h>/*printf*/
#include "bits_array.h"
#include <string.h>/*strcmp */

void TestIsOn();
void TestToString();
void TestIsOff();
void TestSetOn();
void TestSetOff();
void TestSetBit();
void TestFlipBit();
void TestResetAll();
void TestSetAll();
void TestCountBitsOn();
void TestCountBitsOff();
void TestRotateRigh();
void TestRotateLeft();
void TestCountOnFast();

int main()
{
    TestToString();
    TestIsOn();
    TestIsOff();
    TestSetOn();
    TestSetOff();
    TestSetBit();
    TestFlipBit();
    TestResetAll();
    TestSetAll();
    TestCountBitsOn();
    TestCountBitsOff();
    TestRotateRigh();
    TestRotateLeft();
    TestCountOnFast();

    return 0;
}

void TestCountOnFast()
{
    size_t bits_array []= {8, 65, 0};
    size_t  expected[] = {1, 2, 0};
    int i = 0;
    size_t result = 0;

    const int number_of_tests = sizeof(bits_array) / sizeof(bits_array[0]);
    for (i = 0; i < number_of_tests; i++)
    {
        result = BitsArrayCountOnFast(bits_array[i]);

        if (expected[i] != result)
        {
            printf("You have a problem.TestCountIsOnFast %lu\n",result);
        }
        else
        {
            printf("Good.TestCountIsOnFast\n");
        }
    }
}

void TestRotateLeft()
{
    size_t bits_array[] = {8, 0};
    size_t expected[] = {2, 0};
    size_t num_of_rot = 2;
    int i = 0;
    size_t result = 0;

    const int number_of_tests = sizeof(bits_array) / sizeof(bits_array[0]);

    for (i = 0; i < number_of_tests; i++)
    {
        result = BitsArrayRotateLeft(bits_array[i], num_of_rot);

        if (expected[i] != result)
        {
            printf("You have a problem.TestRotateLeft %lu\n",result);
        }
        else
        {
            printf("Good.TestRotateLeft\n");
        }
    }

    printf("\n");
}
void TestRotateRigh()
{
    size_t bits_array[] = {2, 0, 23};
    size_t expected[] = {8, 0, 92};
    size_t num_of_rot = 2;
    int i = 0;
    size_t result = 0;

    const int number_of_tests = sizeof(bits_array) / sizeof(bits_array[0]);

    for (i = 0; i < number_of_tests; i++)
    {
        result = BitsArrayRotateRight(bits_array[i], num_of_rot);

        if (expected[i] != result)
        {
            printf("You have a problem.TestRotateRigh %lu\n",result);
        }
        else
        {
            printf("Good.TestRotateRigh\n");
        }
    }

    printf("\n");
}


void TestCountBitsOff()
{
    size_t bits_array[] = {1, 0, 358};
    size_t expected[] = {63, 64, 59};
    int i = 0;
    size_t result = 0;

    const int number_of_tests = sizeof(bits_array) / sizeof(bits_array[0]);

    for (i = 0; i < number_of_tests; i++)
    {
        result = BitsArrayCountBitsOff(bits_array[i]);

        if (expected[i] != result)
        {
            printf("You have a problem.TestCountBitsOff %lu\n",result);
        }
        else
        {
            printf("Good.TestCountBitsOff \n");
        }
    }

    printf("\n");
}

void TestCountBitsOn()
{
    size_t bits_array[] = {1, 0, 358};
    size_t expected[] = {1, 0, 5};
    int i = 0;
    size_t result = 0;

    const int number_of_tests = sizeof(bits_array) / sizeof(bits_array[0]);

    for (i = 0; i < number_of_tests; i++)
    {
        result = BitsArrayCountBitsOn(bits_array[i]);

        if (expected[i] != result)
        {
            printf("You have a problem.TestCountBitsOn %lu\n",result);
        }
        else
        {
            printf("Good.TestCountBitsOn \n");
        }
    }

    printf("\n");

}
void TestSetAll()
{
    size_t bits_array[] = {1, 0, 358};
    size_t expected = ~0;
    int i = 0;
    size_t result = 0;

    const int number_of_tests = sizeof(bits_array) / sizeof(bits_array[0]);

    for (i = 0; i < number_of_tests; i++)
    {
        result = BitsArraySetAll(bits_array[i]);

        if (expected != result)
        {
            printf("You have a problem.TestSetAll %d\n", i);
        }
        else
        {
            printf("Good.TestSetAll %d\n", i);
        }
    }

    printf("\n");
}
void TestResetAll()
{
    size_t bits_array[] = {1, 0, 358};
    size_t expected[] = {0, 0, 0};
    int i = 0;
    size_t result = 0;

    const int number_of_tests = sizeof(bits_array) / sizeof(bits_array[0]);

    for (i = 0; i < number_of_tests; i++)
    {
        result = BitsArrayResetAll(bits_array[i]);

        if (expected[i] != result)
        {
            printf("You have a problem.TestResetAll %d\n", i);
        }
        else
        {
            printf("Good.TestResetAll %d\n", i);
        }
    }

    printf("\n");
}
void TestFlipBit()
{
    size_t bits_array[] = {1, 0, 358};
    size_t index = 1;
    size_t expected[] = {3, 2, 356};
    int i = 0;
    size_t result = 0;

    const int number_of_tests = sizeof(bits_array) / sizeof(bits_array[0]);

    for (i = 0; i < number_of_tests; i++)
    {
        result = BitsArrayFlipBit(bits_array[i], index);

        if (expected[i] != result)
        {
            printf("You have a problem.TestFlipBit %d\n", i);
        }
        else
        {
            printf("Good.TestFlipBit %d\n", i);
        }
    }

    printf("\n");
}


void TestSetBit()
{
    size_t bits_array[] = {1 ,0, 358};
    size_t index = 1;
    int value = 1;
    size_t expected[] = {3, 2 ,358};
    int i = 0;
    size_t result = 0;

    const int number_of_tests = sizeof(bits_array) / sizeof(bits_array[0]);

    for (i = 0; i < number_of_tests; i++)
    {
        result = BitsArraySetBit(bits_array[i],index,value);

        if (expected[i] != result)
        {
            printf("You have a problem.TestSetBit %d\n",i);
        }
        else
        {
            printf("Good.TestSetBit %d\n",i);
        }
    }
        printf("\n");
}


void TestSetOff()
{
    size_t bits_array[] = {1, 0, 359};
    size_t index = 0;
    size_t expected[] = {0, 0, 358};
    int i = 0;
    size_t result = 0;

    const int number_of_tests = sizeof(bits_array) / sizeof(bits_array[0]);

    for (i = 0; i < number_of_tests; i++)
    {
        result = BitsArraySetOff(bits_array[i], index);

        if (expected[i] != result)
        {
            printf("You have a problem.TestSetOff %d\n",i);
        }
        else
        {
            printf("Good.TestSetOff %d\n",i);
        }
    }

        printf("\n");
}


void TestIsOn()
{
    size_t bits_array[] = {1, 0, ~((size_t)0)};
    int index = 40;
    int expected[] = {0, 0, 1};
    int i = 0;
    int result = 0;

    const int number_of_tests = sizeof(bits_array) / sizeof(bits_array[0]);

    for (i = 0; i < number_of_tests; i++)
    {
        result = BitsArrayIsOn(bits_array[i],index);

        if (expected[i] != result)
        {
            printf("You have a problem.TestIsOn %d\n",i);
        }
        else
        {
            printf("Good.TestIsOn %d\n",i);
        }
    }
        printf("\n");
}


void TestIsOff()
{
    size_t bits_array[] = {1, 0, 355};
    size_t index = 1;
    size_t expected[] = { 1, 1, 0};
    int i = 0;
    size_t result = 0;

    const int number_of_tests = sizeof(bits_array) / sizeof(bits_array[0]);

    for (i = 0; i < number_of_tests; i++)
    {
        result = BitsArrayIsOff(bits_array[i], index);

        if (expected[i] != result)
        {
            printf("You have a problem.TestIsOff %d\n",i);
        }
        else
        {
            printf("Good.TestIsOff %d\n",i);
        }
    }
        printf("\n");
}


void TestSetOn()
{
    size_t bits_array[] = {1, 0, 355};
    size_t index = 2;
    size_t expected[] = {5, 4 ,359};
    int i = 0;
    size_t result = 0;

    const int number_of_tests = sizeof(bits_array) / sizeof(bits_array[0]);

    for (i = 0; i < number_of_tests; i++)
    {
        result = BitsArraySetOn(bits_array[i],index);

        if (expected[i] != result)
        {
            printf("You have a problem.TestSetOn %d\n",i);
        }
        else
        {
            printf("Good.TestSetOn %d\n",i);
        }
    }
        printf("\n");
}


void TestToString()
{
    size_t bits_array = 1;
    char buffer[100] ="";

    char expected[] ="1000000000000000000000000000000000000000000000000000000000000000";

    BitsArrayToString(bits_array,buffer);
    if (!(strcmp(buffer,expected) == 0))
    {
        printf("You have a problem.ToString\n");
    }
    else
    {
        printf("Good.ToString\n");
    }
    printf("\n");
}
