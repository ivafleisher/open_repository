#ifndef __BITS_ARRAY_H__
#define __BITS_ARRAY_H__
#include <stddef.h>

/* function checks whether specified bit is set to 1 and returns 1 if it is */
int BitsArrayIsOn(size_t bits_array, size_t index);
/* function checks whether specified bit is set to 0 and returns 1 if it is */
int BitsArrayIsOff(size_t bits_array, size_t index);
/* function sets a specified bit to 1 */
size_t BitsArraySetOn(size_t bits_array, size_t index);
/* function sets a specified bit to 0 */
size_t BitsArraySetOff(size_t bits_array, size_t index);
/* function sets a specified bit to specified value */
size_t BitsArraySetBit(size_t bits_array, size_t index, int bit_value);
/* function changes to opposite value the specified  bit */
size_t BitsArrayFlipBit(size_t bits_array, size_t index);
/* function set off all bits */
size_t BitsArrayResetAll(size_t bits_array);
/* function set on all bits */
size_t BitsArraySetAll(size_t bits_array);
/* function returns the number of bits set to 1*/
size_t BitsArrayCountBitsOn(size_t bits_array);
/* function returns the number of bits set to 0*/
size_t BitsArrayCountBitsOff(size_t bits_array);
size_t BitsArrayCountOnFast(size_t bits_array);
/* function rotates array to the rigth num_of_rot times */
size_t BitsArrayRotateLeft(size_t bits_array, size_t num_of_rot);
/* function returns a string representation of array, from left to right */
size_t BitsArrayRotateRight(size_t bits_array, size_t num_of_rot);

/*buffer must be big enough for the arra and null terminator
(word_size + 1 of length)*/
/* function returns a string representation of array, from left to right */
char *BitsArrayToString(size_t bits_array, char buffer[]);


#endif
