#include <stdio.h>/*printf*/
#include <assert.h>/*assert*/

#define BIT_ARRAY_SIZE (sizeof(size_t) * 8)
#define ONE (size_t)(1)
#define ASSERT_IN_RANGE assert(index < BIT_ARRAY_SIZE)



int BitsArrayIsOn(size_t bits_array, size_t index)
{
    ASSERT_IN_RANGE;
    return (0 !=(bits_array &(ONE << index)));
}

int BitsArrayIsOff(size_t bits_array, size_t index)
{
    ASSERT_IN_RANGE;
    return !BitsArrayIsOn(bits_array, index);
}

size_t BitsArraySetOn(size_t bits_array, size_t index)
{
    ASSERT_IN_RANGE;
    return(bits_array | (ONE << index));
}

size_t BitsArraySetOff(size_t bits_array, size_t index)
{
    ASSERT_IN_RANGE;
    return(bits_array &(~(ONE << index)));
}

size_t BitsArraySetBit(size_t bits_array, size_t index, int bit_value)
{
    ASSERT_IN_RANGE;
    bits_array &= ~(ONE << index);
    bits_array |= (bit_value & ONE) << index;

    return bits_array;
}


size_t BitsArrayFlipBit(size_t bits_array, size_t index)
{
    ASSERT_IN_RANGE;
    return(bits_array ^ (ONE << index));
}

size_t BitsArrayResetAll(size_t bits_array)
{
  return bits_array = (size_t)0;
}

size_t BitsArraySetAll(size_t bits_array)
{
    return bits_array = ~((size_t)0);
}

int BitsArrayCountBitsOn(size_t bits_array)
{
    size_t i = 0;
    size_t counter = 0;

    for (i = 0; i < BIT_ARRAY_SIZE; i++)
    {
        if ((bits_array >> i) & 1)
        {
            counter++;
        }
    }

    return counter;
}

int BitsArrayCountBitsOff(size_t bits_array)
{
    return ( BIT_ARRAY_SIZE - BitsArrayCountBitsOn(bits_array));
}

size_t BitsArrayRotateLeft(size_t bits_array, size_t num_of_rot)
{
    return (bits_array >> num_of_rot) | (bits_array << (BIT_ARRAY_SIZE - num_of_rot));
}

size_t BitsArrayRotateRight(size_t bits_array, size_t num_of_rot)
{
    return (bits_array << num_of_rot) | (bits_array >> (BIT_ARRAY_SIZE - num_of_rot));
}

/*buffer must be big enough for the arra and null terminator
(word_size + 1 of length)*/

char *BitsArrayToString(size_t bits_array, char buffer[])
{
    char *pointer = buffer;
    size_t i = 0;

    for (i = 0;i < BIT_ARRAY_SIZE; i++)
    {
        *pointer =(bits_array & ONE) + '0';

        bits_array >>= ONE;
        pointer++;
    }

    *pointer = '\0';

    return buffer;
}


#define BYTE 8
int BitsArrayCountOnFast(size_t bits_array)
{
    int counter_bit = 0;
    size_t i = 0;
    size_t bits = 0;
    size_t mask = 0xFF;

    static const unsigned int lut_set[] =
    {0,	1,	1,	2,	1,	2,	2,	3,	1,	2,
     2,	3,	2,	3,	3,	4,	1,	2,	2,	3,
     2,	3,	3,	4,	2,	3,	3,	4,	3,	4,
     4,	5,	1,	2,	2,	3,	2,	3,	3,	4,
     2,	3,	3,	4,	3,	4,	4,	5,	2,	3,
     3,	4,	3,	4,	4,	5,	3,	4,	4,	5,
     4,	5,	5,	6,	1,	2,	2,	3,	2,	3,
     3,	4,	2,	3,	3,	4,	3,	4,	4,	5,
     2,	3,	3,	4,	3,	4,	4,	5,	3,	4,
     4,	5,	4,	5,	5,	6,	2,	3,	3,	4,
     3,	4,	4,	5,	3,	4,	4,	5,	4,	5,
     5,	6,	3,	4,	4,	5,	4,	5,	5,	6,
     4,	5,	5,	6,	5,	6,	6,	7,	1,	2,
     2,	3,	2,	3,	3,	4,	2,	3,	3,	4,
     3,	4,	4,	5,	2,	3,	3,	4,	3,	4,
     4,	5,	3,	4,	4,	5,	4,	5,	5,	6,
     2,	3,	3,	4,	3,	4,	4,	5,	3,	4,
     4,	5,	4,	5,	5,	6,	3,	4,	4,	5,
     4,	5,	5,	6,	4,	5,	5,	6,	5,	6,
     6,	7,	2,	3,	3,	4,	3,	4,	4,	5,
     3,	4,	4,	5,	4,	5,	5,	6,	3,	4,
     4,	5,	4,	5,	5,	6,	4,	5,	5,	6,
     5,	6,	6,	7,	3,	4,	4,	5,	4,	5,
     5,	6,	4,	5,	5,	6,	5,	6,	6,	7,
     4,	5,	5,	6,	5,	6,	6,	7,	5,	6,
     6,	7,	6,	7,	7, 8};

     for (i =0; i < BIT_ARRAY_SIZE/BYTE; i++)
     {
         bits = (mask & bits_array);
         bits_array >>= BYTE;
         counter_bit += lut_set[bits];
     }
     return counter_bit;
}
