#ifndef __C_BUFFER_H__
#define __C_BUFFER_H__

#include <sys/types.h> /* size_t and ssize_t */

/* circular buffer is a FIFO data structure*/

typedef struct c_buffer buffer_t;

/* creates a buffer with specified capacity (in bytes) and returns a pointer to it */
buffer_t *CBufferCreate(size_t capacity);

/* destroys the buffer and frees all the allocated memory */
void CBufferDestroy(buffer_t *buffer);

/* On success, the number of bytes read is returned (can be less than count).
On error, -1 is returned */
ssize_t CBufferRead(buffer_t *buffer, void *dest, size_t count);

/* On success, the number of bytes written is returned (can be less than count).
On error, -1 is returned */
ssize_t CBufferWrite(buffer_t *buffer, const void *src, size_t count);

/* returns 1 if the buffer is empty, otherwise 0 */
int CBufferIsEmpty(const buffer_t *buffer);

/* returns amount of free space (in bytes) in the buffer */
size_t CBufferFreeSpace(const buffer_t *buffer);

/* returns capacity of the VLAD buffer (in bytes) */
size_t CBufferCapacity(const buffer_t *buffer);


#endif /* __C_BUFFER_H__ */
