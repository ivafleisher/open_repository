#include <assert.h>/*assert*/
#include <stdlib.h>/*malloc*/
#include <string.h>
#include <sys/types.h>/*ssize_t*/
#include <errno.h>/*errno*/
#include "c_buffer.h"

#define ASSERT_BUFFER_NOT_NULL assert(NULL != buffer);

struct c_buffer
{
    char *to_write;
    char *to_read;
    size_t capacity;
    char bytes[1];
};

#define MIN2(a,b) ( ((a) > (b)) ? (b) : (a) )
/* creates a buffer with specified capacity (in bytes) and returns a pointer to it */
buffer_t *CBufferCreate(size_t capacity)
{
    buffer_t *new_buff = malloc(sizeof(buffer_t) + sizeof(char) * capacity);

    assert(0 < capacity);

    new_buff->to_write = new_buff->bytes;
    new_buff->to_read = new_buff->bytes;

    new_buff->capacity = capacity;

    return new_buff;
}

/* destroys the buffer and frees all the allocated memory */
void CBufferDestroy(buffer_t *buffer)
{
    ASSERT_BUFFER_NOT_NULL
    free(buffer);
    buffer = NULL;
}

/* On success, the number of bytes read is returned (can be less than count).
On error, -1 is returned */
ssize_t CBufferRead(buffer_t *buffer, void *dest, size_t count)
{
    size_t bytes_to_read = 0;
    size_t bytes_before_end =0;

    size_t counter = 0;
    size_t size = 0;
    errno = 0;

    if (CBufferFreeSpace(buffer) == buffer->capacity)
    {
        return 0;
    }

    size = buffer->capacity - CBufferFreeSpace(buffer);
    bytes_to_read = MIN2(count,size);
    bytes_before_end = (buffer->bytes + buffer->capacity + 1) - buffer->to_read;

    if (bytes_to_read > bytes_before_end)
    {
        memcpy( dest ,buffer->to_read , bytes_before_end);
        bytes_to_read -= bytes_before_end;
        buffer->to_read = buffer->bytes;

        counter += bytes_before_end;
        dest = (char *)dest + bytes_before_end;
    }

    memcpy(dest, buffer->to_read, bytes_to_read);
    buffer->to_read += bytes_to_read;
    counter += bytes_to_read;

    if (counter != count)
    {
        errno = ENODATA;
    }

    return counter;
}


/* On success, the number of bytes written is returned (can be less than count).
On error, -1 is returned */
ssize_t CBufferWrite(buffer_t *buffer, const void *src, size_t count)
{
    size_t bytes_to_write = 0;
    size_t counter = 0;
    size_t bytes_befor_end = 0;

    char *end = buffer->bytes + buffer->capacity + 1;
    bytes_befor_end = end - buffer->to_write;

    errno = 0;

    if (CBufferFreeSpace(buffer) == 0)
    {
        return 0;
    }

    bytes_to_write = MIN2(count,CBufferFreeSpace(buffer));

    if (bytes_befor_end <  bytes_to_write)
    {
        memcpy( buffer->to_write, src, bytes_befor_end);
        bytes_to_write -= bytes_befor_end;
        counter += bytes_befor_end;

        buffer->to_write = buffer->bytes;
        src = (char *)src + bytes_befor_end;
    }

    memcpy(buffer->to_write, src,bytes_to_write);
    buffer->to_write += bytes_to_write;
    counter += bytes_to_write;


    if (counter != count)
    {
        errno = ENOBUFS;
    }

    return counter;
}

/* returns 1 if the buffer is empty, otherwise 0 */
int CBufferIsEmpty(const buffer_t *buffer)
{
    ASSERT_BUFFER_NOT_NULL
    if (CBufferFreeSpace(buffer) == buffer->capacity)
    {
        return 1;
    }

    return 0;
}

/* returns amount of free space (in bytes) in the buffer */
size_t CBufferFreeSpace(const buffer_t *buffer)
{
    long diff = 0;

    ASSERT_BUFFER_NOT_NULL

    diff = buffer->to_write - buffer->to_read;

    if (diff < 0)
    {
        return -diff -1;
    }

    return buffer->capacity - diff;
}

/* returns capacity of the VLAD buffer (in bytes) */
size_t CBufferCapacity(const buffer_t *buffer)
{
    ASSERT_BUFFER_NOT_NULL

    return buffer->capacity;
}
