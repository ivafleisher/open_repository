#include <stdio.h>/*printf*/
#include <sys/types.h>/*ssize_t*/
#include <errno.h>/*errno*/
#include "c_buffer.h"

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"


void TestBufferCreateDestroy();
void TestBuffer();
void TestWriteRead();
void VerifyErrno(int exp_errno_name);
void VerifyWrite(ssize_t result, ssize_t expected);
void VerifyWrite(ssize_t result, ssize_t expected);
void VerifyFreeSpace(const buffer_t *buff, size_t expected);
void VerifyIsEmpty(const buffer_t *buff, int  expected);
void VerifyCapacity(const buffer_t *buff, size_t  expected);

int main()
{
    TestBufferCreateDestroy();
    TestBuffer();
    TestWriteRead();
    return 0;
}


void TestWriteRead()
{
	buffer_t *my_buff = CBufferCreate(5);
	const char *src = "ABCDEFQWERTYPOIU12345";

	char dest[100] = "";

	printf("TestWriteRead\n");

	VerifyIsEmpty(my_buff, 1);
	VerifyFreeSpace(my_buff, 5);

    CBufferWrite(my_buff, src, 3);
	VerifyIsEmpty(my_buff, 0);
    VerifyFreeSpace(my_buff, 2);
	VerifyErrno(0);

    CBufferRead(my_buff, dest, 2);
    VerifyIsEmpty(my_buff, 0);
    VerifyFreeSpace(my_buff, 4);
	VerifyErrno(0);

    CBufferRead(my_buff, dest, 2);
    VerifyIsEmpty(my_buff, 1);
    VerifyFreeSpace(my_buff, 5);
    VerifyErrno(ENODATA);

	CBufferDestroy(my_buff);
	printf("\n");
}

void VerifyErrno(int exp_errno_name)
{
	if (errno == exp_errno_name)
	{
		printf(KGRN"Errno SUCCESS\n"KNRM);
	}
	else
	{
		printf(KRED"Errno FAIL\n"KNRM);
	}
}
void TestBuffer()
{
    buffer_t *buff = CBufferCreate(5);
    VerifyIsEmpty(buff, 1);
    VerifyCapacity(buff, 5);
    VerifyFreeSpace(buff, 5);

    CBufferDestroy(buff);
}


void TestBufferCreateDestroy()
{
    buffer_t *buff = CBufferCreate(5);

    if (buff != NULL)
    {
        printf(KGRN"Success.TestBufferCreateDestroy\n"KWHT);
    }
    else
    {
        printf(KRED"Problem.TestBufferCreateDestroy \n"KWHT);
    }

    CBufferDestroy(buff);
}


void VerifyWrite(ssize_t result, ssize_t expected)
{
	if (result == expected)
	{
		printf(KGRN"Write SUCCESS\n"KNRM);
	}
	else
	{
		printf(KRED"Write FAIL\n"KNRM);
	}
}


void VerifyFreeSpace(const buffer_t *buff, size_t expected)
{
    if (CBufferFreeSpace(buff) != expected)
    {
        printf(KRED"Problem.VerifyFreeSpace %lu, expected %lu\n"KWHT,
                CBufferFreeSpace(buff), expected);
    }
    else
    {
        printf(KGRN"GOOD.VerifyFreeSpace\n"KWHT);
    }
}

void VerifyIsEmpty(const buffer_t *buff, int  expected)
{
    if ( CBufferIsEmpty(buff) != expected)
    {
        printf(KRED"Problem.VerifyIsEmpty %d, expected %d  \n"KWHT,
                CBufferIsEmpty(buff), expected);
    }
    else
    {
        printf(KGRN"GOOD.VerifyIsEmpty\n"KWHT);
    }
}


void VerifyCapacity(const buffer_t *buff, size_t  expected)
{
    if ( CBufferCapacity(buff) != expected)
    {
        printf(KRED"Problem.VerifyCapacity %lu, expected %lu \n"KWHT,
                CBufferCapacity(buff), expected);
    }
    else
    {
        printf(KGRN"GOOD.VerifyCapacity\n"KWHT);
    }
}
