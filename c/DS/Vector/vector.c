#include <stdlib.h> /* malloc , free */
#include<assert.h> /* assert */
#include "vector.h"
/******************************************************************************
 *                          VECTOR FUNCTIONS
*******************************************************************************/

#define ASSERT_NOT_NULL(x) (assert(NULL != x));

struct vector
{
    size_t capacity;
    size_t size;
    void **head;
};

/* 
 * The function creates the vector_t vector with a given capacity 
 *
 * Return value:
 * pointer to the vector_t structure or NULL in case of creation failed
 * 
 * */
vector_t * VectorCreate(size_t capacity)
{
    vector_t *new_vector = malloc(sizeof(vector_t));
    
    if (NULL == new_vector)
    {
        return NULL;
    }
    
    new_vector->head = malloc(sizeof(void*) * capacity);
    
    if (NULL == new_vector->head)
    {
        return NULL;
    }
    
    new_vector->capacity = capacity;
    new_vector->size = 0;

    return new_vector;
}

/*
 * The function deletes vector_t vector and frees memory
 *
 * */
void VectorDestroy(vector_t *vector)
{
    ASSERT_NOT_NULL(vector)
   
    free (vector->head);
    vector->head = NULL;

    free(vector);
    vector = NULL;
}


/*
 * The function pop back one element in the given vector_t vector
 * 
 * Return value:
 * 0 - SUCCESS
 * 1 - FAIL
 * 
 * */
void VectorPopBack(vector_t *vector)
{
    ASSERT_NOT_NULL(vector);
    assert(vector->size > 0);
    
    vector->size--;
}


/*
 * The function push back a void pointer element into the vector_t vector 
 * return 1 when fail to allocate memory and 1 when succeed
 * 
 * */
int VectorPushBack(vector_t *vector, void *element)
{
    ASSERT_NOT_NULL(vector)

    if (vector->size == vector->capacity)
    {
        size_t  new_capacity = vector->capacity * 2 + 1;

        if (VectorReserve(vector, new_capacity))
        {
            return 1;
        }

        vector->capacity = new_capacity;
    }
    
    vector->head[vector->size]= element;
    vector->size++;

    return 0;
}


/*
 * The function returns pointer that stores the element in the position pos 
 * in the vector_t vector. If pos is greater than the vector capacity, the 
 * behavior is undefined.
 * 
 * Note: NULL can be stored as value as well
 *
 * */
void *VectorGetElement(vector_t *vector, size_t pos)
{
    ASSERT_NOT_NULL(vector)
    assert(vector->capacity >= pos);

    return vector->head[pos];
}


/*
 * The function accepts pointer to vector_t vector and returns the number
 * of elements in this structure
 * 
 * */
size_t VectorSize(const vector_t *vector)
{
    ASSERT_NOT_NULL(vector)

    return vector->size;
}


/*
 * The function accepts pointer to vector_t vector structure and checks if it 
 * empty
 *
 * Return value:
 * 1 - the Vector is empty
 * 0 - the Vector is non-empty
 * 
 * */
int VectorIsEmpty(const vector_t *vector)
{
    ASSERT_NOT_NULL(vector)

    return (vector->size == 0);
}


/*
 * The function returns the capacity value of the given vector_t vector
 *
 * */
size_t VectorCapacity(const vector_t *vector)
{
    ASSERT_NOT_NULL(vector)

    return vector->capacity;
}


/*
 * The function allocate new_capacity memory space in the vector_t vector on. 
 * If the new_capacity is bigger than the current vector capacity, the behavior
 * is undefined.
 * Return 1 when fail to allocate memory and 1 when succeed.
 * 
 * */
int VectorReserve(vector_t *vector, size_t new_capacity)
{
    void *tmp = NULL;

    ASSERT_NOT_NULL(vector)

    tmp = realloc(vector->head, new_capacity * sizeof(void*)); 
    
    if (NULL == tmp)
    {
        return 1;
    }

    vector->head = tmp;
    vector->capacity = new_capacity;

    return 0;
}

/*
 * The function reduce memory space in the vector_t vector until it fit the
 * current size
 *
 * Return value:
 * 0 - success
 * 1 - unsuccessfull
 * */
int VectorShrinkToFit(vector_t *vector)
{
    ASSERT_NOT_NULL(vector)

    return VectorReserve(vector, VectorSize(vector));

}

/*
 * The function set a new value in the position pos of the vector_t vector.
 * If pos is greater than the vector capacity, the behavior is undefined.
 *
 * Note: NULL can be stored as value as well
 *
 * */

void VectorSetElement(vector_t *vector, size_t pos, void *value)
{
    ASSERT_NOT_NULL(vector)
    assert(pos < vector->capacity);

    vector->head[pos] = value;
}