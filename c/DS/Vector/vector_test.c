#include <stdio.h>/*printf*/
#include "vector.h"

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

static void CreatDestroy        ();
static void TestPopPush         ();
static void TestResize          ();
static void VerifySize          (size_t result_size, size_t expect_size);
static void VerifyElement       (int result_element, int expected_element);
static void VerifyEmpty         (size_t result, size_t expect);

int main()
{
    /* 
    */
    CreatDestroy();
    TestResize();
    TestPopPush(); 

    return 0;
}
/********************************CREATE DESTROY********************************/
static void CreatDestroy()
{
    vector_t * p_vector = VectorCreate(5);
    
    printf(KCYN"CreatDestroy\n"KWHT);

    if (NULL != p_vector)
    {
        printf(KGRN"GOOD.CreatDestroy.\n"KWHT);
    }
    else
    {
        printf(KRED"Problem.CreatDestroy.\n"KWHT);
    }
    
    VerifyEmpty(VectorIsEmpty(p_vector), 1);

    VectorDestroy(p_vector);
}
/********************************POP PUSH**************************************/

static void TestPopPush()
{
    vector_t *new_vector = VectorCreate(5);
    int data[] = {1,2,3,4,5};
    size_t size_data = sizeof (data)/ sizeof (data[0]);
    size_t i = 0;

    printf(KCYN"TestPopPush\n"KWHT);
    
    VerifyEmpty(VectorIsEmpty(new_vector), 1);
   
    for ( i = 0; i < size_data - 1; i++)
    {
        VectorPushBack(new_vector, (void *)&data[i]);
    }
    
    VerifySize( VectorSize(new_vector),  size_data - 1);

    VectorPopBack(new_vector);
    VectorPopBack(new_vector);
    
    VerifySize( VectorSize(new_vector),  size_data - 3);

    VectorPushBack(new_vector, (void *)&data[2]);
    VectorPushBack(new_vector, (void *)&data[3]);
    
    VerifySize( VectorSize(new_vector),  size_data - 1);
    
    for (i = 0; i < size_data -1; i++)
    {
        VerifyElement(*((int *)VectorGetElement(new_vector,i)),  i + 1);
    } 

    VectorPushBack(new_vector, (void *)&data[4]);
    VectorPushBack(new_vector, (void *)&data[4]);
    
    VerifyElement(*((int *)VectorGetElement(new_vector, 5)),  5);

    VerifySize( VectorSize(new_vector),  size_data + 1);
    VerifySize( VectorCapacity(new_vector),  11);

    VerifyEmpty(VectorIsEmpty(new_vector), 0);

    VectorDestroy(new_vector);
}
/********************************RESIZE****************************************/

static void TestResize()
{
    vector_t *new_vector = VectorCreate(5);
    int data[] = {1,2,3,4,5};
    size_t size_data = sizeof (data)/ sizeof (data[0]);
    size_t i = 0;

    printf(KCYN"TestResize\n"KWHT);
    
    for ( i = 0; i < size_data - 1; i++)
    {
        VectorPushBack(new_vector, (void *)&data[i]);
    }

    VerifySize( VectorSize(new_vector),  size_data - 1);
    
    VectorReserve(new_vector,8);
    VerifySize( VectorCapacity(new_vector),  8);

    VectorShrinkToFit(new_vector);
    VerifySize( VectorCapacity(new_vector),  size_data - 1);
    VerifyElement(*((int *)VectorGetElement(new_vector, size_data - 2)),  4);


    VectorDestroy(new_vector);
}

static void VerifySize( size_t result_size, size_t expect_size)
{
    if(result_size == expect_size)
    {
        printf(KGRN"GOOD.CapacitySize\n"KWHT);
    }
    else
    {
        printf(KRED"PROBLEM.CapacitySize.size %lu\n"KWHT,
        result_size);
    }
}

static void VerifyEmpty( size_t result, size_t expect)
{
    if (result == expect)
    {
        printf(KGRN"GOOD.VerifyEmpty\n"KWHT);
    }
    else
    {
        printf(KRED"PROBLEM.VerifyEmpty.size %lu\n"KWHT,
        result);
    }
}

static void VerifyElement(int result_element, int expected_element)
{
    if (result_element == expected_element )
    {
        printf(KGRN"GOOD.Element\n"KWHT);
    }
    else
    {
        printf(KRED"PROBLEM.Element. element %d, expected %d\n"KWHT,
        result_element, expected_element);
    }
}