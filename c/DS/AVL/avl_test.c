#include <stdio.h>  /* printf */
#include <stdlib.h> /* malloc , abs */
#include <time.h>   /*time */
#include <assert.h> /*assert*/

#include "avl.h"

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"
/******************************************************************************/
void TestCreate(void);
void TestInsert(void);
void TestForEach(void);
void TestFind(void);
void TestRotateLeft(void);
void TestRotateRight(void);
void TestRotateRightLeft(void);
void TestRemoveRotateLeft(void);
void TestBig();
void FailTest(void);

int Action(void * data, void *param);
void VerifySize(size_t result, size_t expected);
void VerifyIsEmpty(int result, int expected);
void VerifyHight(int result, int expected);
/******************************************************************************/
struct avl 
{
	is_before isbefore;			 
	avl_node_t *root;
};


struct node
{
	void *data;				
	struct node *children[2];
	size_t node_height;
};

int IsBalncedF(avl_node_t *node)
{
    return !(abs(DifferenceHeightF(node)) <= 1);
}

int GetHeightF(avl_node_t *node)
{
    int height = 0;
    if (NULL != node)
    {
        height = node->node_height;
    }
    return height;
}

int DifferenceHeightF(avl_node_t *node)
{
    return (GetHeightF(node->children[0]) - GetHeightF(node->children[1]));
}
int InOrderF(avl_node_t *node, int (*action)(avl_node_t*node))
{
    int status = 0;
    
    if(NULL == node)
    {
        return 0;
    }

    status = InOrderF(node->children[0],action);
    if(0 != status)
    {
        return status;
    }
    
    status = action(node);
    if(0 != status)
    {
        return status;
    }
    
    return InOrderF(node->children[1],action);
}


void IsBalsncedTree(avl_t *tree)
{
    if (!InOrderF(tree->root, IsBalncedF))
    {
        printf("GOOD .IsBALANCED\n");
    }
    else
    {
        printf("FAIL .IsBALANCED\n");
    
    }
    
}
#define COUNT 10  
void print2DUtil(avl_node_t *root, int space)  
{  
    int i = 0;
     
    if (root == NULL)  
        return;  
  
    /* Increase distance between levels */  
    space += COUNT;  
  
    /* Process right child first */  
    print2DUtil(root->children[1], space);  
  
    /* Print current node after space */  
    /* count */ 
    printf("\n"); 
    /* cout<<endl;   */
    for (i = COUNT; i < space; i++)  
    printf(" ");
    printf("%d\n", *(int*)(root->data));
    /*  Process left child */  
    print2DUtil(root->children[0], space);  
}  
  
 
void print2D(avl_node_t *root)  
{  
    /*  Pass initial space count as 0 */  
    print2DUtil(root, 0);  
} 
int main()
{

    TestBig();
  /*   
    FailTest();
    TestRemoveRotateLeft(); 
    TestRotateLeft(); 
    TestRotateRight(); 
    TestRotateRightLeft(); 
    */

  /*   
    TestFind();
    TestForEach();
    TestCreate();
    TestInsert(); 
  */
    return 0;
}
 
int Is_before(const void* data1, const void* data2)
{
   return ((*(int *)data1 - *(int *)data2) > 0);
}
void FailTest(void)
{
    avl_t *tree = AVLCreate(Is_before);
    int data[]={ 58,89,47,97,20,65,5,7,66,53,21,78,22,39,64,49,77,55,14,42 ,
    29,81,24,60,31,34,50,30,84,99,51,56,15,61,79,93,67,83,82,10,96,2,52,45,70,
    62,94,72,92,71,43,27,80,41,32,88,38,73,4,3,0,17,40,33,28,12,90,9,25,76,18,69,
    48,1,8,26,59,68,98,23,74,54,44,16,11,91,35,36,87,86,37,6,46,13,57,85,95,19,75,63 };
    size_t size = sizeof(data)/sizeof(data[0]);
    size_t i = 0;

    printf(KMAG"TestRotateRightLeft\n"KWHT); 

    for(i = 0; i < size; i++)
    {
        printf(KCYN"INSERT %d\n"KWHT, data[i]); 
        AVLInsert(tree, &data[i]);
        VerifySize(AVLSize(tree),i+1);
        VerifyHight(AVLHeight(tree), 1);

    }
    AVLForEach(tree, Action,NULL,IN_ORDER);
    printf("\n");

    print2D(tree->root);
    IsBalsncedTree(tree);


    for(i = 0; i < size-1; i++)
    {
        printf(KCYN"REMOVE %d\n"KWHT, data[i]); 
        AVLRemove(tree, &data[i]);
        VerifyHight(AVLHeight(tree), 1);
        IsBalsncedTree(tree);
        printf("\n\n\n\n");

        print2D(tree->root);

    }

 

}

void TestRotateRightLeft(void)
{
    avl_t *tree = AVLCreate(Is_before);
    int data[]={50,40,60,65,55,35,37,36};
    size_t size = sizeof(data)/sizeof(data[0]);
    size_t i = 0;

    printf(KMAG"TestRotateRightLeft\n"KWHT); 

    for(i = 0; i < size; i++)
    {
        printf(KCYN"INSERT %d\n"KWHT, data[i]); 
        AVLInsert(tree, &data[i]);
        VerifySize(AVLSize(tree),i+1);
        VerifyHight(AVLHeight(tree), 1);
    }
 
    AVLForEach(tree, Action,NULL,IN_ORDER);
    printf("\n");
    print2D(tree->root);

    AVLDestroy(tree);
}

void TestRotateLeft(void)
{
    avl_t *tree = AVLCreate(Is_before);
    int data[]={50,40,30};
    size_t size = sizeof(data)/sizeof(data[0]);
    size_t i = 0;

    printf(KMAG"TestRotateLeft\n"KWHT); 

    for(i = 0; i < size; i++)
    {
        printf(KCYN"INSERT %d\n"KWHT, data[i]); 
        AVLInsert(tree, &data[i]);
        VerifySize(AVLSize(tree),i+1);
        VerifyHight(AVLHeight(tree), 1);
    }
 
    AVLForEach(tree, Action,NULL,IN_ORDER);
    printf("\n");
    print2D(tree->root);
    AVLDestroy(tree);
}


void TestRemoveRotateLeft(void)
{
    avl_t *tree = AVLCreate(Is_before);
    int data[]={40,30,50};
    size_t size = sizeof(data)/sizeof(data[0]);
    size_t i = 0;

    printf(KMAG"TestRotateLeft\n"KWHT); 

    for(i = 0; i < size; i++)
    {
        printf(KCYN"INSERT %d\n"KWHT, data[i]); 
        AVLInsert(tree, &data[i]);
        VerifySize(AVLSize(tree),i+1);
        VerifyHight(AVLHeight(tree), 1);
    }
 
    AVLForEach(tree, Action,NULL,IN_ORDER);
    printf("\n");
    print2D(tree->root);
   
    AVLRemove(tree, &data[0]);
      printf("\n");
     
   print2D(tree->root);
   AVLForEach(tree, Action,NULL,IN_ORDER); 
   /* 
    printf("\n");
    
    AVLDestroy(tree);
   */
}
void TestRotateRight(void)
{
    avl_t *tree = AVLCreate(Is_before);
    int data[]={50,60,70};
    size_t size = sizeof(data)/sizeof(data[0]);
    size_t i = 0;

    printf(KMAG"TestRotateRight\n"KWHT); 

    for(i = 0; i < size; i++)
    {
        printf(KCYN"INSERT %d\n"KWHT, data[i]); 
        AVLInsert(tree, &data[i]);
        VerifySize(AVLSize(tree),i+1);
        VerifyHight(AVLHeight(tree), 1);
    }
 
    AVLForEach(tree, Action,NULL,IN_ORDER);
    printf("\n");

    AVLDestroy(tree);
}
void TestCreate(void)
{
    avl_t *tree = AVLCreate(Is_before);
    printf(KMAG"TestCreate\n"KWHT);
    if (NULL != tree)
    {
        printf(KGRN"GOOD\n"KWHT);
    }
    else
    {
        printf(KRED"FAIL\n"KWHT);
    }
    
    VerifySize(AVLSize(tree),0);
    VerifyIsEmpty(AVLIsEmpty(tree),1);
    VerifyHight(AVLHeight(tree), 0);

    AVLDestroy(tree);

}
int Action(void * data, void *param)
{
    printf("%d ", *(int *)data);
    return 0;
    (void)param;
}
void TestInsert(void)
{
    avl_t *tree = AVLCreate(Is_before);
    int data[]={50,40,30,20,45,60,55,65};
    size_t size = sizeof(data)/sizeof(data[0]);
    size_t i = 0;

    printf(KMAG"TestInsert\n"KWHT); 

    for(i = 0; i < size; i++)
    {
        printf(KCYN"INSERT %d\n"KWHT, data[i]); 
        AVLInsert(tree, &data[i]);
        VerifySize(AVLSize(tree),i+1);
        VerifyHight(AVLHeight(tree), 1);
    }
    AVLRemove(tree, &data[3]);
    VerifySize(AVLSize(tree),size-1);
    AVLForEach(tree, Action,NULL,IN_ORDER);
    printf("\n");

    AVLDestroy(tree);
}

void TestForEach(void)
{
    avl_t *tree = AVLCreate(Is_before);
    int data[]={50,40,30,20,45,60,55,65};
    size_t size = sizeof(data)/sizeof(data[0]);
    size_t i = 0;

    printf(KMAG"TestForEach\n"KWHT); 

    for(i = 0; i < size; i++)
    {
        printf(KCYN"INSERT %d\n"KWHT, data[i]); 
        AVLInsert(tree, &data[i]);
        VerifySize(AVLSize(tree),i+1);
        VerifyHight(AVLHeight(tree), 1);
    }
    AVLForEach(tree, Action,NULL,IN_ORDER);
    printf("\n");


    AVLDestroy(tree);
}

void TestFind(void)
{
    avl_t *tree = AVLCreate(Is_before);
    int data[]={50,40,30,20,45,60,55,65};
    int fail = 21;
    void* find = NULL;
    size_t size = sizeof(data)/sizeof(data[0]);
    size_t i = 0;

    printf(KMAG"TestFind\n"KWHT); 

    for(i = 0; i < size; i++)
    {
        printf(KCYN"INSERT %d\n"KWHT, data[i]); 
        AVLInsert(tree, &data[i]);
        VerifySize(AVLSize(tree),i+1);
        VerifyHight(AVLHeight(tree), 1);
    }
    AVLForEach(tree, Action,NULL,IN_ORDER);
    printf("\n");
    printf("DATA :%d \n",data[1]);

    printf("FIND :%d \n",*(int *)AVLFind(tree, &data[1]));
    find = AVLFind(tree, &fail);
    if (NULL == find)
    {
        printf(KGRN"Is NULL\n"KWHT);
    }

    AVLDestroy(tree);
}
/******************************************************************************/


#define SIZE (500)
#define SIZE_INSERT (100)
#define SIZE_REMOVE (50)

void TestBig()
{
    avl_t *tree  = AVLCreate(Is_before);
    int arr[SIZE] = {0};
    size_t i = 0;
    size_t j = 0;
    size_t size = 0;
    int inserted[SIZE_INSERT] = {0};
    srand(time(NULL));

    printf(KCYN"BIG TEST\n"KWHT);

    for(i = 0; i < SIZE; i++)
    {
        arr[i] = rand()% 100;
    }
    
    i = 0;
    
    while (i < SIZE && j < SIZE_INSERT)
    {
        if (NULL == AVLFind(tree, &arr[i]))
        {
            printf("%d,",arr[i]);
            AVLInsert(tree, &arr[i]);
            inserted[j] = arr[i];
       
            j++;
            size++;
        }
       i++;
    }
    
    VerifySize(AVLSize(tree),SIZE_INSERT);
    IsBalsncedTree(tree);

    print2D(tree->root); 
    printf("REMOVE\n");
 
    for (i = 0; i < 50; i++)
    {     
        printf("%d,",inserted[i]);
        AVLRemove(tree, &inserted[i]);
    }
     
   IsBalsncedTree(tree); 
    print2D(tree->root); 
    
}
/******************************************************************************/
void VerifySize(size_t result, size_t expected)
{
    if(result == expected)
    {
        printf(KGRN"GOOD.Size\n"KWHT);
    }
    else
    {
        printf(KRED"FAIL.Size %ld\n"KWHT,result);
    }
}


void VerifyIsEmpty(int result, int expected)
{
    if(result == expected)
    {
        printf(KGRN"GOOD.IsEmpty\n"KWHT);
    }
    else
    {
        printf(KRED"FAIL.IsEmpty\n"KWHT);
    }
}

void VerifyHight(int result, int expected)
{
    if(result == expected)
    {
        printf(KGRN"GOOD.Hight\n"KWHT);
    }
    else
    {
        printf(KRED"FAIL.Hight %d\n"KWHT, result);
    }
}
