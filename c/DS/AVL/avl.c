
 /******************************************************************************
 *          * AVL *
 *          Description: Balance Tree
 *
 *          Version:  1.0
 *
 ******************************************************************************/
#include <stdlib.h>/*malloc*/
#include "avl.h"

enum child{LEFT, RIGHT, NUM_OF_CHILDREN};
struct avl 
{
	is_before isbefore;			 
	avl_node_t *root;
};

struct node
{
	void *data;				
	struct node *children[NUM_OF_CHILDREN];
	size_t node_height;
};

/****************************SUB FUNCTIONS*************************************/
avl_node_t *FindPrev        (avl_node_t *node, void **to_return);
avl_node_t *SubRemove       (avl_node_t *node, avl_t *btree, void *data);
avl_node_t* RemoveNode      (avl_node_t *node);
avl_node_t *LeftCh          (avl_node_t *node);
avl_node_t *RightCh         (avl_node_t *node);
size_t SubSize              (avl_node_t *node);
void SubDestroy             (avl_node_t *node);
avl_node_t * Prev                 (avl_node_t *node,void**data_return);
void *SubFind               (avl_node_t *node,avl_t *btree, void *data);

avl_node_t *RightRotation   (avl_node_t* node);
avl_node_t *LeftRotation    (avl_node_t* node);

int NoChildren              (avl_node_t *node);
int HasOneChild             (avl_node_t *node);
int HasTwoChildren          (avl_node_t *node);

void UpdateHeight           (avl_node_t *node);
int GetHeight               (avl_node_t *node);
int IsBalnced               (avl_node_t *node);
int DifferenceHeight        (avl_node_t *node);
avl_node_t *RemoveBalance   (avl_node_t* node);
avl_node_t *InsertBalance   (avl_node_t* node, avl_t *btree, void*data);
avl_node_t *LeftLeft        (avl_node_t *node);
avl_node_t *LeftRight       (avl_node_t* node);
avl_node_t *RightLeft       (avl_node_t *node);
avl_node_t *RightRight      (avl_node_t* node);

void SwapData               (void**data, void**data2);
void SubInsert              (avl_node_t *node, avl_node_t *to_insert,avl_t *btree, 
                                                                    int side);

/******************************************************************************/
int IsMatch(avl_t*btree, void*data1, void*data2)
{
    return (btree->isbefore(data1, data2)) == (btree->isbefore(data2,data1));
}

avl_node_t * CreateNode(void *data)
{
    avl_node_t *node = malloc(sizeof(avl_node_t));
    if(NULL == node)
    {
        return NULL;
    }

    node->data = data;
    node->children[LEFT] = NULL;
    node->children[RIGHT] = NULL;
    node->node_height = 1;

    return node;
}
/******************************************************************************/
avl_t *AVLCreate(int(*is_before)(const void* data1, const void* data2))
{
    avl_t *tree = malloc(sizeof(avl_t));
    if (NULL == tree)
    {
        return NULL;
    }

    tree->isbefore = is_before;
    tree->root = NULL;

    return tree;
}
/******************************************************************************/

void AVLDestroy(avl_t *btree)
{
    if(!AVLIsEmpty(btree))
    {
        SubDestroy(btree->root);
    }

    free(btree);
    btree = NULL;
}

void SubDestroy(avl_node_t *node)
{
    if (NULL == node)
    {
        return;
    }
    
    SubDestroy(node->children[LEFT]);
    SubDestroy(node->children[RIGHT]);
    
    free(node);
    node = NULL;
}
/******************************************************************************/

size_t AVLSize(const avl_t *btree)
{
    if(NULL == btree->root)
    {
        return 0;
    }

    return SubSize(btree->root);
}

size_t SubSize(avl_node_t *node)
{
    if (NULL == node)
    {
        return 0;
    }

    return 1 + SubSize(node->children[LEFT]) + SubSize(node->children[RIGHT]);
}
/******************************************************************************/
size_t AVLHeight(const avl_t *btree)
{
    if(NULL == btree->root)
    {
        return 0;
    }

    return btree->root->node_height;
}
/******************************************************************************/
int AVLIsEmpty(const avl_t *btree)
{
    return (btree->root == NULL);
}
/******************************************************************************/
int AVLInsert(avl_t *btree, void *data)
{
    avl_node_t * to_insert = CreateNode(data);
    if(NULL == to_insert)
    {
        return 1;
    }
    
    if (NULL == btree->root)
    {
       btree->root = to_insert;
    }
    else
    {
        SubInsert(btree->root, to_insert, btree, LEFT);
        UpdateHeight(btree->root);
        if (!IsBalnced(btree->root))
        {
            btree->root = InsertBalance(btree->root, btree, to_insert->data);
        }  
    } 
    
    return 0;
}


void SubInsert(avl_node_t *node, avl_node_t *to_insert,avl_t *btree, int side)
{
    if (btree->isbefore(node->data, to_insert->data))
    {
        side = LEFT;
    }
    else
    {
        side = RIGHT;
    }

    if (NULL == node->children[side]),inserted[i]
    {
         node->children[side] = to_insert;
         return;
    }
    
    SubInsert(node->children[side], to_insert, btree, side);
    UpdateHeight(node->children[side]); 
    if (!IsBalnced(node->children[side]))
    {
      node->children[side] = InsertBalance(node->children[side], btree,
                                                         to_insert->data);
    }  
}

/******************************************************************************/

void AVLRemove(avl_t *btree, void *data)
{
    avl_node_t*to_return =NULL;
    to_return = SubRemove(btree->root, btree, data);
    btree->root = to_return;
    UpdateHeight(btree->root); 
    if (!IsBalnced(btree->root))
    {
       btree->root = RemoveBalance( btree->root);
    }   
 }

 avl_node_t *SubRemove (avl_node_t *node, avl_t *btree, void *data)
{
    int side = 0;
    avl_node_t *child = NULL;
    
    if (IsMatch(btree,node->data, data))
    {
        return RemoveNode(node);
    }
     if (btree->isbefore(node->data, data))
    {
        side = LEFT;
    }
    else
    {
        side = RIGHT;
    } 

    child = SubRemove(node->children[side], btree, data);
    node->children[side] = child;
    UpdateHeight(node); 
    if (!IsBalnced(node))
    {
      node = RemoveBalance(node);
    }  

    return node;
} 

avl_node_t* RemoveNode(avl_node_t *node)
{
    avl_node_t *to_return = NULL;
    if (NoChildren(node))
    {
        free(node);
        node = NULL;
       
    }
    else if(HasOneChild(node))
    {
        if(NULL != LeftCh(node))
        {
            to_return = LeftCh(node);
        }
        else
        {
            to_return = RightCh(node);
        }

        free(node);
        node = NULL;
    }
    
    else if(HasTwoChildren(node))
    {
        node = Prev(node,&(node->data));
        to_return = node;
    } 

    return to_return;
}
  
/******************************************************************************/
void *AVLFind(avl_t *btree, void *to_find)
{
    return SubFind(btree->root,btree,to_find);
}

void *SubFind(avl_node_t *node,avl_t *btree, void *data)
{
    int side = 0;
    if(NULL == node)
    {
        return NULL;
    }
    if(!btree->isbefore(node->data, data) && 
        !btree->isbefore( data, node->data))
    {
        return node->data;
    }
    if (btree->isbefore(node->data, data))
    {
        side = LEFT;
    }
    else
    {
        side = RIGHT;
    }
    return SubFind(node->children[side], btree, data);
} 
/******************************************************************************/
int AVLForEach(avl_t *btree, int (*action)(void* data, void* param), void* param,
                                                    traversal_order_t order)
{
    int status =0;
    switch (order)
    {
    case PRE_ORDER:
         status = PreOrder(btree->root, action, param);
         break;
    
    case POST_ORDER:
        status = PostOrder(btree->root, action, param);
         break;

    case IN_ORDER:
        status= InOrder(btree->root, action, param);
         break;
    }
    return status;
}

int InOrder(avl_node_t *node, int (*action)(void* data, void* param), void* param)
{
    int status = 0;
    
    if(NULL == node)
    {
        return 0;
    }

    status = InOrder(node->children[LEFT], action, param);
    if(0 != status)
    {
        return status;
    }
    
    status = action(node->data,param);
    if(0 != status)
    {
        return status;
    }
    
    return InOrder(node->children[RIGHT], action, param);
}


int PostOrder(avl_node_t *node, int (*action)(void* data, void* param), void* param)
{
    int status = 0;
    
    if(NULL == node)
    {
        return 0;
    }

    status = PostOrder(node->children[LEFT], action, param);
    if(0 != status)
    {
        return status;
    }
    
    status = PostOrder(node->children[RIGHT], action, param);
    if(0 != status)
    {
        return status;
    }
    status = action(node->data,param);
    if(0 != status)
    {
        return status;
    }
    return status;
}


int PreOrder(avl_node_t *node, int (*action)(void* data, void* param), void* param)
{
    int status = 0;
    
    if(NULL == node)
    {
        return 0;
    }

    PreOrder(node->children[LEFT], action, param);
    if(0 != status)
    {
        return status;
    }
    
    action(node->data,param);
    if(0 != status)
    {
        return status;
    }
    
    return PreOrder(node->children[RIGHT], action, param);
} 
/******************************************************************************/
avl_node_t *LeftCh(avl_node_t *node)
{
    return node->children[LEFT];
}

avl_node_t *RightCh(avl_node_t *node)
{
    return node->children[RIGHT];
}

avl_node_t* Prev(avl_node_t *node, void **to_return)
{

    if(NULL == RightCh(LeftCh(node)))
    {
        SwapData(to_return, &(node->children[LEFT]->data));
        node->children[LEFT]= RemoveNode(node->children[LEFT]);
        UpdateHeight(node); 
        if (!IsBalnced(node))
        {
        node = RemoveBalance(node);
        }  
    }
    else
    {
        node->children[LEFT]=FindPrev(node->children[LEFT],to_return);
        UpdateHeight(node); 
        if (!IsBalnced(node))
        {
            node = RemoveBalance(node);
        }  
    }
    return node;
     
}

avl_node_t *FindPrev(avl_node_t *node, void **to_return)
{
    avl_node_t *child = NULL;
    if(NULL == RightCh(node))
    {
        SwapData(to_return, &(node->data));
        return RemoveNode(node);
    }

    node->children[RIGHT] = FindPrev(RightCh(node), to_return);
    UpdateHeight(node); 
    if (!IsBalnced(node))
    {
      node = RemoveBalance(node);
    }  
    return node;
}

int NoChildren(avl_node_t *node)
{
    return (NULL == RightCh(node) && NULL == LeftCh(node));
}

int HasOneChild(avl_node_t *node)
{
    return  (NULL != RightCh(node) && NULL == LeftCh(node)) ||
            (NULL == RightCh(node) && NULL != LeftCh(node));
}

int HasTwoChildren(avl_node_t *node)
{
    return (NULL != RightCh(node) && NULL != LeftCh(node));
} 


void SwapData(void**data, void**data2)
{
    void*tmp = *data;
    *data = *data2;
    *data2 = tmp;;
}

#define MAX_NUM_2(a,b)((a)>(b)?(a):(b))
void UpdateHeight(avl_node_t *node)
{
    int h_left = 0;
    int h_right = 0;
  
    if (NULL != node->children[LEFT])
    {
        h_left = node->children[LEFT]->node_height;
    }

    if (NULL != node->children[RIGHT])
    {
        h_right = node->children[RIGHT]->node_height;
    }

    node->node_height =  MAX_NUM_2(h_left, h_right) + 1;
}

int IsBalnced(avl_node_t *node)
{
    return (abs(DifferenceHeight(node)) <= 1);
}

int GetHeight(avl_node_t *node)
{
    int height = 0;
    if (NULL != node)
    {
        height = node->node_height;
    }
    return height;
}

int DifferenceHeight(avl_node_t *node)
{
    return (GetHeight(node->children[LEFT]) - GetHeight(node->children[RIGHT]));
}

avl_node_t *InsertBalance(avl_node_t* node, avl_t *btree, void*data)
{
    avl_node_t *node_to_return = NULL;
    if (btree->isbefore(node->data ,data)) /* left */
    {
        if (btree->isbefore(node->children[LEFT]->data ,data))  /*left left*/
        {
            node_to_return = LeftLeft(node);
        }
        else /*left right*/
        {
            node_to_return = LeftRight(node);
        }
    }
    else
    {
        if (!btree->isbefore(node->children[RIGHT]->data ,data))  /*right right*/
        {
            node_to_return = RightRight(node);
        }
        else /*right right*/
        {
            node_to_return = RightLeft(node);
        }
    }

    return node_to_return;
}


avl_node_t *RemoveBalance(avl_node_t* node)
{
    avl_node_t *node_to_return = NULL;
    if (DifferenceHeight(node)>1) /* left */
    {
        if (DifferenceHeight(node->children[LEFT])>=1)  /*left left*/
        {
            node_to_return = LeftLeft(node);
        }
        else /*left right*/
        {
            node_to_return = LeftRight(node);
        }
    }
    else
    {
        if (DifferenceHeight(node->children[RIGHT])< 1)  /*right right*/
        {
            node_to_return = RightRight(node);
        }
        else /*right left*/
        {
            node_to_return = RightLeft(node);
        }
    }

    return node_to_return;
}
avl_node_t * LeftLeft(avl_node_t *node)
{
    avl_node_t *node_to_return = RightRotation(node);
    UpdateHeight(node_to_return->children[LEFT]);
    UpdateHeight(node_to_return->children[RIGHT]); 
    UpdateHeight(node_to_return);
    return node_to_return;
}
avl_node_t * LeftRight(avl_node_t* node)
{
    avl_node_t *node_to_return = LeftRotation(node->children[LEFT]);
    node->children[LEFT]= node_to_return;
    node_to_return = RightRotation(node);
    UpdateHeight(node_to_return->children[LEFT]);
    UpdateHeight(node_to_return->children[RIGHT]);
    UpdateHeight(node_to_return);
    return node_to_return;
}

avl_node_t * RightLeft(avl_node_t *node)
{
    avl_node_t * node_to_return = RightRotation(node->children[RIGHT]);
    node->children[RIGHT] = node_to_return;
    node_to_return = LeftRotation(node);
    UpdateHeight(node_to_return->children[LEFT]);
    UpdateHeight(node_to_return->children[RIGHT]);
    UpdateHeight(node_to_return);
    return node_to_return;
}
avl_node_t * RightRight(avl_node_t*node)
{
    avl_node_t *node_to_return = LeftRotation(node);
    UpdateHeight(node_to_return->children[LEFT]);
    UpdateHeight(node_to_return->children[RIGHT]);
    UpdateHeight(node_to_return);
    return node_to_return;
}

avl_node_t* RightRotation(avl_node_t* node)
{
    avl_node_t* new_head = node->children[LEFT];
    node->children[LEFT] = new_head->children[RIGHT];
    new_head->children[RIGHT] = node;
    return new_head;
}

 avl_node_t* LeftRotation(avl_node_t* node)
{
    avl_node_t* new_head = node->children[RIGHT];
    node->children[RIGHT] = new_head->children[LEFT];
    new_head->children[LEFT] = node;
    return new_head;

}
