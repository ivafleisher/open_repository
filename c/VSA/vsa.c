
#include <stdlib.h> /* abs */
#include <assert.h> /* assert */
#include <stdio.h> /* stdio */
#include "vsa.h"
/* Variable Size Allocator imitates the behavior of malloc and free */
#define MAGIC ((size_t)1111)
#define HEADER (sizeof(header_t))
#define WORD_SIZE (sizeof(size_t))
#define ASSERT_NOT_ZERO(x) (assert(0 != x))
#define ASSERT_MORE_24B(x) (assert(24 != x))
#define ASSERT_NOT_NULL(x) (assert(NULL != x))

typedef struct header
{
    long size;
    
    #ifndef NDEBUG
	size_t  magic_number;
    #endif 
  
}header_t ;

struct allocator 
{
    header_t *end;
};

/***********************************SUPPORT FUNC*******************************/

static long SizeToFree      (header_t *current, size_t size);
static long SizeToAlloc     (header_t *current, size_t size);
static int IsEnd            (header_t *header,  vsa_t *allocator);
static int HaveNextFree     (header_t *header,  vsa_t *allocator);
static header_t *GetNext    (header_t *header);
static int IsFree           (header_t *header);
static size_t GetSize       (header_t *header);
static size_t AlignedSize   (size_t size_block);
static size_t AlignedBuffer (size_t size_block);
static void Defragment      (header_t *header);

/******************************************************************************/

/*
 * Input: (void *)pointer to allocated buffer, (size_t)size of buffer in bytes.
 * Process: Create and initialize the allocator;
 * Return: (vsa_t *)pointer to the allocator;
 */
vsa_t *VSAInit(void *buffer, size_t buffer_size)
{
    vsa_t *allocator = (vsa_t *)buffer; 
    header_t *header = (header_t *)(allocator + 1);
    
    ASSERT_NOT_NULL(buffer);
    ASSERT_MORE_24B(buffer_size);
    
    allocator->end  = (header_t*)((char*)allocator + AlignedBuffer(buffer_size));
    SizeToFree(header, AlignedBuffer(buffer_size) - sizeof(vsa_t) - HEADER);
    
    return allocator;
}

/*
 * Input: (vsa_t *)pointer to allocator and size of the block.
 * Process: allocate block of memory, defragments if needed;
 * Return:(void *)return pointer to the block or NULL if there is no sufficient chunk;
 */
void *VSAAlloc(vsa_t *allocator, size_t block_size)
{
    header_t* next = NULL;
    size_t prev_size = 0;
    header_t *header = (header_t *)(allocator + 1);
    size_t size = AlignedSize(block_size);

    ASSERT_NOT_NULL(allocator);
    ASSERT_NOT_ZERO(block_size);

    while (!IsEnd(header, allocator))
    {
        if (IsFree(header))
        {
            while (HaveNextFree(header, allocator)
                && GetSize(header) < size)
            {
                Defragment(header);
            }
            
            if (GetSize(header) >= size + HEADER + WORD_SIZE)
            {
                prev_size = GetSize(header);
                SizeToAlloc(header, size);
                next = GetNext(header);
                SizeToFree(next, prev_size - size - HEADER);
                
                #ifndef NDEBUG
                header->magic_number = MAGIC;
                #endif 
                
                return header + 1;
            }
            else if (GetSize(header) >= size ) 
            {
                SizeToAlloc(header, GetSize(header));
                
                #ifndef NDEBUG
                header->magic_number = MAGIC;
                #endif 
                
                return header + 1;
            }
        }

        header = GetNext(header);
    }
   
    return NULL;
}

/*
 * Input: (void *)pointer to block.
 * Process: free this block of memory;
 * Return: none;
 */
void VSAFree(void *block)
{
    header_t *header = NULL;

    ASSERT_NOT_NULL(block);
    
    header = (header_t *)( (char *)block - HEADER);
 
	assert(MAGIC == header->magic_number);
 
    SizeToFree(header, GetSize(header));
}

/*
 * Input: (vsa_t *)pointer to allocator.
 * Process: looks for the biggest chunk available, defragments if needed;
 * Return:(size_t)size of the biggest chunk found;
 */
size_t VSABiggestChunk(const vsa_t *allocator)
{
    size_t biggest_chunk = 0;
    header_t *header = (header_t *)(allocator + 1);

    ASSERT_NOT_NULL(allocator);

    while (!IsEnd(header,  (vsa_t*)allocator))
    {
        if (IsFree(header))
        {
            while (HaveNextFree(header, (vsa_t*)allocator))
            {
                Defragment(header);
            }
           
            if (biggest_chunk < GetSize(header))
            {
                biggest_chunk = GetSize(header);
            }
        }

        header = GetNext(header);
    }

    return biggest_chunk;
}

/*******************************************************************************/

static long SizeToFree(header_t * current, size_t size)
{
    return (current->size = -(size));
}

static long SizeToAlloc(header_t * current, size_t size)
{    
    return (current->size = size);
}

static int IsEnd(header_t *header, vsa_t* allocator)
{
    return ((allocator->end)== header);
}

static int IsFree(header_t * header)
{
    return (header->size < 0);
}

static size_t GetSize(header_t *header)
{
    return abs(header->size);
}
static int HaveNextFree(header_t *header, vsa_t *allocator)
{
    header_t *next = GetNext(header);
    return !IsEnd(next, allocator) && IsFree(next);
}

static header_t *GetNext(header_t* header)
{
   return (header_t *)((char *)header + GetSize(header) + HEADER); 
}

static size_t AlignedSize(size_t size_block)
{
    if (0 == size_block % WORD_SIZE)
    {
        return size_block;
    }

    return size_block + (WORD_SIZE - (size_block % WORD_SIZE));
}

static size_t AlignedBuffer(size_t size_block)
{
    return size_block - (size_block % WORD_SIZE);
}

static void Defragment(header_t * header)
{
    header->size += (-GetSize(GetNext(header)))- HEADER;
}