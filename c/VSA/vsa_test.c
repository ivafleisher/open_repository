#include <stdio.h>/*printf*/
#include <stdlib.h>/*malloc*/
#include "vsa.h"

#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"

struct allocator 
{
    void *end;
};

static void TestCreate              ();
static  void TestAlloc              ();

static void VerifyFreeSize          (size_t result , size_t expected);
static void VerifyBiggestChunk(size_t result, size_t expected);
/******************************************************************************
                                    * MAIN *       
******************************************************************************/

int main()
{
 
    TestCreate();
    TestAlloc();

    return 0;
}


static void Print(long *buff, size_t size)
{
    size_t i = 0;

    printf(KBLU"diff %lu  \n "KWHT, ((size_t )buff[0] - (size_t)buff));
    printf(KCYN"end %lu |"KWHT, buff[0]);
    
    
    for (i = 1; i < size; i++)
    {
        printf("%ld ", buff[i]);
    }
}


#define SIZE ((size_t )48)
/* struct |end ptr8 , magic number = 8,long  header = 8; + sizeof element aligned */
static void TestCreate ()
{
    long *buffer = malloc (SIZE);
    vsa_t * allocator = VSAInit(buffer, 48);
    printf(KMAG"\nTestCreate "KWHT);
    VerifyBiggestChunk(VSABiggestChunk(allocator), 24);
     
    Print(buffer, 6);
    free(buffer);

}

static void TestAlloc()
{
    long *buffer = malloc (100);
    vsa_t *allocator = VSAInit(buffer, 100);
    size_t data[] = {10,11,12,13};
    size_t sizes_of_element[] = {8,15,8,8};
    size_t biggest[] = {48, 16, 0, 8, 0,16, 48};
    size_t size = sizeof(data )/ sizeof(data[0]);
    long *to_free[4] = {NULL};
    size_t i = 0;
    
    printf(KMAG"\nTestAlloc "KWHT);
    VerifyBiggestChunk(VSABiggestChunk(allocator), 72);
    Print(buffer, 12);

    for (i = 0; i < 3; i++)
    {
        printf(KBLU"\nALLOC \n"KWHT);
        to_free[i] = VSAAlloc(allocator, sizes_of_element[i]);
        *to_free[i] = data[i];
        VerifyBiggestChunk(VSABiggestChunk(allocator), biggest[i]);
        Print(buffer, 12);
    }

    printf(KBLU"\n FREE \n"KWHT);
    VSAFree( to_free[0]);
    VerifyBiggestChunk(VSABiggestChunk(allocator), biggest[i]);
    Print(buffer, 12);  

    printf(KBLU"\nALLOC \n"KWHT);
    to_free[3] = VSAAlloc(allocator, 8);
    *to_free[3] = 13;
    VerifyBiggestChunk(VSABiggestChunk(allocator), biggest[i + 1]);

    Print(buffer, 12); 

    for(i = 1; i < 3; i++)
    {
        printf(KBLU"\n FREE \n"KWHT);
        VSAFree( to_free[i]);
        VerifyBiggestChunk(VSABiggestChunk(allocator), biggest[i + 4]);
        Print(buffer, 12);   
    }

    free(buffer);

}

static void VerifyBiggestChunk(size_t result, size_t expected)
{
    if (result == expected)
    {
        printf(KGRN"GOOD.VerifyBiggestChunk result : %lu\n"KWHT, result);
    }
    else
    {
        printf(KRED"FAIL.VerifyBiggestChunk result : %lu, expected : %lu \n"KWHT,
         result, expected);
    }
}