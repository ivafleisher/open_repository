#ifndef __WQUEUE_HPP
#define __WQUEUE_HPP
/******************************************************************************
 * API WQUEUE
 * 11.08.2020
******************************************************************************/
#include <iostream>
#include <boost/chrono.hpp>
#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/thread/locks.hpp>
#include <boost/noncopyable.hpp>

namespace dp
{

template <class QUEUE, typename T >
class WaitableQueue : private boost::noncopyable
{
public:
    // default Ctor and Dtor
    typedef boost::posix_time::milliseconds Millisec;
    void Push(const T& val);
    void Pop(T& peaked_value); // will use front and pop
    bool Pop(T& peaked_value, Millisec timeout);
    bool Empty() const; //please make sur to use in a thread-safe environment

private:
    QUEUE m_queue;
    boost::condition_variable m_pushflag; //use boost::unique_lock
    mutable boost::mutex m_mutex;//use boost::unique_lock
};

/******************************************************************************
 * IMPLEMENTATION OF WQUEUE
******************************************************************************/
template <class QUEUE, typename T >
void WaitableQueue< QUEUE, T >::Push (const T& val)
{
    boost::unique_lock<boost::mutex> scoped_lock(m_mutex);

    m_queue.push(val);
    m_pushflag.notify_all();
}
/******************************************************************************/
template <class QUEUE, typename T >
void WaitableQueue< QUEUE,  T >::Pop(T& peaked_value)
{
    boost::unique_lock<boost::mutex> scoped_lock(m_mutex);
    while(Empty())
    {
        m_pushflag.wait(scoped_lock);
    }

    peaked_value = m_queue.front();
    m_queue.pop();
}
/******************************************************************************/

template <class QUEUE, typename T >
bool WaitableQueue< QUEUE,  T >::Pop(T& peaked_value, Millisec timeout)
{
    bool time_ = 0;
    boost::unique_lock<boost::mutex> scoped_lock(m_mutex);

    boost::system_time duration = boost::get_system_time() + timeout;

    while(Empty())
    {
        if(!m_pushflag.timed_wait(scoped_lock, duration))
        {
            return false;
        }
    }
    
    peaked_value = m_queue.front();
    m_queue.pop();
 
    return 1;
}
/******************************************************************************/

template <class QUEUE, typename T >
bool WaitableQueue< QUEUE,  T >:: Empty()const
{
    return m_queue.empty();
}


} // namespace dp

#endif // __WQUEUE_HPP           
