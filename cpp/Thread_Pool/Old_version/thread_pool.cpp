#include "thread_pool.hpp"
#include <boost/thread.hpp>
/******************************************************************************
 *	Title:		Implementation Thraed Pool
 *	Authour:	Ivanna Fleisher
 ******************************************************************************/
namespace dp
{
/******************************************************************************/
void ThreadPool::ThreadWork(int position)
{
    Task temp;
    while(m_is_run[position])
    {
        if(m_paused)
        {
            boost::unique_lock<boost::mutex> scoped_lock(m_pause_mutex);
            while(m_paused)
            {
                m_cond.wait(scoped_lock);
            }
        }
        if(m_tasks.Pop(temp,WaitableQueue<std::queue<Task>,Task >::Millisec(10000))) // will use front and pop
        {
            temp();
        }
    }
}

ThreadPool::Task::~Task(){}

void ThreadPool::Task::operator()()
{
    m_exec_func();
}

ThreadPool::ThreadPool(size_t num_of_threads)
:m_total_num(num_of_threads),m_paused(false), m_is_run(true)
{
    for(size_t i = 0 ;i < m_total_num; ++i)
    {
        boost::shared_ptr <boost::thread> thread(new boost::thread(boost::bind
                                        (&ThreadPool::ThreadWork, this, _1), i));
        m_threads.push_back(thread); 
        m_is_run.push_back(1);   
    }
}

void ThreadPool::AddTask(const Task& task)
{
    m_tasks.Push(task);
}

bool ThreadPool::SetThreadsAmount(size_t new_amount) //Set a new amount of threads
{
    size_t i = 0;
    if (new_amount > m_total_num)
    {
        for (i = 0;  i < (new_amount - m_total_num); ++i)
        {
        
            boost::shared_ptr <boost::thread> thread(new boost::thread(boost::bind
                                            (&ThreadPool::ThreadWork, this, _1), i));
            m_threads.push_back(thread);                   
            m_is_run.push_back(1);                
        }
    }
    else
    {
        for (i = 0;  i < (m_total_num - new_amount); ++i)
        {
            m_is_run[m_total_num - i-1] = 0;
            m_threads[m_total_num - i-1]->join();
            m_threads.pop_back();
            m_is_run.pop_back();                
        }
    }

    m_total_num = new_amount;

    return 0;
}

void ThreadPool::Stop() //Exits all threads
{
    for(size_t i = 0 ;i < m_threads.size(); ++i)
    {
        m_is_run[i] = 0; 
        m_threads[i]->join();
    }
}

void ThreadPool::Pause()//Pause all threads
{
    boost::unique_lock<boost::mutex> scoped_lock(m_pause_mutex);
    m_paused = true;
}

void ThreadPool::Resume() //Resume all threads to work again
{
    boost::unique_lock<boost::mutex> scoped_lock(m_pause_mutex);
    
    m_paused = false;
    m_cond.notify_all();
}

ThreadPool::~ThreadPool()
{
    Stop();
}

} // namespace dp
