#ifndef __THREAD_POOL_HPP
#define __THREAD_POOL_HPP

#include <boost/noncopyable.hpp>
#include <boost/thread.hpp>
#include <vector>
#include <queue>
#include <boost/function.hpp>
#include <boost/thread/mutex.hpp>

#include "waitable_queue.hpp"

namespace dp
{

static void Default(){}
class ThreadPool:private boost::noncopyable
{
public:

    typedef boost::function< void(int) > ActionFunc;
    /****************************************************/
    enum PRIORITY
    {
        LOW,
        MIDDLE,
        HIGH
    };
    /****************************************************/
    
    class Task
    {
    public:
        Task(ActionFunc func = Default):m_exec_func(func){}
        virtual ~Task();
        virtual void operator()();
    private:
        ActionFunc m_exec_func;
    };
    /****************************************************/

    explicit ThreadPool(size_t num_of_threads);
    void AddTask(const Task& task, PRIORITY priority); 
    bool SetThreadsAmount(size_t new_amount); //Set a new amount of threads
    void Stop(); //Exits all threads
    void Pause(); //Pause all threads
    void Resume(); //Resume all threads to work again
    ~ThreadPool();
private:
    size_t m_total_num;
    std::vector<boost::shared_ptr <boost::thread> > m_threads;
    WaitableQueue<std::queue<Task>, Task> m_tasks;

    boost::condition_variable m_cond;//
    boost::mutex m_pause_mutex;//

    std::vector<int> m_is_run; //
    bool m_paused;

    void ThreadWork(int position);
   
};

} // namespace dp

#endif // __THREAD_POOL_HPP
