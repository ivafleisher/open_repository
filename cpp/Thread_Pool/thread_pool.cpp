/******************************************************************************
 *	Title:		Implementation Thraed Pool
 *	Authour:	Ivanna Fleisher
 ******************************************************************************/
#include <iostream>
#include "thread_pool.hpp"

namespace dp
{

/*******************************THREAD WORK************************************/
void ThreadPool::PopAndExecute()
{
#ifndef R
    std::cout << "Pop AND Execute" << std::endl;
#endif

    boost::shared_ptr<Task> task_ptr(new Task());
    PriorityAndTask to_pop(MIDDLE, task_ptr);
   // bool &flag_ref = m_threads.find(boost::this_thread::get_id())->second.first;

    while(m_threads.find(boost::this_thread::get_id())->second.first == true)
    {
        m_tasks.Pop(to_pop);
#ifndef R
        std::cout <<"pop the task "<<to_pop.first<<std::endl;
#endif
        (*(to_pop.second))(0);
    }

#ifndef R
    std::cout <<"DELETE MY SELF"<<std::endl;
    std::cout << boost::this_thread::get_id()<< std::endl;
#endif

    m_toJoin.Push(m_threads.find(boost::this_thread::get_id())->second.second);
    m_threads.erase(boost::this_thread::get_id());

#ifndef R
    std::cout <<"ERESE MY SELF"<<std::endl;
#endif
}
/******************************************************************************/
void ThreadPool::Task::operator()(int param)
{
    std::cout <<"FUNCTION "<<&m_exec_func<<std::endl;
    m_exec_func(param);
}
/******************************************************************************/

ThreadPool::ThreadPool(size_t num_of_threads)
    :m_numOfThreads(num_of_threads),m_paused(false)
{}

void ThreadPool::AddTask(Task *task,ThreadPool::Priority priority)
{
    PriorityAndTask to_insert(priority, boost::shared_ptr<Task>(task));
    m_tasks.Push(to_insert);
}

void ThreadPool::Start()
{
    RunNThreads(m_numOfThreads);
}

void ThreadPool::Stop()
{
#ifndef R
    std::cout<<"TO STOP"<<std::endl;
#endif

    SetThreadsAmount(0);
}

/******************************************************************************/

void ThreadPool::Pause()//Pause all threads
{
    boost::shared_ptr<Task> paused_task(new Task
                    (boost::bind(&ThreadPool::PauseFunc, this,_1)));

    PriorityAndTask p_and_t (SYSTEM, paused_task);
    
    if(!m_paused)
    {
        m_paused = true;

        for (size_t i = 0; i < m_numOfThreads; ++i)
        {
            m_tasks.Push(p_and_t);
        }
    }
}

void ThreadPool::Resume() //Resume all threads to work again
{
    boost::unique_lock<boost::mutex> scoped_lock(m_pause_mutex);

    m_paused = false;
    m_cond.notify_all();
}

bool ThreadPool::SetThreadsAmount(size_t new_num)
{
    if (new_num >= m_numOfThreads)
    {
        size_t diff = new_num - m_numOfThreads;
        RunNThreads(diff);
    }
    else 
    {
        size_t m_thread_to_remove = m_numOfThreads - new_num;
        DeleteAndJoin(m_thread_to_remove);
    }

    m_numOfThreads = new_num;

    return 0;
}

ThreadPool::~ThreadPool()
{
    Stop();
}
    
/***************************Tasks for implementation***************************/
void ThreadPool::TerminateFunc(int num)
{
#ifndef R
    std::cout << "in termination function" << std::endl;
#endif

    (m_threads.find(boost::this_thread::get_id()))->second.first = false;
    (void)num;
}

void ThreadPool::PauseFunc(int num)
{
#ifndef R
    std::cout <<"want to Pause"<<std::endl;
#endif
  
    boost::unique_lock<boost::mutex> scoped_lock(m_pause_mutex);
    while(m_paused)
    {
        m_cond.wait(scoped_lock);
    }

    (void)num;
}

void ThreadPool::RunNThreads(size_t num_of_threads)
{
    for (size_t i = 0; i < num_of_threads; ++i)
    {
        boost::shared_ptr<boost::thread> ptr(new boost::thread
        (boost::bind(&ilrd::ThreadPool::PopAndExecute, this)));

        m_threads.insert(std::pair<boost::thread::id,
                    FlagandThread>(ptr->get_id(),FlagandThread(true,ptr)));
    }
}

void ThreadPool::DeleteAndJoin(size_t m_thread_to_remove)
{
    boost::shared_ptr<Task> termination_task(new Task
                    (boost::bind(&ThreadPool::TerminateFunc, this,_1)));

    PriorityAndTask p_and_t (SYSTEM, termination_task);
    
    for (size_t i = 0; i < m_thread_to_remove; ++i)
    {
        m_tasks.Push(p_and_t);
    }

    while(m_thread_to_remove)
    {
        shared_thread_ptr return_val;
        
        m_toJoin.Pop(return_val);
        return_val->join();
        
        --m_thread_to_remove;
    }
}
} //namespace dp
