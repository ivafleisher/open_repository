#ifndef __THREAD_POOL_HPP
#define __THREAD_POOL_HPP

#include <boost/noncopyable.hpp>
#include <boost/thread.hpp>
#include <vector>
#include <queue>
#include <map>
#include <boost/function.hpp>
#include <boost/thread/mutex.hpp>
#include <boost/bind.hpp>
#include "waitable_queue.hpp"
#include "priority_queue.hpp"

//#include "logger.hpp"

namespace dp
{

static void Default(int num){(void)num;}

class ThreadPool:private boost::noncopyable
{
public:
/*******************************PRIORITY***************************************/
    enum Priority
    {   
        SYSTEM = 0,
        HIGH ,
        MIDDLE,
        LOW
    };
/*******************************TASK*******************************************/
    class Task;
    typedef boost::function< void(int) > ActionFunc;
    typedef std::pair<Priority, boost::shared_ptr<Task> > PriorityAndTask;

    class Task
    {
    public:
        Task(ActionFunc func = Default):m_exec_func(func){}
        virtual ~Task(){}
        virtual void operator()(int param);
    private:
        ActionFunc m_exec_func;
    };
/********************************THREAD POOL***********************************/
    explicit ThreadPool(size_t num_of_threads);
    ~ThreadPool();
    
    void AddTask(Task *task,ThreadPool::Priority priority = MIDDLE);
    bool SetThreadsAmount(size_t new_amount); //Set a new amount of threads
    
    void Start(); //Run the pool of the threads
    void Stop();  //Exits all threads
    void Pause(); //Pause all threads
    void Resume(); //Resume all threads to work again

    struct Compare//compare function for the waitable<priority queue>
    {
        bool operator() (PriorityAndTask a, PriorityAndTask b)
        {
            return (a.first > b.first);
        }
    };
private:
    typedef boost::shared_ptr< boost::thread > shared_thread_ptr;
    typedef std::pair<bool, shared_thread_ptr > FlagandThread;
    
    /* THREAD POOL CONF */
    size_t m_numOfThreads;
    std::map< boost::thread::id, FlagandThread > m_threads;
    
    /* TO REMOVE */
    WaitableQueue<std::queue<shared_thread_ptr>,shared_thread_ptr> m_toJoin;

    /* PAUSE / RESUME */
    bool m_paused;
    boost::condition_variable m_cond;
    boost::mutex m_pause_mutex;
    
    /* FOR TASKS */
    WaitableQueue<PriorityQueue<PriorityAndTask, std::vector<PriorityAndTask>,
                            ThreadPool::Compare >,  PriorityAndTask> m_tasks;

private: //tasks for implementation   
    void PopAndExecute();
    void PauseFunc(int num);
    void TerminateFunc(int num);
    void RunNThreads(size_t num_of_threads);
    void DeleteAndJoin(size_t m_thread_to_remove);
};

}//namespace dp

#endif // __THREAD_POOL_HPP
