/*****************************************************************************
 * Test Thread Pool Test
 * Ivanna Fleisher
******************************************************************************/
#include <iostream>

#include "thread_pool.hpp"
/******************************************************************************/
#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"
/******************************************************************************/

using namespace dp;
#define THREADS_NUM (10)

void Task1(int num)
{
    std::cout << "******************I'm task: " << num << std::endl;
}

void Task2(int num)
{
    std::cout << "I'm task: 2" << std::endl;
    (void)num;
}

void Task3(int num)
{
    std::cout << "I'm task: 3" << std::endl;
    (void)num;
}

void Task4(int num)
{
    std::cout << "I'm task: 4" << std::endl;
    (void)num;
}

void Test_Start_Dtor_OneThread()
{
    std::cout << KMAG<<"Test_Start_Dtor_OneThread"<< KWHT << std::endl;

    ThreadPool::Task *task_ptr = new ThreadPool::Task(Task1);
    ThreadPool my_pool(1);

    std::cout << KCYN<<"ADD TASKS"<< KWHT << std::endl;
    my_pool.AddTask(task_ptr);

    std::cout << KCYN<<"START"<< KWHT << std::endl;
    my_pool.Start();
     
    my_pool.Stop();
    std::cout << KCYN<<"AFTER STOP"<< KWHT << std::endl;
}

#define SEC_TO_WAIT (3)
void Test_Start_Pause_Resume()
{
    std::cout << KMAG<<"Test_Start_Pause_Resume"<< KWHT << std::endl;
    ThreadPool::Task *task_ptr = new ThreadPool::Task(Task1);
    ThreadPool::Task *task_ptr2 = new ThreadPool::Task(Task2);

    ThreadPool my_pool(2);

    std::cout << KCYN<<"ADD TASKS"<< KWHT << std::endl;
    my_pool.AddTask(task_ptr);
    my_pool.AddTask(task_ptr2);

    std::cout << KCYN<<"START"<< KWHT << std::endl;
    my_pool.Start();
    
    std::cout << KCYN<<"PAUSE for"<< SEC_TO_WAIT<<"seconds"<<KWHT << std::endl;
    my_pool.Pause();
    sleep(SEC_TO_WAIT);
    my_pool.Resume();

    my_pool.Stop();
    std::cout << KCYN<<"AFTER STOP"<< KWHT << std::endl;
}


#define THREADS (10)
void Test_NThreads()
{
    std::cout << KMAG<<"Test_Start_Pause_Resume"<< KWHT << std::endl;
    ThreadPool::Task *task_ptr = new ThreadPool::Task(Task1);
    ThreadPool::Task *task_ptr2 = new ThreadPool::Task(Task2);

    ThreadPool my_pool(THREADS);

    std::cout << KCYN<<"ADD TASKS"<< KWHT << std::endl;
    my_pool.AddTask(task_ptr);
    my_pool.AddTask(task_ptr2);

    std::cout << KCYN<<"START"<< KWHT << std::endl;
    my_pool.Start();
    
    std::cout << KCYN<<"PAUSE for"<< SEC_TO_WAIT<<"seconds"<<KWHT << std::endl;
    my_pool.Pause();
    sleep(SEC_TO_WAIT);
    my_pool.Resume();
    //std::cout << KCYN<<"ADD TASKS"<< KWHT << std::endl;

   /* 
   my_pool.AddTask(task_ptr);

    my_pool.AddTask(task_ptr2); */

    std::cout << KCYN<<"Before STOP"<< KWHT << std::endl;
    my_pool.Stop();
}


int main ()
{
/*     Test_Start_Dtor_OneThread();
    Test_Start_Pause_Resume(); */
    Test_NThreads();

    return (0);
}
