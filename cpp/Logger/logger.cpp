/*******************************************************************************
                     * IMPLEMENTATION LOGGER *
 ******************************************************************************/

#include "logger.hpp"   //logger
#include <fstream>      // std::ofstream

namespace dp
{
Logger::Logger() : m_flag(1),
m_filename(getenv("LOGGERNAME")),
m_thread(boost::thread(boost::bind(&ilrd::Logger::PrintMessageIntoLogfile, this)))
{
    if(NULL == m_filename)
    {
        throw std::runtime_error("Logger:getenv(LOGGERNAME)\n");
    }
}

Logger::Message::Message(ErrorLevel elevel, std::string message) 
        :m_error(elevel),
        m_string(message),
        m_timepoint(boost::chrono::system_clock::now())
{}

// construct the message and push into the queue
void Logger::PushMessage(ErrorLevel elevel, std::string message)
{
    Message m(elevel, message);
    m_wqueue.Push(m);
} 

void  Logger::PrintMessageIntoLogfile()   // print the message 
{
    std::ofstream ofs;
    ofs.open(m_filename);
 
    while(m_flag)
    {
        //Take message
        Message msg;
        m_wqueue.Pop(msg);

        //convert time
        time_t t = boost::chrono::system_clock::to_time_t(msg.m_timepoint);

        //write to the file 
        ofs <<msg.m_error<<" :";
        ofs << std::ctime(&t);
        ofs << msg.m_string<<std::endl;
    }

    ofs.close();
}     
                                     
Logger::~Logger()
{ 
    m_flag = 0;
    PushMessage(INFO, "END_OF_THE_FILE");

    m_thread.join();
}
}//namespace dp 
