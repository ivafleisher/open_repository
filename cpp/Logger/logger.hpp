#ifndef __LOGGER_HPP__
#define __LOGGER_HPP__

#include <stdlib.h> 
#include <iostream>
#include <string>
#include <boost/chrono.hpp>
#include <boost/bind.hpp>
#include "singleton.hpp"
#include "waitable_queue.hpp"
#include "priority_queue.hpp"
#include <boost/thread/thread.hpp>//boost::threads

namespace dp
{

class Logger
{
public:
    enum ErrorLevel {
        ERROR = 0,
        WARNING,
        INFO,
        DEBUG
    };
    // construct the message and push into the queue
    void PushMessage(ErrorLevel elevel, std::string str); 

//privat methods
    Logger();
    ~Logger();
private:

    struct Message
    {
        // constructor computes time here
        Message(ErrorLevel elevel = ERROR, std::string message = " ");

        inline bool operator>(const Message& other_) const
        {
            return (m_timepoint > other_.m_timepoint);
        }

        ErrorLevel m_error;
        std::string m_string;
        boost::chrono::system_clock::time_point m_timepoint;
    };

    friend class Singleton<Logger>;//thread which will take care of it 
    void PrintMessageIntoLogfile();// print the message in the logfile 
                                   //with error level and timepoint
    
private://members
    int m_flag;
    char * m_filename;
    WaitableQueue<PriorityQueue<Message, std::vector<Message>, std::greater<Message> > , Message> m_wqueue;
    //to set up in the constructor 
    //to run PrintMessageIntoLogfile
    boost::thread m_thread; 
};

#define LOG_ERROR(msg) (Singleton<Logger>::GetInstance()->PushMessage(Logger::ERROR, (msg)))
#define LOG_WARNING(msg) (Singleton<Logger>::GetInstance()->PushMessage(Logger::WARNING, (msg)))
#define LOG_INFO(msg) (Singleton<Logger>::GetInstance()->PushMessage(Logger::INFO, (msg)))
#define LOG_DEBUG(msg) (Singleton<Logger>::GetInstance()->PushMessage(Logger::DEBUG, (msg)))

}

#endif /* __LOGGER_HPP__ */
