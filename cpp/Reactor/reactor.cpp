/*******************************************************************************
                     * IMPLEMENTATION LISTENER*
 ******************************************************************************/
#include "reactor.hpp"
/*******************************************************************************
                     * IMPLEMENTATION REACTOR*
 ******************************************************************************/

int Reactor::m_g_stop = 0;

void Reactor::Add(HandleAndMode handle_and_mode, HandleFunc func)
{
    m_EventHandlers.insert(std::pair<HandleAndMode, HandleFunc>(handle_and_mode, func));
}

void Reactor::Remove(HandleAndMode handle_and_mode)
{
    m_EventHandlers.erase(handle_and_mode);
}

void Reactor::Stop()
{
    m_g_stop = 1;
}

void Reactor::Run()
{
    std::vector<HandleAndMode>to_listen;
    std::map<HandleAndMode, HandleFunc>::iterator it;

    for ( it = m_EventHandlers.begin(); it != m_EventHandlers.end(); it++ )
    {
        to_listen.push_back(it->first); 
    }       

    while(!m_EventHandlers.empty() && !m_g_stop)
    {
        std::vector<HandleAndMode>to_handle = m_Listener->Listen(to_listen);

        for (size_t i = 0; i < to_handle.size(); i++)
        {  
            it =  m_EventHandlers.find(to_handle[i]);

            if (it != m_EventHandlers.end())
            {
                it->second(to_handle[i].second);
            }

            if(m_g_stop)
            {
                return ;
            }
        }

    }
}
