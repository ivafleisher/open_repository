
/******************************************************************************
 *	Title:		TesT REACTOR
 *	Authour:	Ivanna Fleisher

 gppd reactor.cpp reactor_test.cpp ../../system_programming/Server_PP/servers_imp.c 
 -I../../system_programming/Server_PP/include 
 ******************************************************************************/
#include "servers.h"    /* implementation part of servers p_p */
#include <stdlib.h>     /* exit                               */
#include <string.h>     /* strlen                             */
#include <netinet/in.h> /* sockaddr_in                        */
#include <unistd.h>     /* close                              */
#include <stdio.h>      /* printf, perror                     */

#include <sys/select.h>

/* According to earlier standards */
#include <sys/time.h>
#include <sys/types.h>

#include "reactor.hpp"  //REACTOR api
#include "listener.hpp"    //servers api

#define PORT (8080)
#define MAXLINE (1024 )
#define MAX_CLIENT (30)
#define MAXFUNC(a,b)((a)>(b)?(a):(b))


char buffer[MAXLINE]; 
Reactor reactor(new DerievedListener());
void Client_TCP(int fd)
{
    TCP_Answer(fd, "hi from tcp server to client tcp");
}

void HandleFirst(int fd)
{ 
    printf("^^^^RECEIVE MESSAGE UDP^^^^^\n");
    int n = 0;
    struct sockaddr_in address_peer; 
    int addrlen = sizeof(struct sockaddr_in);
    bzero(buffer, sizeof(buffer));

    printf("\nMessage from UDP client: "); 
    n = recvfrom(fd, buffer, sizeof(buffer), 0, 
                    (struct sockaddr*)&address_peer, (socklen_t *)&addrlen); 
    buffer[n] = '\0'; 
    printf("... %s\n", buffer); 
    sendto(fd, "hello from UDP server", strlen("hello from UDP server"), 0, 
    (struct sockaddr*)&address_peer, sizeof(address_peer));
    reactor.Stop();
}

void HandleSecond(int fd)
{
    printf("^^^^RECEIVE MESSAGE TCP^^^^^\n");
    struct sockaddr_in address_peer; 
    int addrlen = sizeof(struct sockaddr_in);

    int new_socket = accept(fd, (struct sockaddr*)&address_peer, (socklen_t*)&addrlen);

    HandleAndMode new_s(READ,new_socket);
    TCP_Answer(new_socket, "hello from tcp server");
    //add to the reactor
    reactor.Add(new_s,Client_TCP);
   
}

int main()
{   
    /* sets of file descriptors*/  
    int sock[SIZE] = {0};
    struct sockaddr_in address[SIZE] = {0};
    /* initialize all sockets */
    sock[UDP] = CreateSocket(SOCK_DGRAM);
    sock[TCP] = CreateSocket(SOCK_STREAM);

    SocketInfo(&address[UDP],sock[UDP], INADDR_ANY, htons(PORT));
    SocketInfo(&address[TCP],sock[TCP], INADDR_ANY, htons(PORT));

    listen(sock[TCP], 10); 
    /***********************************************************/
    //Create reactor
    HandleAndMode first(READ,sock[UDP]);
    HandleAndMode second(READ,sock[TCP]);

    reactor.Add(first,HandleFirst); 
    reactor.Add(second,HandleSecond);
    reactor.Run();

    return 0;
}
