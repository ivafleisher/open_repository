
/******************************************************************************
 *	Title:		LISTENER
 *	Authour:	Ivanna Fleisher
 ******************************************************************************/
#include <stdio.h>      /* printf, perror */
#include <stdlib.h>     /* exit */
#include <unistd.h>     /* close */
#include <sys/select.h>

#include "listener.hpp"  //REACTOR api
#define MAXFUNC(a,b)((a)>(b)?(a):(b))

std::vector<HandleAndMode> DerievedListener::Listen(const std::vector<HandleAndMode>& handle)
{
    std::vector<HandleAndMode> return_vector;
    //time limit
    struct timeval tv;
    tv.tv_sec = 7;
    tv.tv_usec = 0; 

    //write,read, exceptiom
    fd_set sets[3];  
    size_t i = 0;

#ifdef DEBG// for print      
    printf("PUT TO LIST\n");
#endif
    int maxfdp1 = 0;
    for(i = 0 ; i < handle.size(); i++)
    {
        FD_SET(handle[i].second, &sets[handle[i].first]);
        maxfdp1 = MAXFUNC(maxfdp1, handle[i].second) + 1;
    }
#ifdef DEBG// for print   
    printf("Before SELECT\n");
#endif   

    int nready = select(maxfdp1, &sets[READ],NULL ,NULL, &tv);

    if(nready < 0)
    {
        printf("NO TO READ\n");
    }
    else if (nready == 0)
    {
        printf("7 sec dont have notifications\n");
    }
    else
    {
        for(i = 0; i < handle.size(); i++)
        {
            if(FD_ISSET(handle[i].second, &sets[handle[i].first]))
            {
#ifdef DEBG// for print   
                printf("*****************PUSH TO HANDLE\n");
#endif     
                HandleAndMode return_pair(handle[i].first, handle[i].second);
                return_vector.push_back(return_pair);

            }
        } 

    }


return return_vector;

}