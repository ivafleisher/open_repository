#ifndef __LISTENER_HPP__
#define __LISTENER_HPP__

#include "reactor.hpp"  //REACTOR api

class DerievedListener: public IListener
{
public:
    ~DerievedListener(){};
    std::vector<HandleAndMode> Listen(const std::vector<HandleAndMode>& handle);
}; 

#endif /* __LISTENER_HPP__*/
