#ifndef __PRIORITYQUEUE_HPP
#define __PRIORITYQUEUE_HPP

#include <iostream>
#include <queue>
#include <vector>

template <typename T, class Container = std::vector<T>,
  class Compare = std::less<typename Container::value_type> >
class PriorityQueue : private std::priority_queue<T, Container, Compare>
{
public:
    explicit PriorityQueue(const Compare& comp = Compare(),
                         const Container& ctnr = Container());
    template <class InputIterator>
    PriorityQueue(InputIterator first, InputIterator last,
                         const Compare& comp = Compare(),
                         const Container& ctnr = Container());//not mandatory (bonus)
    void push (const T& val);
    void pop(void);
    const T& front() const;
    bool empty() const;
};

/******************************************************************************
 * IMPLEMENTATION OF PQUEUE
******************************************************************************/
    template < typename T, class Container ,class Compare >
    PriorityQueue<T,Container,Compare>::PriorityQueue(const Compare& comp ,
                                                    const Container& ctnr)
        :std::priority_queue<T, Container, Compare>(comp, ctnr)
    {}

    template < typename T, class Container ,class Compare >
    template <class InputIterator>
    PriorityQueue<T,Container,Compare>::PriorityQueue(InputIterator first, 
                                                    InputIterator last,
                                                    const Compare& comp,
                                                    const Container& ctnr)
        :std::priority_queue<T, Container, Compare>(first,last,comp, ctnr)
    {}
    
    template < typename T, class Container ,class Compare >
    inline void PriorityQueue<T,Container,Compare>::push (const T& val)
    {
        std::priority_queue<T, Container, Compare>::push(val);
    }
    
    template < typename T, class Container ,class Compare >
    inline void PriorityQueue<T,Container,Compare>::pop(void)
    {
        std::priority_queue<T, Container, Compare>::pop();
    }
    
    template < typename T, class Container ,class Compare >
    inline const T& PriorityQueue<T,Container,Compare>::front() const
    {
        return std::priority_queue<T, Container, Compare>::top();
    }
    
    template < typename T, class Container ,class Compare >
    inline bool PriorityQueue<T,Container,Compare>::empty() const
    {
        return std::priority_queue<T, Container, Compare>::empty();
    }


#endif // __PRIORITYQUEUE_HPP
