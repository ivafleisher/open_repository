
/*****************************************************************************
 * Test Priority Queue
 * Ivanna Fleisher

******************************************************************************/
//#include <iostream>
#include "priority_queue.hpp"
/******************************************************************************/
#define KNRM  "\x1B[0m"
#define KRED  "\x1B[31m"
#define KGRN  "\x1B[32m"
#define KYEL  "\x1B[33m"
#define KBLU  "\x1B[34m"
#define KMAG  "\x1B[35m"
#define KCYN  "\x1B[36m"
#define KWHT  "\x1B[37m"
/******************************************************************************/

void TestPushPop();
/******************************************************************************/

template <typename T>
void Verify(T result ,T expected ,const char *str)
{
    if (result == expected)
    {
        std::cout << KGRN << "GOOD." << str<< KWHT << std::endl;
    }
    else
    {
        std::cout << KRED << "Problem." << str<< KWHT << std::endl;
    }
}
/******************************************************************************/

int main()
{
    TestPushPop();
  

    return 0;
}

/******************************************************************************/

void TestPushPop()
{
    std::cout << KMAG<<"TestPush"<< KWHT << std::endl;

    int myints[]= {10,60,50,20};
    int results[]= {60,50,20,10};
    PriorityQueue< int >pq;
    size_t size = sizeof(myints)/sizeof(myints[0]);
    Verify<bool>(pq.empty(),1," Check Is empty");

    std::cout << KCYN<<"PUSH"<< KWHT << std::endl;

    for(size_t i = 0; i < size; i++)
    {
        pq.push(myints[i]);
        std::cout <<"PUSH:"<<myints[i]<<" FRONT:"<<pq.front()<<std::endl;
        Verify<bool>(pq.empty(),0," Check Is empty");
    }
    std::cout << KCYN<<"POP"<< KWHT << std::endl;

    for(size_t i = 0; i < size; i++)
    {
        Verify<int>(pq.front(),results[i]," Check Front");
        pq.pop();
    }

    Verify<bool>(pq.empty(),1," Check Is empty");
}

