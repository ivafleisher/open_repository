#ifndef FACTORY_HPP
#define FACTORY_HPP
/******************************************************************************
 * API FACTORY
 * 02.08.2020
******************************************************************************/
#include <iostream>
#include <map>
#include "boost/core/noncopyable.hpp"

namespace dp
{
template<class KEY,class BASE,class PARAMS ,class BUILDER = BASE* (*)(PARAMS)>
class Factory:private boost::noncopyable
{
public:
    // Add Function
    // Receives: key - key to the specific function to be created.
    //           creation_func - function which creates a specific object.
    // Exceptions: throws runtime_error if function is NULL;
    // Throws run-time exception if key already exist
    // BUILDER accepts functions which follows this declaration BASE* (*) (PARAMS) 
     
    void Add(KEY key, BUILDER creation_func);

    // Create Function
    // Receives: key - key to the specific function to be created.
    //           build_params - params that are needed for a creation function.
    // Exceptions: throws runtime_error if creation fails or key doesn't exist.
    BASE* Create(KEY key, PARAMS build_params)const;

private:
    std::map<KEY, BUILDER>m_factory_creation;
};
/******************************************************************************
 * IMPLEMENTATION OF FACTORY
******************************************************************************/
template< class KEY,class BASE,class PARAMS ,class BUILDER >
void Factory< KEY, BASE, PARAMS, BUILDER >::Add(KEY key, BUILDER creation_func)
{
    if(NULL == creation_func)
    {
        throw std::runtime_error("creation_func not exist");
    }

    std::pair<typename std::map<KEY, BUILDER>::iterator , bool> ret =
    m_factory_creation.insert(std::pair<KEY,BUILDER>(key, creation_func));

    if (false == ret.second) 
    {
        throw std::runtime_error("this key already exist");
    }
}

template< class KEY,class BASE,class PARAMS ,class BUILDER >
BASE* Factory< KEY, BASE, PARAMS, BUILDER >::Create(KEY key, PARAMS build_params)const
{
    typename std::map<KEY, BUILDER>::const_iterator it;
    if( m_factory_creation.end() == (it = m_factory_creation.find(key)))
    {
        throw std::runtime_error("this key not exist");
    }

    return it->second(build_params);
}

} // namespace dp

#endif // FACTORY_HPP           
