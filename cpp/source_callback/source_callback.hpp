#ifndef __ADVANCED_OBSERVER_HPP__
#define __ADVANCED_OBSERVER_HPP__
/******************************************************************************
 *	Title:		Advanced OBSERVER
 *	Authour:	Ivanna Fleisher
 ******************************************************************************/
#include <boost/core/noncopyable.hpp>
#include <boost/function.hpp>
#include <iostream>

#define LOG_ERR(x)(std::cerr<<"ERROR: "<<(x)<<std::endl)
#define LOG_WRN(x)(std::cerr<<"WARNING: "<<(x)<<std::endl)

template < typename SOURCE >
class Callback;

template < typename T >
class Source;

//EmptyDethFunc
/* void DeathEmpty()
{}
 */

/******************************************************************************/
template < typename T >
class Source //: private boost::noncopyable
{
public:
    Source();
    ~Source();
    typedef T dataType; // nested type

    void Subscribe(Callback< Source<T> >* callback);
    void Unsubscribe(Callback< Source<T> >* callback);
    void Notify(dataType data);

private:
    Callback< Source<T> >* m_callback;
};

template < typename SOURCE > 
class Callback : private boost::noncopyable
{
public:
    typedef boost::function< void(typename SOURCE::dataType) > CallbackPointer;
    //typedef boost::function< void() > DeathPointer;

    Callback(const CallbackPointer& func);
             //const DeathPointer&death_func = DeathPointerEmpty); 
             // initialize an empty function for death_func

    ~Callback();
private:
    //friend
    template<class T>
    friend class Source;

    // should move to the private section
    void Link(SOURCE* source);
    void Unlink();
    void Invoke(typename SOURCE::dataType data);
   // friend SOURCE;

    SOURCE* m_source;
    const CallbackPointer& m_func;
    //const DeathPointer& m_death_func;
    
};


/******************************************************************************
 *	Title:		IMPLEMENTATION Source
 *	Authour:	Ivanna Fleisher
 ******************************************************************************/

template < typename T >
Source<T>::Source()
:m_callback(NULL)
{}

template < typename T >
Source<T>::~Source()
{
    //check NULL
    if(NULL != m_callback)
    {
        m_callback->Unlink();
    }
}

template < typename T >
void Source<T>::Subscribe(Callback< Source<T> >* callback)
{
    if(!callback)
    {
        LOG_ERR("callback not exist");
    }
    else
    {
        m_callback = callback;
        m_callback->Link(this);
    }
    
}

template < typename T >
void Source<T>::Unsubscribe(Callback< Source<T> >* callback)
{
    //check NULL
    if(NULL != m_callback)
    {
        m_callback->Unlink();
        m_callback = NULL;
    }
}

template < typename T >
void Source<T>::Notify(dataType data)
{
    //check NULL
    if(NULL != m_callback)
    {
        m_callback->Invoke(data);
    }
}
/******************************************************************************
 *	Title:		IMPLEMENTATION Source
 *	Authour:	Ivanna Fleisher
 ******************************************************************************/

template < typename SOURCE > 
Callback<SOURCE>::Callback(const CallbackPointer& func)
                            //const DeathPointer& death_func) 
                            // initialize an empty function for death_func
    :m_source(NULL),m_func(func)//, m_death_func(death_func)
{}

template < typename SOURCE > 
Callback<SOURCE>::~Callback()
{
    //check NULL = source 
    //Unsubscribe 
    if(NULL != m_source)
    {
        m_source->Unsubscribe(this);
    }
    
   // m_death_func();
}
    // should move to the private section
template < typename SOURCE > 
void Callback<SOURCE>::Link(SOURCE* source)
{
    if(!source)
    {
        LOG_ERR("source not exist");
    }
    else
    {
        m_source = source;
    }
}
    
template < typename SOURCE > 
void Callback<SOURCE>::Unlink()
{
    m_source = NULL;
}

template < typename SOURCE > 
void Callback<SOURCE>::Invoke(typename SOURCE::dataType data)
{
    if(m_func)
    {
        m_func(data);
    }
    else
    {
        LOG_WRN("m_func not exist");
    }
}

#endif /* __ADVANCED_OBSERVER_HPP__*/
