/*****************************************************************************/
/*                     TEST SINGLETON                                      */
/*****************************************************************************/
#include <pthread.h> //threads
#include <iostream> // ostream
#include <fstream>      // std::ofstream
#include "singleton.hpp" // singleton implementation

using namespace dp; 

class Logger
{
public:
    Logger()
    {
        std::cout << "Create Logger"<<std::endl;
        if(num_init)
        {
            std::cout << "WHAT ????"<<std::endl;
        }
        ++num_init;

    }
    void Log( size_t x)
    {
        std::ofstream ofs;
        ofs.open ("test.txt", std::ofstream::out | std::ofstream::app);
     
        ofs << x;
        ofs.close();
    }
    static int num_init;
};

int Logger::num_init= 0;


void *myThreadFun(void *vargp) 
{ 
    Singleton<Logger>::GetInstance()->Log((size_t)(vargp));

    return NULL; 
} 

/*****************************************************************************/
/*                     MAIN                                                   */
/*****************************************************************************/

const int NUM_THREADS = 1000;
int main() 
{ 
    std::remove("test.txt"); // delete file
    size_t i = 0;

    pthread_t thread_id[NUM_THREADS]; 

    for (i = 0; i < NUM_THREADS; i++)
    {
        pthread_create(&thread_id[i], NULL, myThreadFun, (void*)i); 
    }

    for (i = 0; i < NUM_THREADS; i++)
    {
       pthread_join(thread_id[i], NULL);  

    }

    Singleton<Logger>::CleanUp();

    return 0;
}
