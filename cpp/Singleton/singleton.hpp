#ifndef __SINGLETON_HPP
#define __SINGLETON_HPP

#include <iostream>
#include <boost/atomic.hpp>
#include "boost/core/noncopyable.hpp"

namespace dp
{

template < typename T >
class Singleton: private boost::noncopyable
{
public:

    static T* GetInstance();
    static void CleanUp();

private:
    Singleton()
    {}
    static T * m_instance;
    static int m_init;
};

template<typename T> 
T* Singleton<T>::m_instance = NULL;
template<typename T> 
int Singleton<T>::m_init = 0;

template < typename T >
T *Singleton<T>::GetInstance()
{
    if(NULL == m_instance)
    {
        T* temp = NULL;
        if (0 ==__atomic_fetch_or(&m_init, 1, __ATOMIC_SEQ_CST))
        {
            try
            {
                temp = new T;
            }
            catch(...)
            {
                m_init = 0;
                throw;
            }
            
            __atomic_store_n (&m_instance, temp, __ATOMIC_SEQ_CST);
            __atomic_clear(&m_init,__ATOMIC_SEQ_CST);
        }
    }
    else
    {
        while(m_init)
        {}
    }

    return m_instance;
}

template<typename T> 
void Singleton<T>::CleanUp()
{
    delete m_instance;
}
} // namespace dp


#endif // __SINGLETON_HPP
