#include <cstring>
#include "command.hpp"
#include "logger.hpp"
/*****************************************************************************
 * Implementation Command
 * Ivanna Fleisher
******************************************************************************/
namespace ilrd
{

WriteCommand::WriteCommand(Params &params):m_params(params)
{
    *(m_params.response) = static_cast<Response*>(operator new(sizeof(Response)));
    memset(*(m_params.response), 0, Response().ResponseSize());
}

void WriteCommand::operator()()
{
    int m_index = be64toh(m_params.request.m_index);
    try
    {
        m_params.storage.Write(m_index, m_params.request.m_data);
    }
    catch(...)
    {
        (*(m_params.response))->m_status = 1;
        LOG_ERROR("Minion: Failed to read data");
    }
}

BaseCommand* WriteBuilder(Params m_params)
{
    return (new WriteCommand(m_params));
}
/******************************************************************************/
ReadCommand::ReadCommand(Params params): m_params(params)
{
    *(m_params.response) = static_cast<Response*>(operator new(sizeof(Response) + BLOCK_SIZE ));
    memset(*(m_params.response), 0, Request().RequestSize() + BLOCK_SIZE);
}

void ReadCommand::operator()()
{
    int m_index = be64toh(m_params.request.m_index);
    try
    {
        m_params.storage.Read(m_index, (*(m_params.response))->m_data);
    }
    catch(...)
    {
        (*(m_params.response))->m_status = 1;
        LOG_ERROR("Minion: Failed to read data");
    }
}
BaseCommand* ReadBuilder(Params m_params)
{
    return (new ReadCommand(m_params));
}



}// namespace ilrd;
