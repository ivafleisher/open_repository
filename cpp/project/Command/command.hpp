#ifndef _COMMAND_HPP
#define _COMMAND_HPP

#include "storage.hpp"
#include "protocol.hpp"

namespace ilrd
{

class BaseCommand
{
public:
    virtual void operator()() = 0;
    virtual ~BaseCommand(){};
};

struct Params
{
    Storage<BLOCK_SIZE>& storage;
    Response **response;
    const Request& request;
};

class WriteCommand : public BaseCommand
{
public:
    WriteCommand(Params &params);
    void operator()();
private:
    Params m_params;
};

class ReadCommand : public BaseCommand
{
public:
    ReadCommand(Params params);
    void operator()();
private:
    Params m_params;
};

//BUILDERS

BaseCommand* WriteBuilder(Params m_params);
BaseCommand* ReadBuilder(Params m_params);
}//namepsce ilrd

#endif // _COMMAND_HPP    
