/*****************************************************************************/
/*                     TEST STORAGE                                          */
/*****************************************************************************/
#include <iostream>         // ostream
#include "storage.hpp"      // storage

/*****************************************************************************/
/*                                  MAIN                                     */
/*****************************************************************************/
using namespace ilrd;
int main()
{ 

    Storage<20> storage(5);

    //WRITE
    char mydata[] ="hi, from Ukraine";
    char result[20]= {0};
    storage.Write(2,mydata);
    storage.Read(2,result);

    std::cout << result <<std::endl;

    return 0;
}



