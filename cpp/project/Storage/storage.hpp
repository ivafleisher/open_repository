#ifndef _STORAGE_HPP
#define _STORAGE_HPP
/******************************************************************************
 * Author:Ivanna Fleisher
 * API STORAGE
******************************************************************************/
#include <iostream>
#include <fstream>      // std::ofstream
#include "boost/core/noncopyable.hpp"
#include <boost/exception/to_string.hpp>  // boost::to_string
#include "logger.hpp"

namespace ilrd
{
template <size_t BLOCK_SIZE>
class Storage:private boost::noncopyable
{
public:
    explicit Storage(size_t num_of_blocks);
    // Write Function
    // Receives: index where to write data, pointer to source.
    // Returns: status of operation        
    void Write(size_t index, const void *src);
    // Read Function
    // Receives: index from where to read, pointer to store read data .
    // Returns: status of operation
    void Read(size_t index, void * dest) const;
    // GetCapacity Function
    // Receives: nothing.
    // Return: size_t number of blocks     
    size_t GetCapacity() const { return m_capacity; }

private:
    size_t m_capacity;
    const char * m_path;
};

/******************************************************************************
 * IMPLEMENTATION OF STORAGE
******************************************************************************/
template <size_t BLOCK_SIZE>
Storage<BLOCK_SIZE>::Storage(size_t num_of_blocks)
    :m_capacity(num_of_blocks), m_path("storage.txt")
{
    std::ofstream fs(m_path, std::fstream::app);
    fs.close();
}

template <size_t BLOCK_SIZE>
void Storage<BLOCK_SIZE>::Write(size_t index, const void *src) 
{
    if( index > m_capacity )
    {
        LOG_ERROR("Storage::Write:out of the boundary");
        throw std::runtime_error("out of the boundary");
    }

    std::fstream fs;
    fs.open (m_path);
    
    fs.seekp(BLOCK_SIZE * index, std::fstream::beg);
    fs.write(reinterpret_cast<const char *>(src), BLOCK_SIZE);
    //Log to the file
    std::string s("Storage::Write:to_index:");
    s += boost::to_string(index);
    LOG_INFO(s);

    fs.close();
}

template <size_t BLOCK_SIZE>
void Storage<BLOCK_SIZE>::Read(size_t index, void * dest) const
{
    if( index > m_capacity )
    {   
        LOG_ERROR("Storage::Read:out of the boundary");
        throw std::runtime_error("out of the boundary");
    }

    std::fstream fs;
    fs.open (m_path);

    fs.seekg(BLOCK_SIZE * index, std::fstream::beg);
    fs.read(reinterpret_cast<char *>(dest), BLOCK_SIZE);
    
    //Log to the file
    std::string s("Storage::Read:to_index:");
    s += boost::to_string(index);
    LOG_INFO(s);

    fs.close();
}

} // namespace ilrd

#endif // _STORAGE_HPP            
