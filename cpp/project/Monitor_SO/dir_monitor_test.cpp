/******************************************************************************
 *	Title:		TEST Dir_Monitor
 ******************************************************************************/
#include "dir_monitor.hpp"  //dir_monitoring
#include <boost/bind.hpp>   //bind
#include <unistd.h>         //sleep
#include <pthread.h>        //threads
#include <boost/thread/thread.hpp>//boost::threads
#include <iostream>         // ostream
#include <fstream>          // std::ofstream

using namespace ilrd;

class Observer
{
public:
    Observer():m_callback(boost::bind(&Observer::ToInvoke,this,_1)){}
    void ToInvoke(std::string str){std::cout<< str <<std::endl;}
    Callback< Dispatcher<std::string> >m_callback;
};

const int NUM_LIB = 4;
const int NUM_THREADS = 2;

//functor
struct CreateFile
{
    CreateFile(std::string *strgs) :m_array(strgs) { }
    void operator()()
    {
        for(int i = 0; i < NUM_LIB; i++)
        {
            std::ofstream ofs;
            ofs.open (m_array[i].c_str(), std::ofstream::out | std::ofstream::app);
            ofs.close(); 

            sleep(1);
        }
    }
private:
   std::string *m_array ;
   
};


//functor
struct Work
{
    void operator()()
    {
        for(int i = 0; i < 6; i++)
        {
            int j;
            std::cout << "Please enter an integer value: \n";
            std::cin >> j;
            std::cout << "The value you entered is \n" << j;
        }
    }
private:
   std::string *m_array ;
   
};

int main()
{
    std::string colour[NUM_LIB] = { "./monit/Blue.txt",
                                    "./monit/Red.txt",
                                    "./monit/Orange.txt",
                                    "./monit/Yellow.txt" };
    size_t i = 0;
    for(i= 0; i < NUM_LIB; i++)
    {
        std::remove(colour[i].c_str()); // delete file
    }

    boost::thread threads[NUM_THREADS];

    /* DirMonitor* ptr = */ DirMonitor monitor("./monit");
    Observer obs;
   
    monitor.Subscribe(& obs.m_callback);
    monitor.StartMonitoring();

    threads[0] = boost::thread(CreateFile(colour));
    threads[1] = boost::thread(Work());

    threads[0].join(); 
    threads[1].join(); 

    //sleep(7);


    return 0;
}