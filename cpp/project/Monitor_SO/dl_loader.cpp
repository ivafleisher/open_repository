/******************************************************************************
 *	Title:		Implementation DLLoader
 *	Authour:	Ivanna Fleisher
 ******************************************************************************/
#include "dl_loader.hpp"

namespace ilrd
{
//Phase3:
//DLLoader - Plug&Play Wrapper.

DLLoader::DLLoader(std::string plug_in_path)
    : m_plug_in_path(plug_in_path),
    m_dirmonitor(plug_in_path),
    m_callback(boost::bind(&ilrd::DLLoader::AddSharedObj,this,_1))
{
    m_dirmonitor.Subscribe(m_callback);
    m_dirmonitor.StartMonitoring(); 
}

DLLoader::~DLLoader()
{
    std::list<SharedObject *>::iterator it = m_libs.begin();
    while(it != m_libs.end())
    {
        delete (*it);
        ++it;
    }
}

void DLLoader::AddSharedObj(const std::string lib_name)
{
    SharedObject *new_shared_ob = new SharedObject(lib_name);
    m_libs.push_back(new_shared_ob);
}
 
}//namespace ilrd
