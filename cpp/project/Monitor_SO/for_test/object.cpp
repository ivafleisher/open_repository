#include <iostream>
#include <stdio.h>
#include "factory.hpp"      // factory
#include "singleton.hpp"    // singleton 
#include "shape.hpp"        // shape abstract
#include "line.hpp"         // line

using namespace ilrd;
Shape* CreateLine(void)
{
    return (new Line());
}

__attribute__((constructor)) void Init ()
{
    printf("Add new library /line/");
   Singleton< Factory<std::string,Shape, void,Shape*(*)(void)> >::GetInstance()->Add("line", CreateLine);
}