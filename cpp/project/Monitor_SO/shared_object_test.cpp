
/******************************************************************************
 *	Title:		TEST Shared_Object
 ******************************************************************************/
#include "shared_object.hpp"//shared_object

using namespace ilrd;
int main(int argc, char **argv)
{
    
    void (*Bar1)(void);
    
    SharedObject shared_ob("./for_test/libso1.so");
  
    *(void**)(&Bar1) = shared_ob.LoadSymbol<void (*)(void)> (argv[1]);
    
    std::cout <<Bar1<<std::endl;

    Bar1();


    return 0;
}