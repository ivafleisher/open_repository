 /******************************************************************************
 *	Title:		Implementation Dir_Monitor
 *	Authour:	Ivanna Fleisher
 ******************************************************************************/
#include <sys/inotify.h>//inotify
#include "dir_monitor.hpp"
#include <iostream>         // ostream
#include <fstream>          // std::ofstream
#include <string.h>

namespace ilrd
{

void DirMonitor::Subscribe(Callback<Dispatcher<std::string> > *callback)
{
    m_dispatcher.Subscribe(callback);
}

void DirMonitor::UnSubscribe(Callback<Dispatcher<std::string> > *callback)
{
    m_dispatcher.Unsubscribe(callback);
}

#define EVENT_SIZE  ( sizeof (struct inotify_event) + 1024)
#define DEFAULT_NAME "/default.txt"

void DirMonitor::operator()()
{
    char buffer[EVENT_SIZE];
    int fd = inotify_init();
    
    if ( fd < 0 ) 
    {
        throw std::runtime_error( "inotify_init" );
    }
    
    int wd = inotify_add_watch( fd, m_path.c_str(), IN_CREATE | IN_MODIFY |IN_OPEN);
    
    while(m_flag)
    {
        int length = read( fd, buffer,EVENT_SIZE); 
        if ( length < 0 ) 
        {
            throw std::runtime_error( "read" );
        }  

        struct inotify_event *event = ( struct inotify_event * ) &buffer;     
        if ( event->len ) 
        {
            if(!strcmp (event->name, DEFAULT_NAME))
            {
                std::string path(m_path.c_str() + std::string(DEFAULT_NAME));
                std::remove(path.c_str()); 
            }
            else if ( (event->mask & IN_CREATE) || (event->mask & IN_MODIFY) ||(event->mask & IN_OPEN )) 
            {
                m_dispatcher.Notify(event->name);
            }
        }
    }
    
    inotify_rm_watch( fd, wd );
    close( fd );
}

DirMonitor::~DirMonitor()
{
    m_flag = 0;
    
    std::string path(m_path.c_str() + std::string(DEFAULT_NAME));

    std::ofstream ofs;
    ofs.open (path.c_str(), std::ofstream::out | std::ofstream::app);
    ofs.close();

    m_thd_start.join();
}

void DirMonitor::StartMonitoring()
{
    m_thd_start = boost::thread(boost::bind(&DirMonitor::operator(), this));
}



}//namespace ilrd