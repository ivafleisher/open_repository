/******************************************************************************
 *	Title:		IMPLEMENTATION Shared_Object
 *	Authour:	Ivanna Fleisher
 ******************************************************************************/
#include "shared_object.hpp"
namespace ilrd
{

SharedObject::SharedObject(const std::string& library_path)
            :m_path(library_path),m_handle(dlopen(library_path.c_str(), RTLD_LAZY))
{
    if(NULL == m_handle)
    {
        throw std::runtime_error("NULL == m_handle");
    }
}

SharedObject::~SharedObject()
{
    dlclose(m_handle);
}
}//namespace ilrd