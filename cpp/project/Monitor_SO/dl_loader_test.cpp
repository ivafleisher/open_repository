/******************************************************************************
 *	Title:		TEST Dir_Monitor
 ******************************************************************************/

#include <boost/bind.hpp>   //bind
#include <unistd.h>         //sleep
#include <pthread.h>        //threads
#include <boost/thread/thread.hpp>//boost::threads
#include <iostream>         // ostream
#include <fstream>          // std::ofstream

#include <vector>           // vector
#include "factory.hpp"      // factory
#include "singleton.hpp"    // singleton 
#include "shape.hpp"        // singleton implementation
#include "circle.hpp"       // singleton implementation
#include "dl_loader.hpp"    //dir_monitoring, dllloader

using namespace ilrd;

Shape* CreateCircle(void)
{
    return (new Circle());
}

Shape* shape_circle = NULL;
Shape* shape_line = NULL;

Factory<std::string,Shape, std::vector<double>,Shape*(*)(void)> *factory =
Singleton< Factory<std::string,Shape, std::vector<double>,Shape*(*)(void)> >::GetInstance();


static void DrawFunction()
{ 
    shape_circle->Draw();
    shape_line->Draw();
    Singleton< Factory<std::string,Shape, std::vector<double>,Shape*(*)(void)> >::CleanUp();
} 

int main()
{
    std::cout <<"DLLOADER"<<std::endl;
    DLLoader dll_loader("./monit");

    factory->Add("circle", CreateCircle);
    shape_circle = factory->Create("circle", void);

    std::string new_shape_name;
    std::cin>>new_shape_name;

    shape_line = = factory->Create("line", void);
    /*--------------------------- mtrace(); */
    DrawInit(argc, argv, 1000, 1000, DrawFunction);
    DrawMainLoop(); 

    return 0;
}