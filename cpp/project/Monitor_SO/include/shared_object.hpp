#ifndef __SHARED_OBJECT_HPP__
#define __SHARED_OBJECT_HPP__

#include <iostream>
#include <string>
#include <cstdlib>
#include <dlfcn.h>
#include <string.h>


namespace ilrd
{

//SharedObject class with unittest.
class SharedObject
{
public:
    explicit SharedObject(const std::string& library_path);
    ~SharedObject();

    template <typename T >
    T* LoadSymbol(std::string symbol);

private:
    std::string m_path;
    void * m_handle;    
};


template <typename T >
T* SharedObject::LoadSymbol(std::string symbol)
{
    void * to_return = dlsym(m_handle, symbol.c_str());

    if(NULL == to_return)
    {
        throw std::runtime_error("NULL == to_return");
    }
    return reinterpret_cast<T*>(to_return);
}

}//namespace ilrd
#endif // __SHARED_OBJECT_HPP__

