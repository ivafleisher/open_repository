
#ifndef __DIR_MONITOR_HPP__
#define __DIR_MONITOR_HPP__

#include <string>//std::string
#include <boost/thread/thread.hpp>//boost::threads
#include "dispatcher.hpp"

/******************************************************************************
 *	Title:		API Dir_Monitor
 *	Authour:	Ivanna Fleisher
 ******************************************************************************/
namespace ilrd
{
//Phase2:
//implement DirMonitor with unittest.
class DirMonitor
{   
public:
    explicit DirMonitor(std::string dir_path): m_path(dir_path),m_flag(1){}
    void Subscribe(Callback<Dispatcher<std::string> > *callback);
    void UnSubscribe(Callback<Dispatcher<std::string> > *callback);
    void StartMonitoring();
    ~DirMonitor();
private:
    std::string m_path;
    boost::thread m_thd_start;
    Dispatcher<std::string> m_dispatcher;
    
private:
    bool m_flag;
    void operator ()();
};
}//namespace ilrd
#endif // __DIR_MONITOR_HPP__
