
#ifndef __DLLOADER_HPP__
#define __DLLOADER_HPP__
/******************************************************************************
 *	Title:		API DLLoader
 *	Authour:	Ivanna Fleisher
 ******************************************************************************/
#include <string>         // ostream
#include "shared_object.hpp"
#include "dispatcher.hpp"
#include "dir_monitor.hpp"

namespace ilrd
{
//Phase3:
//I'm chaning the DLLoader to be our Plug&Play Wrapper.
class DLLoader
{
public:
    explicit DLLoader(std::string plug_in_path);
    ~DLLoader();
    
private:
    void AddSharedObj(const std::string lib_name);
    
    std::string m_plug_in_path;
    std::list<SharedObject *> m_libs;
    
    DirMonitor m_dirmonitor;
    Callback<Dispatcher <std::string> > m_callback;
};
}//namespace ilrd
#endif // __DLLOADER_HPP__
