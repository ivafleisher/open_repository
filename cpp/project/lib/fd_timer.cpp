/*******************************************************************************
    	*       IMPLEMENTATION FD_TIMER   	   *
 ******************************************************************************/

#include "fd_timer.hpp"
#include <sys/timerfd.h>//timer_fd

namespace ilrd
{
    FDTimer::FDTimer(Reactor& reactor, ActionFunc callback_func)
        :m_reactor(reactor),
        m_callback(callback_func),
        m_fd(timerfd_create(CLOCK_REALTIME, 0))
    { 
        if(-1 == m_fd)
        {
            throw std::runtime_error("file descriptor can't be created");
        }

        m_reactor.Add(HandleAndMode(READ,m_fd),&m_callback);
    }

    FDTimer::~FDTimer()
    {
        close(m_fd);
        m_reactor.Remove(HandleAndMode(READ,m_fd));
    }

    void FDTimer::Set(MS milliseconds)
    {
        struct itimerspec new_value;

        new_value.it_value.tv_sec = milliseconds.count()/1000;
        new_value.it_value.tv_nsec = (milliseconds.count()%1000)*1000000;

        new_value.it_interval.tv_sec = 0;
        new_value.it_interval.tv_nsec = 0;
        
        if (timerfd_settime(m_fd, 0, &new_value, NULL) == -1)
        {
            throw std::runtime_error("timer file descriptor can't be written");
        }
    }

    void FDTimer::Unset()
    {
        struct itimerspec new_value;

        new_value.it_value.tv_sec = 0;
        new_value.it_value.tv_nsec = 0;

        new_value.it_interval.tv_sec = 0;
        new_value.it_interval.tv_nsec = 0;

        if (timerfd_settime(m_fd, 0, &new_value, NULL) == -1)
        {
            throw ("timerfd_settime");
        }
        // queue is empty
        ///set zero yo the set time 
    }
} // namespace ilrd

