
/******************************************************************************
 *	Title:		Test Scheduler-Reactor
 *	Authour:	Ivanna Fleisher
 ******************************************************************************/
#include "servers.h"    /* implementation part of servers p_p */
#include <stdlib.h>     /* exit                               */
#include <string.h>     /* strlen                             */
#include <netinet/in.h> /* sockaddr_in                        */
#include <unistd.h>     /* close                              */
#include <stdio.h>      /* printf, perror                     */

#include <sys/select.h>

/* According to earlier standards */
#include <sys/time.h>
#include <sys/types.h>
#include <time.h>//print time

#include "reactor.hpp"  //REACTOR api
#include "listener.hpp"    //servers api
#include "scheduler.hpp" //scheduler, timer
/* DEFINES */
#define PORT (8000)
#define MAXLINE (1024 )
#define MAX_CLIENT (30)
#define MAXFUNC(a,b)((a)>(b)?(a):(b))

using namespace ilrd;
void HandleSecond(int fd);
char buffer[MAXLINE]; 

/******************************************************************************/
Reactor reactor(new DerievedListener());
void Client_TCP(int fd)
{
    TCP_Answer(fd, "hi from tcp server to client tcp");
}
Callback< Source<int> >::CallbackPointer CallClient_TCP(Client_TCP);

void AnswerIO(int fd)
{
    char message[30];
    printf("STDIN...\n");
    scanf("%s", message); 
    
    printf("Message: %s\n", message);
    printf("I HEAR YOOOOOU\n");
    (void)fd;
}
Callback< Source<int> >::CallbackPointer CallAnswerIO(AnswerIO);


void HandleFirst(int fd)
{ 
    printf("^^^^RECEIVE MESSAGE UDP^^^^^\n");
    int n = 0;
    struct sockaddr_in address_peer; 
    int addrlen = sizeof(struct sockaddr_in);
    bzero(buffer, sizeof(buffer));

    printf("\nMessage from UDP client: "); 
    n = recvfrom(fd, buffer, sizeof(buffer), 0, 
                    (struct sockaddr*)&address_peer, (socklen_t *)&addrlen); 
    buffer[n] = '\0'; 
    printf("... %s\n", buffer); 
    sendto(fd, "hello from UDP server", strlen("hello from UDP server"), 0, 
    (struct sockaddr*)&address_peer, sizeof(address_peer));
    reactor.Stop();
}
Callback< Source<int> >::CallbackPointer  CallHandleFirst(HandleFirst);

Callback< Source<int> >::CallbackPointer CallHandleSecond(HandleSecond);
void HandleSecond(int fd)
{
    printf("^^^^RECEIVE MESSAGE TCP^^^^^\n");
    struct sockaddr_in address_peer; 
    int addrlen = sizeof(struct sockaddr_in);

    int new_socket = accept(fd, (struct sockaddr*)&address_peer, (socklen_t*)&addrlen);

    HandleAndMode new_s(READ,new_socket);
    TCP_Answer(new_socket, "hello from tcp server");
    Callback< Source<int> >c_second(CallHandleSecond);
    //add to the reactor
    reactor.Add(new_s,&c_second);
    reactor.Remove(new_s);
}
/******************************************************************************/
void Sched1(int fd)
{ 
    printf("^^^^RECEIVE MESSAGE SCHEDULER 1^^^^^\n");

	// time_t is arithmetic time type
	time_t now;

	// Obtain current time
	// time() returns the current time of the system as a time_t value
	time(&now);
    printf("Today is : %s", ctime(&now)); 
    (void)fd;
}
Callback< Source<int> >::CallbackPointer sched1(Sched1);

void Sched2(int fd)
{ 
    printf("^^^^RECEIVE MESSAGE SCHEDULER 2^^^^^\n");

	// time_t is arithmetic time type
	time_t now;
	// Obtain current time
	// time() returns the current time of the system as a time_t value
	time(&now);
    printf("Today is : %s", ctime(&now));
    (void)fd; 
}
Callback< Source<int> >::CallbackPointer  sched2(Sched2);
/*SET milliseconds*/
FDTimer::MS m1(5000);
FDTimer::MS m2(8000);
FDTimer::MS m3(10000);
/*******************************************************************************
                                * MAIN *
 ******************************************************************************/
int main()
{
    /* sets of file descriptors*/  
    int sock[SIZE] = {0};
    struct sockaddr_in address[SIZE];
    Scheduler* to_use = NULL;
    /* initialize all sockets */
  /*   sock[UDP] = CreateSocket(SOCK_DGRAM);
    sock[TCP] = CreateSocket(SOCK_STREAM);
    SocketInfo(&address[UDP],sock[UDP], INADDR_ANY, htons(PORT));
    std::cout << "create sockets in test scheduler"<< "\n";
    SocketInfo(&address[TCP],sock[TCP], INADDR_ANY, htons(PORT));

    listen(sock[TCP], 10);  */

    Callback< Source<int> >c_first(CallHandleFirst);
    Callback< Source<int> >c_third(CallAnswerIO);
    Callback< Source<int> >c_second(CallHandleSecond);
    /**************************************************************************/
    
  /*   reactor.Add(HandleAndMode (READ,sock[UDP]),&c_first); 
    reactor.Add(HandleAndMode (READ,sock[TCP]),&c_second);
    reactor.Add(HandleAndMode (READ,STDIN_FILENO),&c_third); */
    
    try 
    {
        to_use = new Scheduler(reactor);
    } 
    catch(std::exception const& e) 
    {   
        std::cout << "Exception: " << e.what() << "\n";
    } 

    to_use->ScheduleAction(m1, sched1);
    to_use->ScheduleAction(m2, sched2);
    reactor.Run();
    sleep(20);
    reactor.Stop();
    delete to_use;

    return 0;
}