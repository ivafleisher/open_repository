
/*******************************************************************************
                     * IMPLEMENTATION ADVANCED REACTOR *
 ******************************************************************************/
#include "reactor.hpp"
#include "iostream"

namespace ilrd
{

Reactor:: ~Reactor()
{
    delete m_Listener;  
}

void Reactor::Add(HandleAndMode handle_and_mode,Callback< Source<int> >* callback_o)
{                                                                                            
    assert(callback_o);
    //assert Handle and Mode not in the map 
    boost::shared_ptr< Source<int> > src_tmp( new Source<int>() ); 
    src_tmp.get()->Subscribe(callback_o);
    m_EventHandlers.insert(std::pair<HandleAndMode,
                    boost::shared_ptr< Source<int> > >(handle_and_mode, src_tmp));
}

void Reactor::Remove(HandleAndMode handle_and_mode)
{
    m_EventHandlers.erase(handle_and_mode);
}

void Reactor::Stop()
{
    assert(m_g_stop == 0);

    m_g_stop = 1;
}
/******************************************************************************/
///Functor to Push 
class PushToListen
{ 
public: 
    HandleAndMode operator () (std::pair<const std::pair<MODE, int>,
                              boost::shared_ptr< Source<int> > >& it) const
    { 
        return it.first; 
    } 
}; 

///Functor to Push 
class ToHandle 
{ 
private:
    std::map<HandleAndMode, boost::shared_ptr< Source<int> > >m_map;

public: 
    ToHandle(std::map<HandleAndMode, boost::shared_ptr< Source<int> > >& o_map)
        :m_map(o_map){}

    void operator () (HandleAndMode& handle_mode) 
    { 
        std::map<HandleAndMode, boost::shared_ptr< Source<int> > >::iterator it;
        it = m_map.find(handle_mode);

        if (it != m_map.end())
        {
            it->second.get()->Notify(it->first.second);
        }
    } 
}; 
/******************************************************************************/
void Reactor::Run()
{
    //assert That we are not running
    std::vector<HandleAndMode>to_listen;//move to the while loop (can smth be deleted)
    std::map<HandleAndMode,  boost::shared_ptr< Source<int> > >::iterator it;

    while (!m_EventHandlers.empty() && !m_g_stop)
    {
        std::transform(m_EventHandlers.begin(),m_EventHandlers.end(),
                        std::back_inserter(to_listen), PushToListen());

        std::vector<HandleAndMode>to_handle = m_Listener->Listen(to_listen);
        for_each(to_handle.begin(), to_handle.end(), ToHandle(m_EventHandlers));
    }
}
}// namespace ilrd
