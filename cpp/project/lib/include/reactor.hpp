#ifndef __ADVANCED_REACTOR_HPP__
#define __ADVANCED_REACTOR_HPP__

//Reactor design pattern API
#include <iostream>
#include <vector>
#include <queue>
#include <map>
#include <boost/function.hpp>
//#include <boost/enable_shared_from_this.hpp>
#include <boost/shared_ptr.hpp>
#include <utility>

//Source-Callback
#include "source_callback.hpp"
namespace ilrd
{

//The type of a handle is system specific. 
//We're using UNIX so an handle is represented by an integer
typedef int Handle;

enum MODE
{
    READ,
    WRITE,
    EXCEPTION
};

typedef boost::function< void(int) > HandleFunc;
typedef std::pair<MODE, Handle> HandleAndMode;

//The user may derieves from this class to define his own Listener class 
class IListener
{
public:
    virtual ~IListener(){};
    virtual std::vector<HandleAndMode> Listen(const std::vector< HandleAndMode >& handle) = 0;
};

// Registration interface of the Reactor
class Reactor
{
public:
    Reactor(IListener *listener):m_Listener(listener),m_g_stop(0){}
    void Add(HandleAndMode handle_and_mode,Callback< Source<int> >* callback_o);
    void Remove(HandleAndMode handle_and_mode);
    void Run();
    void Stop(); // can be called only from the handler/run 
                //if there is nothing to listen to (if the map is empty)
    ~Reactor();

private:
    //shared pointer
    std::map<HandleAndMode, boost::shared_ptr< Source<int> > > m_EventHandlers;
    IListener *m_Listener;//IListener m_Listener
    int m_g_stop;
};
}// namespace ilrd


#endif /* __ADVANCED_REACTOR_HPP__*/

