#ifndef _SOURCE_CALLBACK_HPP__
#define _SOURCE_CALLBACK_HPP__
/******************************************************************************
 *	Title:		Advanced OBSERVER
 *	Authour:	Ivanna Fleisher
 ******************************************************************************/
#include <boost/core/noncopyable.hpp>
#include <boost/function.hpp>
#include <iostream>
#include <assert.h>

#define LOG_ERR(x)(std::cerr<<"ERROR: "<<(x)<<std::endl)
#define LOG_WRN(x)(std::cerr<<"WARNING: "<<(x)<<std::endl)

template < typename SOURCE >
class Callback;
//EmptyFunch For death
static void DoNothing()
{}
/******************************************************************************/
template < typename T >
class Source : private boost::noncopyable
{
public:
    Source();
    ~Source();
    typedef T dataType; // nested type

    void Subscribe(Callback< Source<T> >* callback);
    void Unsubscribe(Callback< Source<T> >* callback);
    void Notify(dataType data);

private:
    Callback< Source<T> >* m_callback;
};
/********************************SOURCE****************************************/
template < typename SOURCE > 
class Callback : private boost::noncopyable
{
public:
    typedef boost::function< void(typename SOURCE::dataType) > CallbackPointer;
    typedef boost::function< void() > DeathPointer;

    Callback(const CallbackPointer& func,
             const DeathPointer&death_func = &DoNothing); 
             // initialize an empty function for death_func

    ~Callback();
private:
    // friend SOURCE;
    struct FriendHelper{typedef SOURCE MySource;};
    friend class FriendHelper::MySource;

    // should move to the private section
    void Link(SOURCE* source);
    void Unlink(bool notify);
    void Invoke(typename SOURCE::dataType data);

private:
    SOURCE* m_source;
    const CallbackPointer m_func;
    const DeathPointer m_death_func;
};

/******************************************************************************
 *	Title:		IMPLEMENTATION Source
 *	Authour:	Ivanna Fleisher
 ******************************************************************************/

template < typename T >
Source<T>::Source()
:m_callback(NULL)
{}

template < typename T >
Source<T>::~Source()
{
    //check NULL
    if(NULL != m_callback)
    {
        m_callback->Unlink(true);
    }
}

template < typename T >
void Source<T>::Subscribe(Callback< Source<T> >* callback)
{
    assert(callback); assert(NULL == m_callback);

    m_callback = callback;
    m_callback->Link(this);
}

template < typename T >
void Source<T>::Unsubscribe(Callback< Source<T> >* callback)
{
    //check NULL
    if(NULL != m_callback)
    {
        m_callback->Unlink(false);
        m_callback = NULL;
    }
    else
    {
       LOG_ERR("Source has not subscribe to any callback");
    }
    (void)callback;
}

template < typename T >
void Source<T>::Notify(dataType data)
{
    //check NULL
    if(NULL != m_callback)
    {
        m_callback->Invoke(data);
    }
}
/******************************************************************************
 *	Title:		IMPLEMENTATION Source
 *	Authour:	Ivanna Fleisher
 ******************************************************************************/

template < typename SOURCE > 
Callback<SOURCE>::Callback(const CallbackPointer& func,
                            const DeathPointer& death_func) 
                            // initialize an empty function for death_func
    :m_source(NULL),m_func(func), m_death_func(death_func)
{
    assert(!m_func.empty());    
}

template < typename SOURCE > 
Callback<SOURCE>::~Callback()
{
    //check NULL = source 
    //Unsubscribe 
    if(NULL != m_source)
    {
        m_source->Unsubscribe(this);
    }
   
    m_death_func();
}

// should move to the private section
template < typename SOURCE > 
void Callback<SOURCE>::Link(SOURCE* source)
{
    if(!source)
    {
        LOG_ERR("source not exist");
    }
    else
    {
        m_source = source;
    }
}
    
template < typename SOURCE > 
void Callback<SOURCE>::Unlink(bool notify)
{
    assert(m_source);
    
    if(notify)
    {
        m_source = NULL;
    }
}

template < typename SOURCE > 
void Callback<SOURCE>::Invoke(typename SOURCE::dataType data)
{
    m_func(data);
}

#endif /* _SOURCE_CALLBACK_HPP__*/
