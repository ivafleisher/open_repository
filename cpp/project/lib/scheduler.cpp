    
  /*******************************************************************************
                            IMPLEMENTATION SCHEDULER                                                           	   *
 ******************************************************************************/  
#include <boost/bind.hpp>//binds
#include "scheduler.hpp"

namespace ilrd
{    

Scheduler::Scheduler(Reactor& reactor)
		:m_timer(reactor, boost::bind( &Scheduler::PopTask, this, _1))
{}
// Scheduler Destructor
// Exceptions: no exceptions
Scheduler::~Scheduler()
{
	std::cout<<"Dtor scheduler"<<std::endl;
}


void Scheduler::ScheduleAction(TimePoint timepoint, ActionFunc function)
{
	Task task;
	task.m_function = function;
	task.m_timepoint = timepoint;

	//set time compare time point - now
	m_tasks.push(task);
	if(m_tasks.top().m_timepoint == task.m_timepoint)
	{
		m_timer.Set(boost::chrono::duration_cast<MS>(timepoint - Now()));
	}
}

void Scheduler::ScheduleAction(MS milliseconds,
							ActionFunc function) // boost::chrono::microseconds
{
	Task task;
	task.m_function = function;
	task.m_timepoint = Now() + milliseconds;
	m_tasks.push(task);

	if(m_tasks.top().m_timepoint == task.m_timepoint)
	{
		m_timer.Set(milliseconds);
	}  
}

Scheduler::TimePoint Scheduler::Now()
{
	return boost::chrono::steady_clock::now();
}

void Scheduler::PopTask(int fd)
{
	if(!m_tasks.empty())
	{

	Task to_exec = m_tasks.top();
	m_tasks.pop(); 
	//execute task
	to_exec.m_function(fd);

		// settime 
		if(!m_tasks.empty())
		{
			m_timer.Set(boost::chrono::duration_cast<MS>(m_tasks.top().m_timepoint 
				- Now()));
		}
		else
		{
			m_timer.Unset();
		}
	}
}
}