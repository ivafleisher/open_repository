#ifndef _UDP_CONNECTOR_HPP
#define _UDP_CONNECTOR_HPP
/*****************************************************************************
 * API UDP Connector
 * Ivanna Fleisher
*****************************************************************************/
#include "boost/core/noncopyable.hpp"
#include <netinet/in.h>
#include <cstring>
#include<iostream>

namespace ilrd
{

class UDPConnector: private boost::noncopyable
{
public:
    typedef u_int32_t ip_t;
    explicit UDPConnector(int port);

    inline int GetFD()const{ return m_socket;};
    void Write(const void* buffer, size_t buffer_size, ip_t client_ip,
            int port) const;
    void Read(void* buffer, size_t buffer_size, ip_t *client_ip,
            int *port) const;
    ~UDPConnector(){close(m_socket); }
private:
    int m_socket;
};

} // namespace ilrd

#endif // _UDP_CONNECTOR_HPP
