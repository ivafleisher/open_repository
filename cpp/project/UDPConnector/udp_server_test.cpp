/*****************************************************************************
 * TEST SERVER UDP
 * Ivanna Fleisher
******************************************************************************/
/* Server side implementation of UDP client-server model */ 
#include <netinet/in.h> //sockaddr
#include <iostream>     //std::runtime_error
#include <cstring>      //std::memset
  
#define PORT     8080 
#define MAXLINE 1024 
  
/* Driver code */ 
int main() { 
    int len, n; 
    int sockfd; 
    char buffer[MAXLINE]; 
    char *hello = "Hello from server"; 
    struct sockaddr_in servaddr, cliaddr; 
      
    /*  Creating socket file descriptor */ 
    if ( (sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 )
    { 
        std::cerr<<"socket creation failed";
        return 0;    
    } 
      
    std::memset(&servaddr, 0, sizeof(servaddr)); 
    std::memset(&cliaddr, 0, sizeof(cliaddr)); 
      
    /*  Filling server information */ 
    servaddr.sin_family    = AF_INET; /*  IPv4  */
    servaddr.sin_addr.s_addr = INADDR_ANY; 
    servaddr.sin_port = htons(PORT); 
      
    /* Bind the socket with the server address */ 
    if ( bind(sockfd, (const struct sockaddr *)&servaddr,  
            sizeof(servaddr)) < 0 ) 
    { 
        std::cerr<<"bind failed";
        return 0;
    } 
     //Receive  
    len = sizeof(cliaddr); 
    n = recvfrom(sockfd, (char *)buffer, MAXLINE,  
                MSG_WAITALL, ( struct sockaddr *) &cliaddr, 
                (socklen_t *)&len);
                buffer[n] = '\0'; 
    std::cout<<"Server recive:" << buffer <<std::endl; 
   //Send
    sendto(sockfd, (const char *)hello, strlen(hello),  
        MSG_CONFIRM, (const struct sockaddr *) &cliaddr, len); 
    std::cout <<"Hello message sent.\n";
    close (sockfd);
    
    return 0; 
}
