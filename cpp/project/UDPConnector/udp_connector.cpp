
/*****************************************************************************
 * Implementation UDP Connector
 * Ivanna Fleisher
 * version 05.08.2020.0
******************************************************************************/
#include "udp_connector.hpp"    
#include <netinet/in.h> //sockaddr
#include <iostream>     //std::runtime_error
#include <cstring>      //std::memset

namespace ilrd
{

UDPConnector::UDPConnector(int port)
{
    struct sockaddr_in servaddr;
    if ((m_socket = socket(AF_INET, SOCK_DGRAM, 0)) < 0) 
    { 
        throw std::runtime_error("creation of the socket fail");
    } 
    std::memset(&servaddr, 0, sizeof(servaddr)); 
      
   // Filling server information 
    servaddr.sin_family  = AF_INET; // IPv4 
    servaddr.sin_addr.s_addr = INADDR_ANY; 
    servaddr.sin_port = htons(port); 
    
    if(bind(m_socket, (struct sockaddr *)&servaddr, sizeof(servaddr))< 0)
    //if(connect(m_socket, (struct sockaddr *)&servaddr, sizeof(servaddr)) < 0) 
    { 
        throw std::runtime_error("bind fail");
    }  
}

void UDPConnector::Write(const void* buffer, size_t buffer_size, ip_t client_ip,
                        int port) const
{
    struct sockaddr_in servaddr;

     // Filling server information 
    servaddr.sin_family  = AF_INET; // IPv4 
    servaddr.sin_addr.s_addr = client_ip; 
    servaddr.sin_port = htons(port); 
   
    sendto(m_socket, buffer,buffer_size, 0, 
    (struct sockaddr*)&servaddr, sizeof(servaddr));
}

void UDPConnector::Read(void* buffer, size_t buffer_size, ip_t *client_ip, int *port) const
{
    struct sockaddr_in address;
    socklen_t length = sizeof(struct sockaddr_in);
    if (0 >  recvfrom(  m_socket, 
                        buffer, 
                        buffer_size,  
                        MSG_WAITALL, 
                        ( struct sockaddr *) &address, 
                        &length))
    {
        throw(std::runtime_error("failed to receive data\n"));  
    }

    *client_ip = address.sin_addr.s_addr;
    *port = ntohs(address.sin_port);  

}

} // namespace ilrd

