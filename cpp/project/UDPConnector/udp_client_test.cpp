/*****************************************************************************
 * TEST Client UDP
 * Ivanna Fleisher
 * version 05.08.2020.0
******************************************************************************/
#include <iostream>    //std::cout
#include <netinet/in.h> 
#include "udp_connector.hpp"//implementation udp connector
  
#define PORT     8080 
#define MAXLINE 1024 
using namespace ilrd;
int main() 
{ 
    int n = 0; 
    char buffer[MAXLINE]; 
    const char *hello = "Hello from client"; 
    struct sockaddr_in  servaddr; 
      
    UDPConnector soket_udp(8080);
    //SEnd
    sendto(soket_udp.GetFD(), (const char *)hello, MAXLINE, 
        MSG_CONFIRM, (const struct sockaddr *) NULL, sizeof(servaddr)); 
    std::cout <<"Hello message sent.\n";
    //Recive    
    n = recvfrom(soket_udp.GetFD(), (char *)buffer, MAXLINE,  
                MSG_WAITALL, (struct sockaddr *) &servaddr, NULL); 
    buffer[n] = '\0'; 
    std::cout<<"Server recive:" << buffer <<std::endl; 
  
    close(soket_udp.GetFD()); 

    return 0;
}