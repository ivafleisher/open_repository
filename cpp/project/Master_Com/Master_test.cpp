/*****************************************************************************
 * Test Communication Master to minion
 * Ivanna Fleisher

******************************************************************************/

// Server side implementation of UDP client-server model 
#include <stdio.h> 
#include <stdlib.h> 
#include <unistd.h> 
#include <string.h> 
#include <sys/types.h> 
#include <sys/socket.h> 
#include <arpa/inet.h> 
#include <netinet/in.h> 


#include "protocol.hpp"
#include <cstring>

#define PORT     8080 
#define MAXLINE 1024 
using namespace ilrd;
// Driver code 
int main() 
{ 
    int sockfd; 
    char buffer[MAXLINE]; 
    char *hello = "Hello from server"; 
    struct sockaddr_in servaddr, cliaddr; 
      
    // Creating socket file descriptor 
    if ( (sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) { 
        perror("socket creation failed"); 
        exit(EXIT_FAILURE); 
    } 
      
    memset(&servaddr, 0, sizeof(servaddr)); 
    memset(&cliaddr, 0, sizeof(cliaddr)); 
      
    // Filling server information 
    servaddr.sin_family    = AF_INET; // IPv4 
    servaddr.sin_addr.s_addr = INADDR_ANY; 
    servaddr.sin_port = htons(PORT); 
    // Filling client information 
    cliaddr.sin_family    = AF_INET; // IPv4    
    cliaddr.sin_addr.s_addr = inet_addr("127.0.0.1"); 
    cliaddr.sin_port = htons(8000); 
      
    // Bind the socket with the server address 
    if ( bind(sockfd, (const struct sockaddr *)&servaddr,  
            sizeof(servaddr)) < 0 ) 
    { 
        perror("bind failed"); 
        exit(EXIT_FAILURE); 
    } 
      
    int len, n; 
  
    len = sizeof(cliaddr);  //len is value/resuslt 
    
    Request *req= reinterpret_cast<Request *>(operator new(sizeof(Request)+strlen(hello)));
    req->m_mode = 1;
    req->m_uid = 12345;
    req->m_index = 5;
    std::memcpy(req->m_data, hello,strlen(hello)+1); 

    printf("MASTER SEND A MESSAGE\n"); 

    n = sendto(sockfd, (const char *)req, sizeof(Request)+strlen(hello),  
        MSG_CONFIRM, (const struct sockaddr *) &cliaddr, 
        len);
    if (-1 == n)
    {
        perror("sendto");
    } 
    printf("Hello message sent.\n");  
      while(1){}
    return 0; 
} 
