/*******************************************************************************
 * File: prtocol.hpp - header for protocols for request and responce                                                                                        
                                                              
 ******************************************************************************/
#ifndef __MINION_PROTOCOL_HPP__
#define __MINION_PROTOCOL_HPP__

#include <stdlib.h>     /*  size_t */
#include <stdint.h>    /* unint_64_t */

namespace ilrd
{
    const size_t BLOCK_SIZE = 4096;
    struct Request
    {
        inline size_t RequestSize()const{return sizeof(*this);}

        char m_mode;
        uint64_t m_uid;
        uint64_t m_index;
        char m_data[1];
    
    }__attribute__((packed));


    struct Response
    {
        inline size_t ResponseSize()const{return sizeof(*this);}
        
        char m_mode;
        uint64_t m_uid;
        char m_status;
        char m_data[1];
    }__attribute__((packed));

} // namespace ilrd

#endif /*__MINION_PROTOCOL_HPP__ */
