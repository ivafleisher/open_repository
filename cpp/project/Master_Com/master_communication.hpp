/*******************************************************************************
 * File: master_communication.hpp - header file for master communicator functions                     
 * Author:Ivanna Fleisher                                                         
                                                                
 ******************************************************************************/
#ifndef __MASTER_COMMUNICATOR_HPP__
#define __MASTER_COMMUNICATOR_HPP__

#include <boost/noncopyable.hpp> // boost::noncopyable
#include "udp_connector.hpp"    
#include "reactor.hpp"
#include "protocol.hpp"

namespace ilrd
{
    class MasterCommunicator:private boost::noncopyable
    {
    public:
        typedef boost::function <void (const Request&) > ActionRequest;
        explicit MasterCommunicator(int port, Reactor& reactor, 
            ActionRequest ar_func);

        // ReadRequest Function (added to reactor)
        // Receives: file descriptor.
        // Returns: nothing
        void ReadRequest(int fd);

        // Reply Function
        // Receives: const refernce to class Response.
        // Returns: nothing
        void Reply(const Response& res) const;
        ~MasterCommunicator();
    private:
        ActionRequest m_ar_func;
        UDPConnector m_con;
        Reactor&  m_reactor;
        Callback< Source< int > > m_callback; 
        UDPConnector::ip_t m_master_ip;
        int m_master_port;
    };

} // namespace ilrd

#endif /* __MASTER_COMMUNICATOR_HPP__ */
