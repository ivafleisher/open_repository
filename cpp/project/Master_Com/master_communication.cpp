/*******************************************************************************
 * File: master_communication.cpp - header file for master communicator functions                     
 * Author:Ivanna Fleisher                                                         
                    
                                                           
 ******************************************************************************/        
#include <boost/bind.hpp>//bind function
#include <cstring>
#include <netinet/in.h>             //master communication
#include "master_communication.hpp"//master communication
#include "logger.hpp"//logger

namespace ilrd
{
        
MasterCommunicator::MasterCommunicator(int port,
                                    Reactor& reactor,
                                    ActionRequest ar_func)
    :m_ar_func(ar_func),
    m_con(port),   
    m_reactor(reactor),
    m_callback(boost::bind(&MasterCommunicator::ReadRequest, this, _1))
{
    //put ReadRequest in the reactor 
    m_reactor.Add(HandleAndMode(READ, m_con.GetFD()),&m_callback);
}

// ReadRequest Function (added to reactor)
// Receives: file descriptor.
// Returns: nothing
const int SIZE = 4096;
void MasterCommunicator::ReadRequest(int fd) 
{
    char buffer[sizeof(Request) + SIZE-1];
    m_con.Read(buffer,sizeof(buffer) ,&m_master_ip, &m_master_port);
    m_ar_func(*((Request*)buffer));

    (void)fd;
}

// Reply Function
// Receives: const refernce to class Response.
// Returns: nothing
void MasterCommunicator::Reply(const Response& res) const
{
    size_t n_bytes = 0;
    if(res.m_mode == 1)
    {
       n_bytes = 10; 
    }
    else
    {
       n_bytes = res.ResponseSize()+SIZE-1;
    }
    
    m_con.Write(&res, n_bytes, m_master_ip, m_master_port);
    LOG_INFO("MasterCommunicator::Reply");
} 
MasterCommunicator::~MasterCommunicator()
{
    m_reactor.Remove(HandleAndMode(READ, m_con.GetFD()));
}
}//namespace ilrd 
