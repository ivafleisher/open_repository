/*******************************************************************************
                     * IMPLEMENTATION MINION *
 ******************************************************************************/
#include <boost/bind.hpp>//bind
#include "storage.hpp"
#include "listener.hpp"  //REACTOR api  
#include "minion.hpp"//minion
#include "command.hpp"//command
#include "factory.hpp"//command

namespace ilrd
{
typedef Singleton< Factory<int, BaseCommand,
                Params, BaseCommand* (*)(Params)> > factory_singleton;

Minion::Minion(int port, int num_of_blocks, int master_port)
    :m_reactor(new DerievedListener()),
    m_comm(port,m_reactor,boost::bind(&Minion::OnRequest, this, _1)),
    m_storage(num_of_blocks)
{
  factory_singleton::GetInstance()->Add(0,&ReadBuilder);
  factory_singleton::GetInstance()->Add(1,&WriteBuilder);

  (void)master_port;
}

Minion::~Minion()
{
    m_reactor.Stop();
}

void Minion::Run()
{
    m_reactor.Run();
}

void Minion::OnRequest(const Request& request)
{
    Response * response = NULL;
    Params par = {m_storage, &response, request};

    BaseCommand* command = factory_singleton::GetInstance()->Create(request.m_mode,par);
    (*command)();

    response->m_mode = request.m_mode;    
    response->m_uid = request.m_uid;  

    m_comm.Reply(*response);

    operator delete (response);
}

} // namespace ilrd