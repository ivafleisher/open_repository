/*****************************************************************************
 * Test MINION
 * Ivanna Fleisher
******************************************************************************/
#include "master_communication.hpp"
#include "minion.hpp"
#define PORT (8000)
using namespace ilrd;
int main()
{
    Minion minion(PORT, 10,PORT);
    minion.Run();
    
    return 0;
}
