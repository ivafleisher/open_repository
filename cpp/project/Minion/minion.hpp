#ifndef __MINION_HPP__
#define __MINION_HPP__

#include <boost/noncopyable.hpp> // boost::noncopyable

#include "protocol.hpp"
#include "reactor.hpp"
#include "master_communication.hpp"
#include "storage.hpp"

namespace ilrd
{
//const size_t BLOCK_SIZE = 4096;
class Minion: private boost::noncopyable
{
public:
    explicit Minion(int port, int num_of_blocks, int master_port);
    ~Minion();

    // Run
    // Receives: nothing
    // Returns: nothing
    void Run();
private:

    // OnRequest
    // Receives: Request structure.
    // Returns: nothing
    void OnRequest(const Request& request);

    Reactor m_reactor;
    MasterCommunicator m_comm;
    Storage<BLOCK_SIZE> m_storage;
};

} // namespace ilrd

#endif /* __MINION_HPP__ */
