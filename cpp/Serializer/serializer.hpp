#ifndef _PRIORITYQUEUE_HPP
#define _PRIORITYQUEUE_HPP

#include <iostream> 
#include <typeinfo>             //typeid
#include <boost/shared_ptr.hpp> //shared pointer
#include "factory.hpp"          //factory

namespace dp
{
template < class BASE >
class Serializer: boost::noncopyable
{
public:
    template <class DERIVED>
    void Add(); 

    void Serialize(BASE& obj, std::ostream& os) const;
    boost::shared_ptr<BASE> Deserialize(std::istream& is) const;
private:
    template <class DERIVED>
    static BASE* Creator(std::istream& is);
private:
    Factory<std::string, BASE, std::istream&> m_factory;
};
/******************************************************************************
 * IMPLEMENTATION OF PQUEUE
******************************************************************************/
template < class BASE >
template <class DERIVED>
void Serializer<BASE>::Add() // DERIVED should have default ctor
{
    m_factory.Add(typeid(DERIVED).name(), Creator<DERIVED>);
}

template < class BASE >
template <class DERIVED>
BASE* Serializer<BASE>::Creator(std::istream& is) 
{
    DERIVED* to_return = new DERIVED();
    is >>( *to_return );
   
   return to_return;
}
/******************************************************************************/
template < class BASE >
void Serializer<BASE>::Serialize(BASE& obj, std::ostream& os) const
{
    os<<typeid(obj).name()<<std::endl;
    os<<obj;
}

template < class BASE >
boost::shared_ptr<BASE> Serializer<BASE>::Deserialize(std::istream& is) const
{
    std::string key;
    std::getline(is, key);

    boost::shared_ptr< BASE > shrd_tmp(m_factory.Create(key, is));
    return shrd_tmp;
}


}//namespace dp
#endif // _PRIORITYQUEUE_HPP
