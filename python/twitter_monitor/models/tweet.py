from bson import ObjectId  # json
from mongoengine import Document, StringField, DateTimeField, IntField, ObjectIdField


class Tweet(Document):
    meta = {
        "collection": "tweets"
    }
    # primary key for mongo ObjectId
    _id = ObjectIdField(primary_key=True, default=ObjectId)
    user_id = IntField(required=True)
    tweet_id = IntField(required=True)
    date = DateTimeField(required=True)
    # start with RT go to the retweeted_status.full_text
    text = StringField(required=True)
    source = StringField(required=True)
    retweet_count = IntField(required=True)
    likes_count = IntField(required=True)
