import logging
import logging.config
import os

import yaml


def logging_setup():
    """
    Setup logging configuration
    """
    log_path = os.path.join(os.path.dirname(__file__))

    with open(f"{log_path}/log_config.yaml") as f:
        logging.config.dictConfig(yaml.load(f, Loader=yaml.FullLoader))
