import yaml
from singleton import Singleton


class Configurator(metaclass=Singleton):
    def __init__(self):
        config = self._read_config()
        self.consumer_key = config["consumer_key"]
        self.consumer_secret = config["consumer_secret"]
        self.access_token = config["access_token"]
        self.access_token_secret = config["access_token_secret"]
        self.user_name = config["user_name"]
        self.scheduler_time = config["scheduler_time"]
        self.email = config['from_email']
        self.email_pass = config['email_password']

    @staticmethod
    def _read_config():
        with open("config.yaml") as f:
            return yaml.load(f, Loader=yaml.FullLoader)

