import logging
import traceback

import tweepy

from config import configurator
from config.configurator import Configurator
from mail_message.mail_message import MailMessage
from models.tweet import Tweet
from mongo_ops.mongo_ops import MongoOps
from connection.twitter_api.twitterapi import TwitterApi


class TwitterService:
    def __init__(self):
        self.log = logging.getLogger(__name__)

    def do_update(self):
        try:
            #  update twitter
            config = configurator.Configurator()
            api = TwitterApi().tweet_api

            first_tweet = api.user_timeline(id=config.user_name,
                                            page=1,
                                            count=1,
                                            tweet_mode='extended')
            x = MongoOps().get_latest_tweet_by_user_id(first_tweet[0].user.id)

            tweet_id = x.tweet_id if x else None

            for tweet in tweepy.Cursor(api.user_timeline,
                                       id=config.user_name,
                                       since_id=tweet_id,
                                       tweet_mode='extended'
                                       ).items():

                full_text = tweet.retweeted_status.full_text if tweet.full_text.startswith("RT") else tweet.full_text

                Tweet(tweet_id=tweet.id,
                      date=tweet.created_at,
                      text=full_text,
                      source=tweet.source,
                      retweet_count=tweet.retweet_count,
                      likes_count=tweet.favorite_count,
                      user_id=tweet.user.id).save()

        except:
            traceback_info = traceback.format_exc()
            self.log.error(traceback_info)

            subject = 'OMG Super Important Message'
            body = traceback_info
            gm = MailMessage(Configurator().email, Configurator().email_pass)
            gm.send_message(subject, body)
            exit(1)
