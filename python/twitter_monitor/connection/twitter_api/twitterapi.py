import tweepy
from config import configurator
from singleton import Singleton


class TwitterApi(metaclass=Singleton):
    def __init__(self):
        config_keys = configurator.Configurator()
        auth = tweepy.OAuthHandler(config_keys.consumer_key, config_keys.consumer_secret)
        auth.set_access_token(config_keys.access_token, config_keys.access_token_secret)
        self.tweet_api = tweepy.API(auth)

