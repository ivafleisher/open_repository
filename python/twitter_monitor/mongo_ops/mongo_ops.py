from models.tweet import Tweet


class MongoOps:

    @staticmethod
    def get_latest_tweet_by_user_id(user_id: int):
        return Tweet.objects(user_id=user_id).order_by('-date').first()

