import logging
import traceback

from apscheduler.schedulers.background import BlockingScheduler
import connection.mongo_connection
from config.logger import logging_setup
from twiiter_service import TwitterService
from config import configurator

log = logging.getLogger(__name__)


def task():
    TwitterService().do_update()


if __name__ == '__main__':
    logging_setup()
    connection.mongo_connection.MongoConnection()
    scheduler = BlockingScheduler()
    scheduler.add_job(task, 'interval', minutes=configurator.Configurator().scheduler_time)
    scheduler.start()
