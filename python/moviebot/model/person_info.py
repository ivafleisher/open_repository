
from mongoengine import Document, IntField, ObjectIdField


class PersonInfo(Document):
    meta = {
        "collection": "person_id"
    }
    _id = IntField(primary_key=True, required=True)
    statistic = IntField(required=True)
    games_amount = IntField(required=True)
