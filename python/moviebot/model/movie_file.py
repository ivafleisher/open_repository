from mongoengine import Document, IntField, StringField, ListField


class MovieFile(Document):
    meta = {
        "collection": "MovieFiles"
    }
    _id = IntField(primary_key=True, required=True)
    title = StringField(required=True)
    popularity = IntField(required=True)
    release_date = StringField(required=True)
    backdrop_path = StringField(required=True)
    genre_ids = ListField(required=True)
