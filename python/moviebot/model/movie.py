from enum import Enum


class Movie(Enum):
    ID = "_id"
    TITLE = "title"
    POPULARITY = "popularity"
    RELEASE_DATE = "release_date"
    BACKDROP_PATH = "backdrop_path"
    GENRE_IDS = "genre_ids"
