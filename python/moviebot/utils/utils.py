from urllib.request import urlopen


def get_content(url_path: str):
    return urlopen(url_path).read()


def count_percent(answer_counter, n_questions):
    return (answer_counter / n_questions) * 100
