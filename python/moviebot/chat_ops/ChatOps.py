import random
import time
import logging
import telebot

from telebot import types
from utils.utils import get_content, count_percent
from mongo_ops.MongOps import MongoOps
from model.movie import Movie
from decouple import config

db = MongoOps()
bot = telebot.TeleBot(config("TELEGRAM_TOKEN"))
NUM_QUESTIONS = int(config("NUM_QUESTIONS"))
logging.basicConfig(format="%(asctime)s: %(message)s",
                    level=logging.INFO,
                    datefmt="%H:%M:%S")


@bot.message_handler(commands=['start', 'help'])
def send_welcome(message):
    logging.info("START")
    link_handler(message)


def send_question(message, url_content, counter_questions, markup_items, answer_counter):
    markup = types.ReplyKeyboardMarkup(row_width=2)
    items_buttons = [types.KeyboardButton(markup_items[0][Movie.TITLE.value]),
                     types.KeyboardButton(markup_items[1][Movie.TITLE.value]),
                     types.KeyboardButton(markup_items[2][Movie.TITLE.value])]
    random.shuffle(items_buttons)
    markup.add(*items_buttons)

    logging.info("send_question   :  photo")
    bot.send_photo(message.chat.id, url_content)
    msg = bot.send_message(message.chat.id,
                           f'{counter_questions} Choose one :',
                           reply_markup=markup)
    bot.register_next_step_handler(msg, process_step,
                                   markup_items[0][Movie.TITLE.value],
                                   counter_questions + 1, answer_counter)


def process_step(message, answer, each_post, answer_counter):
    logging.info("process_step")
    check_answer = 'net'

    if message.text == answer:
        check_answer = 'da'
        answer_counter += 1

    bot.send_message(message.chat.id, check_answer)
    link_handler(message, each_post, answer_counter)


def is_continue(message, answer_counter, n_questions):
    bot.send_message(message.chat.id, 'FINISH')
    markup = types.ReplyKeyboardMarkup()
    markup.add(types.KeyboardButton("Start NEW GAME"), types.KeyboardButton("GET STATISTIC"))
    logging.info("is_continue")
    percent_correct = count_percent(answer_counter, n_questions)
    msg = bot.send_message(message.chat.id,
                           f'Results :{percent_correct}: {answer_counter}/{n_questions} '
                           f'Choose :', reply_markup=markup)

    person_info = MongoOps.get_user(message.chat.id)
    MongoOps.fill_statistic(person_info,
                            message.chat.id,
                            person_info.games_amount + 1 if person_info else 1,
                            (person_info.statistic + percent_correct) / 2 if person_info else percent_correct)
    bot.register_next_step_handler(msg, check_is_continue, "Start NEW GAME", "GET STATISTIC")


def check_is_continue(message, next_game_str, statistic_str):
    logging.info("check_is_continue")

    if next_game_str == message.text:
        print(message.text)
        logging.info("check_is_continue get ")
        bot.send_message(message.chat.id, 'LET`S PLAY')
        link_handler(message)

    if statistic_str == message.text:
        statistic = MongoOps.get_statistic(message.chat.id)
        msg = bot.send_message(message.chat.id, f'STATISTIC {statistic}')
        bot.register_next_step_handler(msg, check_is_continue, next_game_str, statistic_str)


def link_handler(message, each_post=0, answer_counter=0):
    if each_post < NUM_QUESTIONS:
        logging.info("link handler")
        movie_curr = db.get_movie().next()
        logging.info(f'link handler : get movie {db.url_begin}{movie_curr[Movie.BACKDROP_PATH.value]}')

        content = get_content(f'{db.url_begin}{movie_curr[Movie.BACKDROP_PATH.value]}')

        try:
            first = db.get_similar(movie_curr[Movie.RELEASE_DATE.value],
                                   movie_curr[Movie.GENRE_IDS.value],
                                   [movie_curr[Movie.ID.value]]).next()
        except Exception as e:
            first = db.get_movie().next()
            logging.info(f'There are no results for this query :{e}')

        try:
            second = db.get_similar(first[Movie.RELEASE_DATE.value],
                                    first[Movie.GENRE_IDS.value],
                                    [first[Movie.ID.value]]).next()
        except Exception as e:
            second = db.get_movie().next()
            logging.info(f'There are no results for this query :{e}')

        new_items = [movie_curr, first, second]
        send_question(message, content, each_post, new_items, answer_counter)
    else:
        is_continue(message, answer_counter, NUM_QUESTIONS)


def bot_polling():
    while True:
        try:
            bot.polling(none_stop=True)
        except Exception as e:
            print(e)
            time.sleep(1)
