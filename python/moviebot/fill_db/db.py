from datetime import datetime
import logging
import threading
import mongoengine
import tmdbsimple as tmdb
from decouple import config
from model.movie_file import MovieFile

START_YEAR = 1990
logging.basicConfig(format="%(asctime)s: %(message)s",
                    level=logging.INFO,
                    datefmt="%H:%M:%S")


class DB:
    def __init__(self):
        mongoengine.connect('movie_bot', host='localhost', port=27017)
        tmdb.API_KEY = config("TMDB_API_KEY")
        self.discover = tmdb.Discover()
        self.movie = tmdb.Movies()

    def update_db_movie(self):
        end_year = datetime.utcnow().year
        threads = list()
        for year in range(START_YEAR, end_year + 1):
            x = threading.Thread(target=self.__thread_function, args=(year,))
            threads.append(x)
            logging.info("START THREAD    :  thread %d.", year)
            x.start()

        for index, thread in enumerate(threads):
            logging.info("JOIN    : before joining thread %d.", index)
            thread.join()
            logging.info("JOIN    : thread %d done", index)

    def __thread_function(self, year):
        logging.info("Thread %s: starting", year)
        last_page = self.discover.movie(page=1, primary_release_year=year )['total_pages']

        for page in range(1, last_page + 1):
            search = self.discover.movie(page=page, primary_release_year=year)

            for x in search["results"]:
                if x["backdrop_path"]:
                    print(f'movie{x["id"]} {x["release_date"]}--------{year}:{page}, total{last_page}')
                    MovieFile.objects(_id=x['id']).update(_id=x["id"],
                                                          title=x["title"],
                                                          popularity=x["popularity"],
                                                          release_date=x["release_date"],
                                                          backdrop_path=x["backdrop_path"],
                                                          genre_ids=x["genre_ids"],
                                                          upsert=True)
        logging.info("Thread %s: finishing", year)
