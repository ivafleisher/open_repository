from fill_db.db import DB
from decouple import config
from apscheduler.schedulers.background import BlockingScheduler

db = DB()
db.update_db_movie()


def task():
    db.update_db_movie()


if __name__ == '__main__':
    scheduler = BlockingScheduler()
    scheduler.add_job(task, 'interval', day=config("SCHEDULER_TIME"))
    scheduler.start()
