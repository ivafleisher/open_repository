import re
import mongoengine
from decouple import config
from model.movie_file import MovieFile
from model.person_info import PersonInfo


class MongoOps:
    def __init__(self):
        mongoengine.connect('movie_bot', host='localhost', port=27017)
        self.url_begin = config("URL_BEGIN")

    @staticmethod
    def get_similar(release_date: str, genre_ids: list, movie_ids: list):
        release_year_pattern = re.compile(release_date[:4], re.I)
        return MovieFile.objects().aggregate([{"$match": {
            'genre_ids': {"$in": genre_ids},
            "release_date": {"$regex": release_year_pattern},
            "_id": {"$nin": movie_ids}}},
            {"$sample": {"size": 1}}])

    @staticmethod
    def get_movie():
        return MovieFile.objects().aggregate([{"$match": {'popularity': {"$gt": 10}}},
                                              {"$sample": {"size": 1}}])

    @staticmethod
    def get_user(id_user):
        return PersonInfo.objects(_id=id_user).first()

    @staticmethod
    def fill_statistic(person_info, chat_id, statistic, games_amount):
        if person_info:
            person_info.update(statistic=statistic, games_amount=games_amount)
        else:
            PersonInfo(_id=chat_id, statistic=statistic, games_amount=1).save()

    @staticmethod
    def get_statistic(chat_id):
        user = MongoOps.get_user(chat_id)
        return user.statistic





